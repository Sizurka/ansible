#!/usr/bin/python3

import subprocess
import tempfile
import yaml

# Adapted from: https://old.reddit.com/r/linuxquestions/comments/pi1daj/secure_boot_how_to_extract_nvidia_uefi_boot/hdgqlfn/


def extract_driver_hashes():
    event_log = subprocess.run(["tpm2_eventlog", "/sys/kernel/security/tpm0/binary_bios_measurements"],
                               stdout=subprocess.PIPE, check=True).stdout
    event_log = yaml.safe_load(event_log)

    hashes = set()
    for event in event_log['events']:
        if event['EventType'] != 'EV_EFI_BOOT_SERVICES_DRIVER':
            continue
        for digest in event['Digests']:
            if digest['AlgorithmId'] != 'sha256':
                continue
            hashes.add(digest['Digest'])
    return hashes


hashes = extract_driver_hashes()
if not hashes:
    print("No TPM hashes detected, is the machine booted with Secure Boot enabled?")
    exit(1)

with open('GUID_OPROM.txt', 'rt') as f:
    guid = f.read().strip()

with open('db_oprom.esl', 'wb') as hash_esl:
    for h in hashes:
        input_file = tempfile.NamedTemporaryFile()
        input_file.write(bytes.fromhex(h))
        input_file.flush()

        output_file = tempfile.NamedTemporaryFile()

        subprocess.check_call(["sbsiglist", "--owner", guid, "--type", "sha256",
                               "--output", output_file.name, input_file.name],)
        hash_esl.write(output_file.read())

subprocess.check_call(["sign-efi-sig-list", "-a", "-g", guid, "-k", "KEK.key", "-c", "KEK.crt",
                       "db", hash_esl.name, "db_oprom.auth"])

print(f"Signed {len(hashes)} OPROM hashes")
