#!/bin/bash
set -e

LINUX="/vmlinuz"
INITRD="/initrd.img"

if [ ! -r "${LINUX}" ]; then LINUX="/boot/vmlinuz-linux"; fi
if [ ! -r "${INITRD}" ]; then INITRD="/boot/initramfs-linux.img"; fi

objcopy \
  --add-section .osrel="/usr/lib/os-release" --change-section-vma .osrel=0x20000 \
  --add-section .cmdline="/etc/kernel/efi-command-line.txt" --change-section-vma .cmdline=0x30000 \
  --add-section .linux="${LINUX}" --change-section-vma .linux=0x40000 \
  --add-section .initrd="${INITRD}" --change-section-vma .initrd=0x3000000 \
  "/usr/lib/systemd/boot/efi/linuxx64.efi.stub" \
  "/boot/linux-image.efi"

sbsign \
  --key /root/keys/efi/db.key \
  --cert /root/keys/efi/db.crt \
  --output "/boot/linux-image.efi" \
  "/boot/linux-image.efi"

{% if not (tpm_security == "secureboot" or tpm_security == "customshim") %}
systemctl enable update-tpm.service || true
{% endif %}
