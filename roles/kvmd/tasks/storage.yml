- name: install storage packages
  apt:
    name:
      - dosfstools
      - parted
      - podman
      - crun
    state: present

- name: create storage mount image
  command:
    argv:
      - fallocate
      - -l
      - "{{ storage_size }}"
      - /var/lib/msd-overlay/File_Storage.img
    creates: /var/lib/msd-overlay/File_Storage.img
  register: mount_image

- name: set storage image permissions
  file:
    path: /var/lib/msd-overlay/File_Storage.img
    state: file
    owner: root
    group: kvmd
    mode: u=rw,g=r,o=

- name: partition storage mount image
  parted:
    label: msdos
    number: 1
    state: present
    part_start: 2MiB
    part_end: 100%
    device: /var/lib/msd-overlay/File_Storage.img
  when: mount_image.changed
  register: mount_partition

- name: add loop device for mount image
  command:
    argv:
      - losetup
      - --show
      - -f
      - -P
      - /var/lib/msd-overlay/File_Storage.img
  register: loop_device
  when: mount_partition.changed
- name: format storage mount image
  filesystem:
    fstype: vfat
    opts: -n KVM
    dev: "{{ loop_device.stdout }}p1"
    force: true
  when: mount_partition.changed
- name: remove mount image loop device
  command:
    argv:
      - losetup
      - -d
      - "{{ loop_device.stdout }}"
  when: mount_partition.changed

- name: create transfer mount point
  file:
    path: /var/lib/storage-data
    state: directory
    owner: root
    group: root
    mode: u=rwx,g=rx,o=rx

- name: add storage mount to fstab
  lineinfile:
    dest: /etc/fstab
    regexp: '^/var/lib/msd-overlay/File_Storage.img\s+/var/lib/storage-data'
    line: "/var/lib/msd-overlay/File_Storage.img	/var/lib/storage-data	vfat	fmask=0111,dmask=0000,noexec,nodev,nosuid,sync,offset=2097152	0	0"

- name: create bind mount for transfer image
  file:
    path: /var/lib/msd-overlay/lower/File_Storage.img
    state: touch
    owner: root
    group: kvmd
    mode: u=rw,g=r,o=
    modification_time: preserve
    access_time: preserve

- name: add storage bind to fstab
  lineinfile:
    dest: /etc/fstab
    regexp: '^/var/lib/msd-overlay/File_Storage.img\s+/var/lib/kvmd/msd/File_Storage.img'
    line: "/var/lib/msd-overlay/File_Storage.img   /var/lib/kvmd/msd/File_Storage.img     none    bind,ro,nofail  0       0"


- name: create File Browser database
  file:
    path: /var/lib/kvmd/filebrowser.db
    state: touch
    owner: kvmd
    group: kvmd
    mode: u=rw,g=rw,o=
    modification_time: preserve
    access_time: preserve
- name: copy File Browser configuration
  copy:
    src: "{{ role_path }}/files/filebrowser.json"
    dest: /etc/kvmd/filebrowser.json
    owner: kvmd
    group: kvmd
    mode: u=rw,g=rw,o=r

- name: pull initial File Browser container
  shell: |
    set -e
    podman pull docker.io/filebrowser/filebrowser
    podman --runtime /usr/bin/crun run \
      --user={{ kvmd_user.uid }}:{{ kvmd_user.group }} -e PUID={{ kvmd_user.uid }} -e GUID={{ kvmd_user.group }} \
      -v /var/lib/storage-data:/srv \
      -v /var/lib/kvmd/filebrowser.db:/database.db \
      -v /etc/kvmd/filebrowser.json:/.filebrowser.json \
      docker.io/filebrowser/filebrowser config init
    podman --runtime /usr/bin/crun run \
      --user={{ kvmd_user.uid }}:{{ kvmd_user.group }} -e PUID={{ kvmd_user.uid }} -e GUID={{ kvmd_user.group }} \
      -v /var/lib/storage-data:/srv \
      -v /var/lib/kvmd/filebrowser.db:/database.db \
      -v /etc/kvmd/filebrowser.json:/.filebrowser.json \
      docker.io/filebrowser/filebrowser config set --auth.method=noauth
    podman --runtime /usr/bin/crun run \
      --user={{ kvmd_user.uid }}:{{ kvmd_user.group }} -e PUID={{ kvmd_user.uid }} -e GUID={{ kvmd_user.group }} \
      -v /var/lib/storage-data:/srv \
      -v /var/lib/kvmd/filebrowser.db:/database.db \
      -v /etc/kvmd/filebrowser.json:/.filebrowser.json \
      docker.io/filebrowser/filebrowser users add admin admin --perm.admin
    touch /var/lib/containers/.filebrowser-pulled
  args:
    creates: /var/lib/containers/.filebrowser-pulled

- name: create File Browser kvmd extras directory
  file:
    path: /usr/share/kvmd/extras/files
    state: directory
- name: copy File Browser kvmd extras integration
  copy:
    src: "{{ role_path }}/files/files-manifest.yaml"
    dest: /usr/share/kvmd/extras/files/manifest.yaml

- name: enable File Browser service
  systemd:
    name: filebrowser.service
    enabled: yes
    masked: no
