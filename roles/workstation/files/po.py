#!/usr/bin/python3

import dbus
import typing
import argparse
from os.path import exists


parser = argparse.ArgumentParser(description="Phone remote control interface.")
subparsers = parser.add_subparsers(dest='command')

subparsers.add_parser('input', help="start input control", aliases=['keyboard', 'key', 'control', 'k'])

subparsers.add_parser('clip', help="push clipboard", aliases=['push', 'clipto', 'c'])

subparsers.add_parser('clipfrom', help="pull clipboard", aliases=['pull'])

command_parser = subparsers.add_parser('open', help="open URI", aliases=['send', 'o'])
command_parser.add_argument('target', nargs=1, help="target to open")

command_parser = subparsers.add_parser('text', help="send text to phone clipboard")
command_parser.add_argument('text', nargs=1, help="text to send")

args = parser.parse_args()


bus = dbus.SessionBus()
obj = bus.get_object('org.gnome.Shell.Extensions.GSConnect', '/org/gnome/Shell/Extensions/GSConnect')

device_path = next(iter(obj.GetManagedObjects(dbus_interface='org.freedesktop.DBus.ObjectManager').keys()))
if not device_path:
    print("No device connected")
    exit(1)


obj = bus.get_object('org.gnome.Shell.Extensions.GSConnect', device_path)


def call_action(name: str, parameter: typing.Optional[typing.Any] = None) -> None:
    parameters: typing.List[typing.Any] = []
    if parameter is not None:
        parameters.append(parameter)

    obj.Activate(name, parameters, {}, dbus_interface='org.gtk.Actions')


if args.command in {'input', 'keyboard', 'key', 'control', 'k'}:
    call_action('keyboard')
elif args.command in {'clip', 'push', 'clipto', 'c'}:
    call_action('clipboardPush')
elif args.command in {'clipfrom', 'pull'}:
    call_action('clipboardPull')
elif args.command in 'text':
    call_action('shareText', args.text[0])
elif args.command in {'open', 'send', 'o'}:
    call_action('shareUri', args.target[0].strip())
else:
    parser.print_help()
