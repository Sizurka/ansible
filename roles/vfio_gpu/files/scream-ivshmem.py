#!/usr/bin/python3

import systemd.daemon
import libvirt
import subprocess
import time
import sys

if len(sys.argv) < 3:
    print("Invalid command line")
    exit(1)

lv = libvirt.open(None)
domain = lv.lookupByName(sys.argv[1])


def is_domain_running():
    return domain.state()[0] == libvirt.VIR_DOMAIN_RUNNING


systemd.daemon.notify("READY=1")
timeout = time.monotonic() + 30
while not is_domain_running():
    if timeout < time.monotonic():
        print("Timeout waiting for domain to start")
        exit(75)
    systemd.daemon.notify("WATCHDOG=1")
    time.sleep(1)

scream = subprocess.Popen(["scream", "-m", sys.argv[2]])
while True:
    if not is_domain_running():
        print("Domain stopped, exiting")
        break
    systemd.daemon.notify("WATCHDOG=1")
    time.sleep(1)

systemd.daemon.notify("STOPPING=1")
scream.terminate()
scream.wait()
exit(0)
