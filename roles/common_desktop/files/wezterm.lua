local wezterm = require 'wezterm'

local background_color = '#eff0f2'

return {
    hide_tab_bar_if_only_one_tab = true,
    -- Missing decorations and crashes on monitor scaling
    enable_wayland = string.lower(os.getenv("XDG_SESSION_DESKTOP") or "") ~= "gnome",

    font = wezterm.font_with_fallback {
        'JetBrains Mono',
        'Symbols Nerd Font Mono',
        'Noto Sans Mono CJK SC',
    },
    harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' },

    window_frame = {
        active_titlebar_bg = background_color,
        inactive_titlebar_bg = background_color,
        active_titlebar_fg = '#404040',
        inactive_titlebar_fg = '#a0a0a0',
        active_titlebar_border_bottom = background_color,
        inactive_titlebar_border_bottom = background_color,

        button_fg = '#a0a0a0',
        button_bg = background_color,
        button_hover_fg = '#000000',
        button_hover_bg = background_color,

        border_left_width = '0.25cell',
        border_right_width = '0.25cell',
        border_bottom_height = '0.125cell',
        border_left_color = background_color,
        border_right_color = background_color,
        border_bottom_color = background_color,
    },

    scrollback_lines = 8192,
    enable_scroll_bar = true,
    colors = {
        scrollbar_thumb = '#666666',

        tab_bar = {
            active_tab = {
                bg_color = background_color,
                fg_color = '#404040',
                intensity = 'Bold',
            },
            inactive_tab = {
                bg_color = background_color,
                fg_color = '#a0a0a0',
            },
            inactive_tab_hover = {
                bg_color = background_color,
                fg_color = '#808080',
                italic = true,
            },
            new_tab = {
                bg_color = background_color,
                fg_color = '#a0a0a0',
            },
            new_tab_hover = {
                bg_color = background_color,
                fg_color = '#808080',
                italic = true,
            },
        },
    },

    window_padding = {
        left = '0.1cell',
        right = '0.5cell',
        top = '0.05cell',
        bottom = '0.1cell',
    },

    mouse_bindings = {
        {
            event={Up={streak=1, button="Left"}},
            mods = "NONE",
            action = wezterm.action.CompleteSelection "PrimarySelection",
        },

        {
            event={Up={streak=1, button="Left"}},
            mods = "SHIFT",
            action = wezterm.action.OpenLinkAtMouseCursor,
        },
    },
}