#!/usr/bin/python3

import xml.etree.ElementTree as ET
import sys
import subprocess

if len(sys.argv) < 2:
    print("Invalid command line")
    exit(1)

domain = sys.argv[1]

bridges = dict()
for arg in sys.argv[2:]:
    parts = arg.split(':')
    bridges[parts[0]] = parts[1]

root = ET.parse("/run/libvirt/hook/" + domain)

for interface in root.findall("devices/interface/[@type='bridge']/target/[@dev]/../source/[@bridge]/.."):
    bridge = interface.find("source").get("bridge")
    if bridge not in bridges:
        continue
    tap = interface.find("target").get("dev")
    mtu = bridges[bridge]
    subprocess.call(["ip", "link", "set", "dev", tap, "mtu", mtu])
    subprocess.call(["ip", "link", "set", "dev", bridge, "mtu", mtu])
