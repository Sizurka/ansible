import asyncio
import cv2
import numpy as np

_MODEL = None


LABELS = [
    'person',
    'bag',
    'face',
]

def _get_model():
    import onnxruntime as ort

    global _MODEL
    if _MODEL is None:
        _MODEL = ort.InferenceSession("/opt/models/resnet34_peoplenet.onnx", providers=["CUDAExecutionProvider", "CPUExecutionProvider"])

    return _MODEL

def detect_objects(image, class_thresholds = None, iou_threshold = 0.5):
    model = _get_model()
    inputs = model.get_inputs()

    original_width = image.shape[1]
    original_height = image.shape[0]
    input_width = inputs[0].shape[3]
    input_height = inputs[0].shape[2]

    fx = input_width / original_width
    fy = input_height / original_height
    if abs(fx - 1.0) > 0.05 or abs(fy - 1.0) > 0.05:
        fx = fy = min(fx, fy)
        resize_width = int(original_width * fx)
        resize_height = int(original_height * fy)
        pad_width = input_width - resize_width
        pad_height = input_height - resize_height
    else:
        resize_width = input_width
        resize_height = input_height
        pad_width = 0
        pad_height = 0

    if resize_width > original_width or resize_height > original_height:
        image = cv2.resize(image, (resize_width, resize_height))
    else:
        image = cv2.resize(image, (resize_width, resize_height), interpolation=cv2.INTER_AREA)

    if pad_width or pad_height:
        image = cv2.copyMakeBorder(image, 0, pad_height, 0, pad_width, cv2.BORDER_CONSTANT, value=[0,0,0])

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_data = np.array(image) / 255.0
    image_data = np.transpose(image_data, (2, 0, 1))
    image_data = np.reshape(image_data, (1, *image_data.shape)).astype(np.float32)

    confs, boxes = model.run(None, {inputs[0].name: image_data})
    box_norm = 35.0

    centers = np.flip(
        np.stack(
            np.meshgrid(
                np.linspace(0, 33, 34), np.linspace(0, 59, 60), indexing="ij"
            )
        )
        * 16.0
        + 0.5
    , 0) / 35.0

    result = []
    for class_idx in range(len(LABELS)):
        conf_threshold = class_thresholds.get(class_idx, 0.5) if class_thresholds else 0.5
        class_boxes = boxes[0, 0+class_idx*4:4+class_idx*4]
        class_conf = confs[0, class_idx].reshape(-1)
        output_boxes = np.empty((4, 34, 60), dtype=np.float32)
        output_boxes[:2] = (class_boxes[:2] - centers) * (-box_norm)
        output_boxes[2:] = (class_boxes[2:] + centers) * box_norm
        boxes_flat = np.transpose(output_boxes, ((1, 2, 0))).reshape(-1, 4)[class_conf > conf_threshold]

        class_conf = class_conf[class_conf > conf_threshold]
        indices = cv2.dnn.NMSBoxes(boxes_flat, class_conf, conf_threshold, iou_threshold)

        for idx in indices:
            x0, y0, x1, y1 = boxes_flat[idx, :4]
            x0 = float(x0) / input_width
            y0 = float(y0) / input_height
            x1 = float(x1) / input_width
            y1 = float(y1) / input_height

            if pad_width:
                scale = 1.0 - pad_width / input_width
                x0 /= scale
                x1 /= scale
            if pad_height:
                scale = 1.0 - pad_height / input_height
                y0 /= scale
                y1 /= scale

            result.append({
                'left': x0,
                'top': y0,
                'right': x1,
                'bottom': y1,
                'label': LABELS[class_idx],
                'confidence': float(class_conf[idx]) * 100.0,
            })
    return result


_LOCK = None

async def async_detect_objects(image, class_thresholds = None, iou_threshold = 0.5):
    global _LOCK
    if _LOCK is None:
        _LOCK = asyncio.Lock()

    async with _LOCK:
        return await asyncio.get_event_loop().run_in_executor(
            None, detect_objects, image, class_thresholds, iou_threshold
        )
