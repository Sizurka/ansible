#!/usr/bin/python3

import os
from pathlib import Path


video_devices = set()
for dev in Path("/dev").iterdir():
    if not dev.name.startswith("video"):
        continue
    if not dev.is_char_device():
        continue
    if dev.is_symlink():
        continue
    video_devices.add(dev)

args = ["bwrap", "--dev-bind", "/", "/"]
for dev in video_devices:
    args.append("--dev-bind-try")
    args.append("/dev/null")
    args.append(str(dev))

args.append("discord")

os.execvp("bwrap", args)
