#!/usr/bin/python3

import sys
import time
import math
import subprocess
import calendar
import os

retain_thresholds = [
    7200,
    86400,
    7 * 86400,
    4 * 7 * 86400,
    3 * 4 * 7 * 86400,
]

snapshot_path = "/"
snapshots_directory = "/.snapshots/root"


def make_snapshot(origin, name):
    subprocess.call(["btrfs", "subvolume", "snapshot", "-r", snapshot_path, name])


def remove_snapshot(name):
    try:
        subprocess.call(["btrfs", "subvolume", "delete", name])
    except:
        pass


def classify_time(origin, now):
    age = now - origin
    prior = retain_thresholds[0]
    if age <= prior:
        return -1, None, None

    for threshold in retain_thresholds:
        if age <= threshold:
            point = int(origin / prior + 0.5)
            return 0, point, abs(origin - point * prior)
        prior = threshold

    return None, None, None


def to_time_name(now=None):
    if now is None:
        now = time.time()
    return "%04d%02d%02dT%02d%02d%02dZ" % time.gmtime(now)[0:6]


def from_time_name(name):
    parts = name.split("T")
    if len(parts) != 2:
        return None
    d = parts[0]
    t = parts[1]
    if len(d) != 8 or len(t) != 7:
        return None
    if not t.endswith("Z"):
        return None
    try:
        year = int(d[0:4])
        month = int(d[4:6])
        day = int(d[6:8])
        hour = int(t[0:2])
        minute = int(t[2:4])
        second = int(t[4:6])
        return calendar.timegm((year, month, day, hour, minute, second))
    except:
        return None


def purge_old_snapshots(directory, now=None):
    if now is None:
        now = time.time()
    inspect = []
    for name in os.listdir(directory):
        taken = from_time_name(name)
        if taken is None:
            continue

        path = directory + "/" + name

        level, index, distance = classify_time(taken, now)
        if level is None:
            remove_snapshot(path)
            continue
        if level < 0 or index is None:
            continue
        while len(inspect) <= level:
            inspect.append(dict())
        if index not in inspect[level]:
            inspect[level][index] = []
        inspect[level][index].append({
            "distance": distance,
            "path": path,
        })
    for level in inspect:
        for index in level:
            elements = level[index]
            if len(elements) <= 1:
                continue
            best = elements[0]
            for e in elements[1:]:
                if e["distance"] > best["distance"]:
                    remove_snapshot(e["path"])
                else:
                    remove_snapshot(best["path"])
                    best = e


purge_old_snapshots(snapshots_directory)
make_snapshot("/", snapshots_directory + "/" + to_time_name())
