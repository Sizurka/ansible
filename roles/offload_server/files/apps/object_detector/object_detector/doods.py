import asyncio
import cv2
import numpy as np
import starlette.status
from starlette.responses import JSONResponse
from starlette.exceptions import HTTPException
from base64 import b64decode
from .yolo import LABELS as YOLO_LABELS, async_detect_objects as yolo_detect_objects
from .peoplenet import LABELS as PEOPLENET_LABELS, async_detect_objects as peoplenet_detect_objects


async def detectors(request):
    return JSONResponse({
        'detectors': [{
            'name': 'yolo',
            'labels': YOLO_LABELS,
            'width': None,
            'height': None,
        }, {
            'name': 'peoplenet',
            'labels': PEOPLENET_LABELS,
            'width': None,
            'height': None,
        }]
    })


async def detect(request):
    data = await request.json()
    raw = np.frombuffer(b64decode(data.get('data')), dtype=np.uint8)
    img = cv2.imdecode(raw, cv2.IMREAD_COLOR)
    if img is None:
        raise HTTPException(starlette.status.HTTP_400_BAD_REQUEST, detail="Invalid image")

    filters = data.get('detect')

    detector = data.get('detector_name')
    if not detector or detector == 'default' or detector == 'yolo':
        size_limit = int(request.query_params.get('size', 512))
        detections = await yolo_detect_objects(img, size_limit)
    elif detector == 'peoplenet':
        class_thresholds = {}
        if filters:
            for class_idx in range(len(PEOPLENET_LABELS)):
                f = filters.get(PEOPLENET_LABELS[class_idx])
                if f is None:
                    f = filters.get('*')
                if f is None:
                    continue
                try:
                    class_thresholds[class_idx] = max(float(f) / 100.0, 0.01)
                except (TypeError, ValueError):
                    pass
        detections = await peoplenet_detect_objects(img, class_thresholds)
    else:
        raise HTTPException(starlette.status.HTTP_400_BAD_REQUEST, detail="Invalid detector")

    def passes_filter(detection):
        if filters is None:
            return True
        f = filters.get(detection.get('label'))
        if f is None:
            f = filters.get('*')
        if f is None:
            return False
        try:
            required_confidence = float(f)
        except (TypeError, ValueError):
            return False
        return detection['confidence'] >= required_confidence

    filtered_detections = []
    for check in detections:
        if not passes_filter(check):
            continue
        filtered_detections.append(check)
    return JSONResponse({
        'id': data.get('id'),
        'detections': filtered_detections,
    })
