#!/bin/sh

systemctl --no-block start "backup-incoming@{{ inventory_hostname }}.service" || exit 75
sleep 1
systemctl is-active "backup-incoming@{{ inventory_hostname }}.service" | grep -q activating || exit 75

if ! /usr/local/bin/backup-snapshot acquire; then
  systemctl --no-block stop "backup-incoming@{{ inventory_hostname }}.service"
  exit 1
fi

nice -n 5 \
  rsync --recursive --links --perms --owner --group --acls --xattrs --specials --times \
  --delete --delete-during --delete-excluded --one-file-system \
  --exclude-from /etc/backup/archive_exclude \
  "/.snapshots/backup/" "/mnt/archive/{{ inventory_hostname }}/"
RSYNC_STATUS="$?"

/usr/local/bin/backup-snapshot release

systemctl --no-block stop "backup-incoming@{{ inventory_hostname }}.service" || exit 2

[ "${RSYNC_STATUS}" == "24" ] && RSYNC_STATUS=0
exit "${RSYNC_STATUS}"