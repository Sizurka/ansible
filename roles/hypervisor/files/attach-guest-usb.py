#!/usr/bin/python3

import sys
import selectors
import systemd.daemon
import pyudev
import libvirt
import time

if len(sys.argv) < 2:
    print("Invalid command line")
    exit(1)

(domain, busnum, devnum) = sys.argv[1].split('/')
busnum = int(busnum)
devnum = int(devnum)

idxml = """
<hostdev mode='subsystem' type='usb'>
    <source startupPolicy='optional'>
        <address bus='{}' device='{}' />
    </source>    
</hostdev>
""".format(busnum, devnum)

udev = pyudev.Monitor.from_netlink(pyudev.Context())
udev.filter_by('usb')
selector = selectors.DefaultSelector()
selector.register(udev, selectors.EVENT_READ)

lv = libvirt.open(None)
domain = lv.lookupByName(domain)


def is_domain_running():
    return domain.state()[0] == libvirt.VIR_DOMAIN_RUNNING


# Wait for enumeration to finish
time.sleep(0.5)

if not is_domain_running():
    systemd.daemon.notify("READY=1")
    exit(75)
domain.attachDeviceFlags(idxml, libvirt.VIR_DOMAIN_DEVICE_MODIFY_LIVE)


def is_monitor_device(device):
    try:
        bus = int(device.properties["BUSNUM"])
    except:
        return False
    try:
        dev = int(device.properties["DEVNUM"])
    except:
        return False
    return bus == busnum and dev == devnum


def update_udev():
    while True:
        device = udev.poll(0)
        if device is None:
            return True
        if device.action == 'remove' and is_monitor_device(device):
            return False


systemd.daemon.notify("READY=1")
while True:
    if not is_domain_running():
        print("Domain stopped, exiting")
        exit(0)
    systemd.daemon.notify("WATCHDOG=1")
    if not update_udev():
        print("Device removed, detaching from domain")
        break
    selector.select(1.0)

systemd.daemon.notify("STOPPING=1")
domain.detachDeviceFlags(idxml, libvirt.VIR_DOMAIN_DEVICE_MODIFY_LIVE)
exit(0)
