if [ -n "$HOME" ] && [ -d "$HOME/.cargo/bin" ]; then
  append_path "$HOME/.cargo/bin"
fi