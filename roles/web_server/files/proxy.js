function FindProxyForURL(url, host) {
    var excluded = [
        "{{ domain_name }}",
        "google.com",
        "gmail.com",
        "gstatic.com",
        "googleapis.com",
        "youtube.com",
        "youtu.be",
        "ytimg.com",
        "googlevideo.com",
        "elevationscu.com",
        "elevationsbanking.com",
        "cu.edu",
        "colorado.edu",
        "discord.com",
        "steampowered.com",
        "steamcommunity.com",
        "steamcontent.com",
        "battle.net",
        "thunder.cmdl.noaa.gov",
        "amazon.com",
        "newegg.com"
    ];
    for (var i=0, len=excluded.length; i < len; i++) {
        var ex = excluded[i];
        if (shExpMatch(host, ex)) {
            return "DIRECT";
        }
        if (shExpMatch(host, "*." + ex)) {
            return "DIRECT";
        }
    }
    if (isPlainHostName(host)) {
        return "DIRECT";
    }
    if (shExpMatch(host, "*.local")) {
        return "DIRECT";
    }

    // {{ "\n" }} {% if network.uplink.ipv4.gateway is defined %}
    if (isInNet(host, "{{ network.uplink.ipv4.gateway }}", "255.255.255.255")) {
        return "DIRECT";
    }
    // {% endif %} {{ "\n" }}
    // {{ "\n" }} {% if network.uplink.ipv4.external is defined %} {% for addr in network.uplink.ipv4.external %}
    if (isInNet(host, "{{ addr }}", "255.255.255.255")) {
        return "DIRECT";
    }
    // {% endfor %} {% endif %} {{ "\n" }}
    

    if (isInNet(host, "10.0.0.0", "255.0.0.0")) {
        return "DIRECT";
    }
    if (isInNet(host, "192.168.0.0", "255.255.0.0")) {
        return "DIRECT";
    }
    if (isInNet(host, "172.16.0.0", "255.240.0.0")) {
        return "DIRECT";
    }
    if (isInNet(host, "169.254.0.0", "255.255.0.0")) {
        return "DIRECT";
    }
    if (isInNet(host, "127.0.0.1", "255.0.0.0")) {
        return "DIRECT";
    }

    return "SOCKS5 sphinx:1080";
}

function FindProxyForURLEx(url, host) {
    // {{ "\n" }}{% if network.uplink.ipv6.gateway is defined %}
    if (isInNetEx(host, "{{ network.uplink.ipv6.gateway }}/128")) {
        return "DIRECT";
    }
    // {% endif %} {{ "\n" }}
    // {{ "\n" }} {% if network.uplink.ipv6.external is defined %} {% for addr in network.uplink.ipv6.external %}
    if (isInNetEx(host, "{{ addr }}/128")) {
        return "DIRECT";
    }
    // {% endfor %} {% endif %} {{ "\n" }}
    
    if (isInNetEx(host, "{{ network.internal.ipv6 }}")) {
        return "DIRECT";
    }

    if (isInNetEx(host, "fd00::/8")) {
        return "DIRECT";
    }
    if (isInNetEx(host, "fe00::/8")) {
        return "DIRECT";
    }
    if (isInNetEx(host, "::1/128")) {
        return "DIRECT";
    }
    return FindProxyForURL(url, host);
}
