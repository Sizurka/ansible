#!/usr/bin/python3

import argparse
import struct
import usb.core
import usb.util
import usb.backend.libusb1


parser = argparse.ArgumentParser(description="USB power switch control.")
parser.add_argument('--on',
                    dest='on', type=int,
                    help="output to set on",
                    nargs='*', action='extend')
parser.add_argument('--off',
                    dest='off', type=int,
                    help="output to set off",
                    nargs='*', action='extend')
parser.add_argument('--status',
                    dest='status', action='store_true',
                    help="read status voltage")
parser.add_argument('--bootloader',
                    dest='bootloader', action='store_true',
                    help="reboot into bootloader mode")
parser.add_argument('--serial-number',
                    dest='serial_number',
                    help="match serial number")
args = parser.parse_args()


def match_device(dev):
    return usb.util.get_string(dev, dev.iSerialNumber) == args.serial_number


device = usb.core.find(idVendor=0x2e8a, idProduct=0xa43e, custom_match=(match_device if args.serial_number else None))
if device is None:
    raise ValueError('Device not found')


device.set_configuration()

mask = 0
set = 0
if args.on:
    for n in args.on:
        n -= 1
        mask |= (1 << n)
        set |= (1 << n)
if args.off:
    for n in args.off:
        n -= 1
        mask |= (1 << n)
        set &= ~(1 << n)
if mask != 0:
    device.ctrl_transfer(usb.util.CTRL_OUT | usb.util.CTRL_TYPE_VENDOR, 0x00, 0, 0, struct.pack('<BB', mask, set))


if args.status:
    supply_mV = device.ctrl_transfer(usb.util.CTRL_IN | usb.util.CTRL_TYPE_VENDOR, 0x02, 0, 0, 4)
    supply_mV = struct.unpack('<I', supply_mV)[0]
    print(f"Supply voltage: {supply_mV/1000.0:.2f} V")
    output_state = device.ctrl_transfer(usb.util.CTRL_IN | usb.util.CTRL_TYPE_VENDOR, 0x01, 0, 0, 2)
    (enabled, error) = struct.unpack('<BB', output_state)
    for i in range(4):
        print(f"Output {i+1}: {'ON ' if enabled & (1<<i) else 'OFF'} {'OVER CURRENT' if error & (1<<i) else ''}")


if args.bootloader:
    try:
        device.ctrl_transfer(usb.util.CTRL_OUT | usb.util.CTRL_TYPE_VENDOR, 0xAB, 0, 0, b'')
    except:
        pass