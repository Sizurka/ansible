#!/usr/bin/python3

import asyncio
import aiohttp
import systemd.daemon
import signal
import json
import platform
import traceback
import pathlib
import hashlib
import base64
import psutil
import dbus
import threading
import os
import re


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()

root_id = platform.node().split('.', 1)[0]


class Averager:
    def __init__(self):
        self.s = 0
        self.c = 0

    def clear(self):
        self.s = 0
        self.c = 0

    def __call__(self, v):
        self.s += v
        self.c += 1

    @property
    def value(self):
        if not self.c:
            return None
        return self.s / self.c


scale_ki = 1.0/1024
scale_mi = 1.0/(1024*1024)


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
_background_tasks = set()
def background_task(coro):
    r = asyncio.get_event_loop().create_task(coro)
    _background_tasks.add(r)
    r.add_done_callback(lambda task: _background_tasks.discard(r))
    return r
http_session = None


async def update_state(parameter, state, attributes=None):
    global http_session
    if http_session is None:
        http_session = aiohttp.ClientSession()

    if type(state) is float:
        state = "{:.2f}".format(state)
    else:
        state = str(state)

    entity_id = root_id + "." + parameter
    content = {
        "state": state,
    }
    if attributes is not None:
        content["attributes"] = attributes
    try:
        await http_session.post("https://ha.{{ domain_name }}/api/states/" + entity_id,
                                data=json.dumps(content),
                                headers={
                                    'Authorization': 'Bearer ' + api_key,
                                    'Content-Type': 'application/json',
                                })
    except:
        traceback.print_exc()


class UIDGenerator:
    def __init__(self):
        self._h = hashlib.shake_128()

    def __iadd__(self, other):
        if other is None:
            return self
        try:
            self._h.update(other)
            return self
        except TypeError:
            pass
        self._h.update(other.encode())
        return self

    def __str__(self):
        return base64.b32encode(self._h.digest(5)).decode().lower()


def add_simple_poll(file, parameter, attributes=None, scale=1.0, rate=10.0, convert=None):
    if convert is None:
        convert = lambda value: float(value.strip())

    async def poll():
        while True:
            await asyncio.sleep(rate)
            try:
                with open(file) as f:
                    value = f.read()
                value = convert(value)
            except (IOError, ValueError):
                continue
            await update_state(parameter, value * scale, attributes)
    background_task(poll())


def add_aggregate_poll(files, parameter, attributes=None, scale=1.0, rate=10.0, convert=None):
    if isinstance(convert, str):
        cnvt_key = convert
        def cnvt(values):
            return float(values[cnvt_key].strip())
        convert = cnvt
    elif convert is None:
        convert = lambda values: max([float(v.strip()) for v in values.values()])

    async def poll():
        values = dict()
        while True:
            await asyncio.sleep(rate)
            values.clear()
            for key, file in files.items():
                try:
                    with open(file) as f:
                        value = f.read()
                except IOError:
                    continue
                values[key] = value
            try:
                value = convert(values)
            except (ValueError, KeyError, IndexError):
                continue
            effective_attributes = attributes
            if callable(effective_attributes):
                effective_attributes = effective_attributes(values)
            await update_state(parameter, value * scale, effective_attributes)
    background_task(poll())


def add_accumulated_rate_poll(file, parameter, attributes=None, scale=1.0, rate=10.0, convert=None, decimals=4):
    if convert is None:
        convert = lambda value: int(value.strip())

    format_code = "{:.%df}" % decimals

    async def poll():
        prior_total = None
        prior_time = None

        while True:
            await asyncio.sleep(rate)
            try:
                with open(file) as f:
                    total = f.read()
                total = convert(total)
            except (IOError, ValueError):
                continue
            now = loop.time()
            if prior_time is not None and prior_total is not None:
                delta_value = total - prior_total
                delta_time = now - prior_time
                if delta_value >= 0:
                    await update_state(parameter, format_code.format((delta_value / delta_time) * scale), attributes)
            prior_time = now
            prior_total = total
    background_task(poll())


for ifname in pathlib.Path("/sys/class/net").iterdir():
    if ifname.name == "lo":
        continue
    if ifname.name.startswith("vnet"):
        continue
    if ifname.name.startswith("macvtap"):
        continue
    if (ifname / "bridge").exists():
        continue
    if not (ifname / "device").exists():
        continue
    rx = ifname / "statistics" / "rx_bytes"
    name = ifname.name.replace('-', '').lower()
    if rx.exists():
        add_accumulated_rate_poll(rx, "net_" + name + "_rx_rate", {
            "friendly_name": "{} Receive".format(ifname.name.upper()),
            "icon": "mdi:download-network",
            "unit_of_measurement": "MiB/s",
        }, scale=scale_mi)
    tx = ifname / "statistics" / "tx_bytes"
    if tx.exists():
        add_accumulated_rate_poll(tx, "net_" + name + "_tx_rate", {
            "friendly_name": "{} Transmit".format(ifname.name.upper()),
            "icon": "mdi:upload-network",
            "unit_of_measurement": "MiB/s",
        }, scale=scale_mi)


cpu_temperatures = list()
for hwmon in pathlib.Path("/sys/class/hwmon").iterdir():
    name = hwmon / "name"
    try:
        with name.open('r') as f:
            name = f.read()
        name = name.strip()
    except IOError:
        name = None

    def find_label(label):
        for check in hwmon.glob('temp*_label'):
            try:
                with check.open('r') as f:
                    if f.read().strip() != label:
                        continue
                return check.parent / (check.name[:-6] + '_input')
            except IOError:
                continue
        return None

    if name == "k10temp":
        check = find_label('Tctl')
        if not check:
            check = find_label('Tdie')
        if check:
            cpu_temperatures.append(check)
    elif name == "coretemp":
        cpu_temperatures.append(hwmon / "temp1_input")
add_aggregate_poll({i: cpu_temperatures[i] for i in range(len(cpu_temperatures))}, "cpu_temperature", {
    "friendly_name": "CPU Temperature",
    "icon": "mdi:memory",
    "unit_of_measurement": "°C",
}, scale=0.001)


if pathlib.Path("/sys/class/nvme").exists():
    for dev in pathlib.Path("/sys/class/nvme").iterdir():
        try:
            with open(dev / "model") as f:
                model = f.read().strip()
        except IOError:
            model = None
        try:
            with open(dev / "serial") as f:
                serial = f.read().strip()
        except IOError:
            serial = None
        pci_device = (dev / "device").resolve().name
        for block in dev.glob("nvme*/stat"):
            try:
                with open(block.parent / "wwid") as f:
                    identity = f.read().strip()
            except IOError:
                identity = None
            device_name = block.parent.name

            uid = UIDGenerator()
            uid += identity
            uid += model
            uid += serial
            uid = str(uid)
            add_accumulated_rate_poll(block, "block_" + uid + "_read_rate", {
                "friendly_name": "{} Read".format(block.parent.name.upper()),
                "icon": "mdi:harddisk",
                "unit_of_measurement": "MiB/s",
                "device_type": "nvme",
                "model": model,
                "serial": serial,
                "identity": identity,
                "pci_device": pci_device,
                "kernel_device": device_name,
            }, scale=512 * scale_mi, convert=lambda value: int(value.strip().split()[2]))
            add_accumulated_rate_poll(block, "block_" + uid + "_write_rate", {
                "friendly_name": "{} Write".format(block.parent.name.upper()),
                "icon": "mdi:harddisk",
                "unit_of_measurement": "MiB/s",
                "device_type": "nvme",
                "model": model,
                "serial": serial,
                "identity": identity,
                "pci_device": pci_device,
                "kernel_device": device_name,
            }, scale=512 * scale_mi, convert=lambda value: int(value.strip().split()[6]))
            add_accumulated_rate_poll(block, "block_" + uid + "_utilization", {
                "friendly_name": "{} Utilization".format(block.parent.name.upper()),
                "icon": "mdi:harddisk",
                "unit_of_measurement": "%",
                "device_type": "nvme",
                "model": model,
                "serial": serial,
                "identity": identity,
                "pci_device": pci_device,
                "kernel_device": device_name,
            }, scale=100 * 0.001, convert=lambda value: int(value.strip().split()[9]))

if pathlib.Path("/sys/class/scsi_disk").exists():
    wwn_lookup = dict()
    for link in pathlib.Path("/dev/disk/by-id").iterdir():
        if not link.name.startswith("wwn-"):
            continue
        components = link.name.split("-")
        if len(components) != 2:
            continue
        wwn = components[1]
        if wwn.startswith("0x"):
            wwn = wwn[2:]
        dev = link.resolve().name
        wwn_lookup[dev] = wwn
    for block in pathlib.Path("/sys/class/scsi_disk").glob("*/device/block/*/stat"):
        # Only consider SATA devices (i.e. ignore USB)
        if not (block.parent.resolve().parent.parent.parent.parent.parent / "ata_port").exists():
            continue
        # Ignore rotational devices (assume they're removable)
        try:
            with open(block.parent / "queue" / "rotational") as f:
                if int(f.read().strip()) == 1:
                    continue
        except (IOError, TypeError):
            pass

        dev = block.parent.parent.parent
        scsi_device = dev.parent.name
        try:
            with open(dev / "model") as f:
                model = f.read().strip()
        except IOError:
            model = None
        try:
            with open(dev / "wwid") as f:
                fields = f.read().strip().split()
                serial = fields[-1]
                identity = " ".join(fields)
        except IOError:
            identity = None
            serial = None
        device_name = block.parent.name
        wwn = wwn_lookup.get(device_name, None)

        uid = UIDGenerator()
        uid += wwn
        if wwn is None:
            uid += identity
            if identity is None:
                uid += scsi_device
        uid += model
        uid += serial
        uid = str(uid)
        add_accumulated_rate_poll(block, "block_" + uid + "_read_rate", {
            "friendly_name": "{} Read".format(block.parent.name.upper()),
            "icon": "mdi:harddisk",
            "unit_of_measurement": "MiB/s",
            "device_type": "scsi",
            "model": model,
            "serial": serial,
            "identity": identity,
            "wwn": wwn,
            "scsi_device": scsi_device,
            "kernel_device": device_name,
        }, scale=512 * scale_mi, convert=lambda value: int(value.strip().split()[2]))
        add_accumulated_rate_poll(block, "block_" + uid + "_write_rate", {
            "friendly_name": "{} Write".format(block.parent.name.upper()),
            "icon": "mdi:harddisk",
            "unit_of_measurement": "MiB/s",
            "device_type": "scsi",
            "model": model,
            "serial": serial,
            "identity": identity,
            "wwn": wwn,
            "scsi_device": scsi_device,
            "kernel_device": device_name,
        }, scale=512 * scale_mi, convert=lambda value: int(value.strip().split()[6]))
        add_accumulated_rate_poll(block, "block_" + uid + "_utilization", {
            "friendly_name": "{} Utilization".format(block.parent.name.upper()),
            "icon": "mdi:harddisk",
            "unit_of_measurement": "%",
            "device_type": "scsi",
            "model": model,
            "serial": serial,
            "identity": identity,
            "wwn": wwn,
            "scsi_device": scsi_device,
            "kernel_device": device_name,
        }, scale=100 * 0.001, convert=lambda value: int(value.strip().split()[9]))


async def poll_cpu_utilization():
    psutil.cpu_percent(percpu=True)
    while True:
        await asyncio.sleep(10)
        cpus = psutil.cpu_percent(percpu=True)
        await update_state("cpu_utilization", sum(cpus) / len(cpus), {
            "friendly_name": "CPU Utilization",
            "icon": "mdi:memory",
            "unit_of_measurement": "%",
            "core_utilization": [round(c, 2) for c in cpus],
        })
background_task(poll_cpu_utilization())


async def poll_memory_utilization():
    while True:
        await asyncio.sleep(10)
        mem = psutil.virtual_memory()
        await update_state("ram_utilization", mem.percent, {
            "friendly_name": "RAM Utilization",
            "icon": "mdi:memory",
            "unit_of_measurement": "%",
            "free_bytes": mem.free,
        })
background_task(poll_memory_utilization())


def available_gpus():
    candidates = list()
    for check in pathlib.Path("/sys/class/drm").iterdir():
        if not check.is_dir():
            continue
        matched = re.fullmatch(r"card(\d+)", check.name)
        if not matched:
            continue
        card_index = int(matched.group(1))

        driver = check / "device" / "driver"
        if driver.is_symlink() and os.readlink(driver).endswith("/nvidia"):
            yield check
            return
        has_mclk = (check / "device" / "pp_dpm_mclk").exists()
        candidates.append((card_index, has_mclk, check))
    candidates.sort(key=lambda x: (0 if x[1] else 1, x[0]))
    for _, _, path in candidates:
        yield path


for gpu_drm in available_gpus():
    if (gpu_drm / "device" / "driver").is_symlink() and \
            os.readlink(gpu_drm / "device" / "driver").endswith("/nvidia"):
        gpu_power = Averager()
        gpu_temperature = Averager()
        memory_temperature = Averager()
        gpu_utilization = Averager()
        memory_utilization = Averager()
        async def output_smi():
            await asyncio.sleep(30)
            while True:
                await asyncio.sleep(10)
                if (temperature := gpu_temperature.value) is not None:
                    attributes = {
                        "friendly_name": "GPU Temperature",
                        "icon": "mdi:expansion-card",
                        "unit_of_measurement": "°C",
                    }
                    if (mtemperature := memory_temperature.value) is not None:
                        attributes["memory_temperature"] = round(mtemperature)
                    if (power := gpu_power.value) is not None:
                        attributes["power"] = round(power)
                    await update_state("gpu_temperature", temperature, attributes)
                if (utilization := gpu_utilization.value) is not None:
                    attributes = {
                        "friendly_name": "GPU Utilization",
                        "icon": "mdi:expansion-card",
                        "unit_of_measurement": "%",
                    }
                    if (memutil := memory_utilization.value) is not None:
                        attributes["memory_utilization"] = round(memutil)
                    await update_state("gpu_utilization", utilization, attributes)

                for c in (gpu_power, gpu_temperature, memory_temperature, gpu_utilization, memory_utilization):
                    c.clear()
        background_task(output_smi())

        async def read_smi():
            await asyncio.sleep(30)
            nvidia_smi = None
            while True:
                if not nvidia_smi:
                    nvidia_smi = await asyncio.create_subprocess_exec("nvidia-smi", "dmon",
                                                                      stdout=asyncio.subprocess.PIPE,
                                                                      stdin=asyncio.subprocess.DEVNULL)
                line = await nvidia_smi.stdout.readline()
                if not line:
                    try:
                        nvidia_smi.terminate()
                    except:
                        pass
                    try:
                        await nvidia_smi.wait()
                    except:
                        pass
                    nvidia_smi = None
                    await asyncio.sleep(5)
                    continue

                line = line.decode("ascii").strip()
                if line.startswith('#'):
                    continue
                fields = line.split()
                for index, target in (
                        (1, gpu_power),
                        (2, gpu_temperature),
                        (3, memory_temperature),
                        (4, gpu_utilization),
                        (5, memory_utilization),
                ):
                    try:
                        target(float(fields[index]))
                    except (ValueError, IndexError):
                        pass
            await nvidia_smi.communicate()
        background_task(read_smi())
    elif (gpu_drm / "device" / "hwmon").exists() or (gpu_drm / "device" / "gpu_busy_percent").exists():
        if (gpu_drm / "device" / "hwmon").exists():
            poll_sources = dict()
            for hwmon in (gpu_drm / "device" / "hwmon").iterdir():
                def find_label(label):
                    for check in hwmon.glob('temp*_label'):
                        try:
                            with check.open('r') as f:
                                if f.read().strip() != label:
                                    continue
                            return check.parent / (check.name[:-6] + '_input')
                        except IOError:
                            continue
                    return None

                if add := find_label("edge"):
                    poll_sources["temperature"] = add
                if add := find_label("mem"):
                    poll_sources["memory_temperature"] = add

                poll_sources["power"] = hwmon / "power1_average"

            def attrs(x):
                attributes = {
                    "friendly_name": "GPU Temperature",
                    "icon": "mdi:expansion-card",
                    "unit_of_measurement": "°C",
                }
                if (v := x.get("memory_temperature")) is not None:
                    attributes["memory_temperature"] = round(float(v.strip()) * 0.001)
                if (v := x.get("power")) is not None:
                    attributes["power"] = round(float(v.strip()) * 1E-6)
                return attributes

            add_aggregate_poll(poll_sources, "gpu_temperature", attrs,
                               convert="temperature", scale=0.001)
        if (gpu_drm / "device" / "gpu_busy_percent").exists():
            def attrs(x):
                attributes = {
                    "friendly_name": "GPU Utilization",
                    "icon": "mdi:expansion-card",
                    "unit_of_measurement": "%",
                }
                if (v := x.get("memory")) is not None:
                    attributes["memory_utilization"] = round(float(v.strip()))
                return attributes

            add_aggregate_poll({
                "utilization": (gpu_drm / "device" / "gpu_busy_percent"),
                "memory": (gpu_drm / "device" / "mem_busy_percent"),
            }, "gpu_utilization", attrs, convert="utilization")


async def poll_failed_units():
    bus = dbus.SystemBus()
    proxy = bus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
    while True:
        await asyncio.sleep(30)
        try:
            failed = proxy.ListUnitsFiltered(['failed'], dbus_interface='org.freedesktop.systemd1.Manager')
        except dbus.DBusException:
            traceback.print_exc()
            continue
        details = []
        for f in failed:
            # snapd update process breaks systemd mount status by deleting the unit files while still lazy mounted
            if f[2] == 'not-found' and f[0].startswith('var-lib-snapd-snap-') and f[0].endswith('.mount'):
                continue
            details.append({
                "name": f[0],
                "description": f[1],
                "sub_state": f[4],
            })
        await update_state("failed_services", len(details), {
            "friendly_name": "Failed Service Count",
            "icon": "mdi:server-off",
            "failed": details,
        })
background_task(poll_failed_units())

# {% if cm_psu +%}
class CMPSUReport(threading.Thread):
    def __init__(self):
        super().__init__(name="PSU reporting", daemon=True)
        self.lock = threading.Lock()
        self.input_power = Averager()
        self.output_power = Averager()
        self.temperature = Averager()
        self.fan_rpm = Averager()

        background_task(self.do_report())

    def run(self):
        import usb.core
        import usb.util
        import usb.backend.libusb1
        import struct

        for pid in (0x01A3, 0x01A5):
            dev = usb.core.find(idVendor=0x2516, idProduct=pid)
            if dev is not None:
                break
        else:
            return

        try:
            dev.detach_kernel_driver(0)
        except usb.core.USBError:
            pass
        try:
            dev.reset()
        except usb.core.USBError:
            pass

        dev.ctrl_transfer(0x21, 0x0A, 0x0000, 0, 0)
        dev.ctrl_transfer(0x21, 0x09, 0x0202, 0, b"\x02\x04")
        dev.clear_halt(0x81)

        while True:
            data = dev.read(0x81, 64, timeout=5000)
            if not data:
                continue
            data = bytes(data)
            if len(data) < 2:
                continue

            report_type = data[0]
            data = data[1:]

            if report_type == 0x04:
                if len(data) >= 1:
                    temperature = float(struct.unpack("<B", data[:1])[0])
                    with self.lock:
                        self.temperature(temperature)
            elif report_type == 0x06:
                if len(data) >= 2:
                    rpm = float(struct.unpack("<H", data[:2])[0])
                    with self.lock:
                        self.fan_rpm(rpm)
            elif report_type == 0x03:
                if len(data) >= 22:
                    (
                        _,  # Something efficiency related ? (166-106)
                        _,  # Constant ? (4)
                        _,  # Something efficiency related ? (6-10)
                        input_power, output_power,
                        v12, i12, p12,
                        v3, i3, p3,
                        v5, i5, p5,
                    ) = struct.unpack("<BBBHHBHHBHHBHH", data[:23])
                    input_power = float(input_power)
                    output_power = float(output_power)
                    # v12 = float(v12) * 0.1
                    # i12 = float(i12) * 0.1
                    # p12 = float(p12)
                    # v3 = float(v3) * 0.1
                    # i3 = float(i3) * 0.1
                    # p3 = float(p3)
                    # v5 = float(v5) * 0.1
                    # i5 = float(i5) * 0.1
                    # p5 = float(p5)
                    with self.lock:
                        self.input_power(input_power)
                        self.output_power(output_power)

    async def do_report(self):
        while True:
            await asyncio.sleep(10)

            report_tasks = []
            with self.lock:
                if (psu_temp := self.temperature.value) is not None:
                    psu_temp = "{:.1f}".format(psu_temp)
                    attributes = {
                        "friendly_name": "PSU Temperature",
                        "icon": "mdi:power-socket",
                        "unit_of_measurement": "°C",
                    }
                    if (rpm := self.fan_rpm.value) is not None:
                        attributes["fan_rpm"] = round(rpm)
                    report_tasks.append(update_state("psu_temperature", psu_temp, attributes))
                if (power := self.input_power.value) is not None:
                    power =  "{:.0f}".format(power)
                    attributes = {
                        "friendly_name": "System Power",
                        "icon": "mdi:power-socket",
                        "unit_of_measurement": "W",
                    }
                    if (opower := self.output_power.value) is not None:
                        attributes["output_power"] = round(opower)
                    report_tasks.append(update_state("system_power", power, attributes))
                for c in (self.temperature, self.fan_rpm, self.input_power, self.output_power):
                    c.clear()
            await asyncio.gather(*report_tasks)
cm_psu = CMPSUReport()
cm_psu.start()
# {% endif +%}

for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGHUP):
    loop.add_signal_handler(sig, lambda: loop.stop())


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


main_t = loop.create_task(main())
loop.run_forever()
