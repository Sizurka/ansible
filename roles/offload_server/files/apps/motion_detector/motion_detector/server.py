import asyncio
import numpy as np
import cv2
from base64 import b64decode
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route
from .detector import detect_motion


async def _detect(request):
    data = await request.json()
    before = np.frombuffer(b64decode(data['before']), dtype=np.uint8)
    before = cv2.imdecode(before, cv2.IMREAD_COLOR)
    after = np.frombuffer(b64decode(data['after']), dtype=np.uint8)
    after = cv2.imdecode(after, cv2.IMREAD_COLOR)
    mask = data.get('mask')
    if mask is not None:
        mask = np.frombuffer(b64decode(mask), dtype=np.uint8)
        mask = cv2.imdecode(mask, cv2.IMREAD_COLOR)

    change_threshold = float(data.get('change_threshold', 0.1))
    detect_threshold = float(data.get('detect_threshold', 0.01))
    size_limit = int(data.get('size_limit', 512))

    changed = await asyncio.get_event_loop().run_in_executor(
        None, detect_motion, before, after, mask,
        change_threshold, size_limit
    )

    return JSONResponse({'motion': changed > detect_threshold, 'changed': changed})


app = Starlette(routes=[
    Route('/detect', endpoint=_detect, methods=['POST']),
])
