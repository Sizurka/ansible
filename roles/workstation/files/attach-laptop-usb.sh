#!/bin/bash

if [ `whoami` != "root" ]; then
  exec sudo "$0"
fi

exec /usr/local/bin/attach-usbip \
  --device-list /etc/remote-usb.conf \
  --ssh 'dragon.{{ domain_name }}'
