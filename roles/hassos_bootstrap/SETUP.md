# Docker Container Entry

Via serial console:
```shell
docker exec -it homeassistant bash
```

# Post Install Setup

Cannot write to configuration.yaml before onboarding (overwritten).

After onboarding, add to configuration:
```yaml
http:
  use_x_forwarded_for: true
  trusted_proxies: 10.200.1.11  # kirin.inthat.cloud
```

# Initial Setup

1. Change to advanced mode (User Profile -> Scroll Down -> Advanced Mode).
2. Install ESPHome: (Settings -> Add-ons -> ESPHome).
3. Install SSH access:
   1. Settings -> Add-ons -> SSH
   2. Configuration, add SSH keys (kitsune, chimera control)
   3. Configuration, add SSH port (22).
4. Install [HACS](https://my.home-assistant.io/redirect/supervisor_addon/?repository_url=https%3A%2F%2Fgithub.com%2Fhacs%2Faddons&addon=cb646a50_get)
   1. [ESPSomfy-RTS](https://github.com/rstrouse/ESPSomfy-RTS-HA)
   2. [EcoFlow Cloud](https://github.com/tolwi/hassio-ecoflow-cloud)
   3. [GTFS2](https://github.com/vingerha/gtfs2)
      4. [Main](https://www.rtd-denver.com/open-records/open-spatial-information/gtfs)
      5. [Realtime](https://www.rtd-denver.com/open-records/open-spatial-information/real-time-feeds)
   4. [Ingress](https://github.com/lovelylain/hass_ingress)
 