#!/bin/bash
set -e

LINUX="/vmlinuz"
INITRD="/initrd.img"

if [ ! -r "${LINUX}" ]; then LINUX="/boot/vmlinuz-linux"; fi
if [ ! -r "${INITRD}" ]; then INITRD="/boot/initramfs-linux.img"; fi

"{{ ukify_location }}" \
  --linux="${LINUX}" --initrd="${INITRD}" \
  --cmdline="@/etc/kernel/efi-command-line.txt" \
  --output="/boot/linux-image.efi"\
  --secureboot-private-key="/root/keys/efi/db.key" \
  --secureboot-certificate="/root/keys/efi/db.crt" \
  build

{% if not (tpm_security == "secureboot" or tpm_security == "customshim") %}
systemctl enable update-tpm.service || true
{% endif %}
