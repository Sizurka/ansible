# Installation
 * Use `viostor` driver for root device.
 * "I don't have internet" to avoid Microsoft account creation.
    * Set user to "Derek Hageman".
 * Leave password blank (skips security questions).
 * Disable all telemetry settings.

# Post Install
 1. Device Manager -> Show Hidden
    1. "Ethernet Controller" set to `NetKVM` driver.
       1.  Set MTU.
 1. Disable proxy autodetection (search for "Proxy").
 1. Windows Update.
 1. Install guest tools from virtio ISO.
 
# SSH Access
[Microsoft Article](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)
 1. Admin Powershell.
    1. Check versions: `Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'`
    1. `Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0`
    1. `Start-Service sshd`
    1. `Set-Service -Name sshd -StartupType 'Automatic'`
    1. Check firewall: `Get-NetFirewallRule -Name *ssh*`
        1. Missing: `New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22`
    1. `New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force`
    1. `scp hageman@kitsune.inthat.cloud:.ssh/id_ed25519.pub c:\ProgramData\ssh\administrators_authorized_keys`
    1. `New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShellCommandOption -Value "/c" -PropertyType String -Force`
    1. `icacls C:\ProgramData\ssh\administrators_authorized_keys /remove "NT AUTHORITY\Authenticated Users"`
    1. `icacls C:\ProgramData\ssh\administrators_authorized_keys /inheritance:r`
    1. `icacls C:\ProgramData\ssh\administrators_authorized_keys /grant Administrators:rw`
    1. `icacls C:\ProgramData\ssh\administrators_authorized_keys /grant SYSTEM:r`
