#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/watchdog.h>
#include <linux/usb.h>

static unsigned int timeout;

struct usbwatchdog {
    struct mutex mutex;
    struct usb_device *udev;
    struct usb_interface *interface;
    struct watchdog_device wdt_dev;
    int present;
};


static int usbwatchdog_command(struct watchdog_device *wdt_dev, __u8 command, __u32 data)
{
    struct usbwatchdog *dev = watchdog_get_drvdata(wdt_dev);
    int ret = 0;

    data = cpu_to_le32(data);

    mutex_lock(&dev->mutex);
    if (!dev->present) {
        ret = -ENODEV;
        goto out;
    }

    ret = usb_control_msg_send(dev->udev,
                               usb_sndctrlpipe(dev->udev, 0),
                               command,
                               USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
                               0,
                               0,
                               &data, 4,
                               500,
                               GFP_KERNEL);

    if (ret) {
        dev_err(wdt_dev->parent, "USB command error: %d", ret);
    }

out:
    mutex_unlock(&dev->mutex);
    return ret;
}


static int usbwatchdog_stop(struct watchdog_device *wdt_dev)
{
    dev_dbg(wdt_dev->parent, "USB watchdog stopping");
    return usbwatchdog_command(wdt_dev, 0x01, 0xd2b245ab);
}

static int usbwatchdog_ping(struct watchdog_device *wdt_dev)
{
    dev_dbg(wdt_dev->parent, "USB watchdog ping");
    return usbwatchdog_command(wdt_dev, 0x02, 0x3bd70854);
}

static unsigned int usbwatchdog_get_timeleft(struct watchdog_device *wdt_dev)
{
    struct usbwatchdog *dev = watchdog_get_drvdata(wdt_dev);
    unsigned int result;

    mutex_lock(&dev->mutex);
    if (!dev->present) {
        result = 0;
        goto out;
    }

    if (usb_control_msg_recv(dev->udev,
                             usb_rcvctrlpipe(dev->udev, 0),
                             0x04,
                             USB_DIR_IN | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
                             0,
                             0,
                             &result, 4,
                             500,
                             GFP_KERNEL)) {
        result = 0;
        goto out;
    }

    result = le32_to_cpu(result);
    dev_dbg(wdt_dev->parent, "USB watchdog get time left: %u ms", result);

    if (result) {
        result /= 1000;
        if (result == 0)
            result = 1;
    }

out:
    mutex_unlock(&dev->mutex);
    return result;
}

static int usbwatchdog_set_timeout(struct watchdog_device *wdt_dev,
                                   unsigned int timeout)
{
    int ret;
    if (timeout < wdt_dev->min_timeout || timeout > wdt_dev->max_timeout) {
        dev_dbg(wdt_dev->parent, "USB watchdog timeout %u out of range", timeout);
        return -EINVAL;
    }

    dev_dbg(wdt_dev->parent, "USB watchdog set timeout: %u s", timeout);
    ret = usbwatchdog_command(wdt_dev, 0x03, timeout * 1000);
    if (ret) {
        return ret;
    }

    wdt_dev->timeout = timeout;
    return 0;
}

static int usbwatchdog_start(struct watchdog_device *wdt_dev)
{
    int ret;

    dev_dbg(wdt_dev->parent, "USB watchdog starting (timeout=%d)", wdt_dev->timeout);

    ret = usbwatchdog_set_timeout(wdt_dev, wdt_dev->timeout);
    if (ret < 0) {
        return ret;
    }

    return usbwatchdog_command(wdt_dev, 0x00, 0xffa74501);
}

static int usbwatchdog_restart(struct watchdog_device *wdt_dev,
                               unsigned long action, void *data)
{
    dev_dbg(wdt_dev->parent, "USB watchdog restart");
    return usbwatchdog_command(wdt_dev, 0x05, 0xa3ac3d49);
}


static const struct watchdog_ops usbwatchdog_ops = {
        .owner          = THIS_MODULE,
        .start          = usbwatchdog_start,
        .stop           = usbwatchdog_stop,
        .ping           = usbwatchdog_ping,
        .set_timeout    = usbwatchdog_set_timeout,
        .get_timeleft	= usbwatchdog_get_timeleft,
        .restart        = usbwatchdog_restart,
};

static const struct watchdog_info usbwatchdog_info = {
        .identity       = "usbwatchdog",
        .options        = WDIOF_SETTIMEOUT |
                          WDIOF_KEEPALIVEPING |
                          WDIOF_MAGICCLOSE,
};

static int usbwatchdog_probe(struct usb_interface *interface,
                             const struct usb_device_id *id)
{
    struct usb_device *udev = interface_to_usbdev(interface);
    struct usbwatchdog *dev;
    struct watchdog_device *wdt_dev;
    int ret;

    dev_dbg(&interface->dev, "USB watchdog probe %s-%s", udev->product, udev->serial);

    dev = devm_kzalloc(&interface->dev, sizeof(*dev), GFP_KERNEL);
    if (!dev)
        return -ENOMEM;

    mutex_init(&dev->mutex);

    wdt_dev = &dev->wdt_dev;
    wdt_dev->parent = &udev->dev;
    wdt_dev->info = &usbwatchdog_info;
    wdt_dev->ops = &usbwatchdog_ops;
    wdt_dev->min_timeout = 1;
    wdt_dev->max_timeout = 0xFFFFFFFF / (1000 * 1000) - 1;
    wdt_dev->timeout = wdt_dev->max_timeout;
    watchdog_set_drvdata(wdt_dev, dev);
    watchdog_stop_on_reboot(wdt_dev);
    watchdog_set_restart_priority(wdt_dev, 0);
    watchdog_init_timeout(wdt_dev, timeout, &interface->dev);
    if (wdt_dev->timeout < wdt_dev->min_timeout || wdt_dev->timeout > wdt_dev->max_timeout) {
        wdt_dev->timeout = wdt_dev->max_timeout;
    }

    dev->interface = usb_get_intf(interface);
    dev->udev = udev;
    dev->present = 1;

    usb_set_intfdata(interface, dev);

    ret = devm_watchdog_register_device(&interface->dev, &dev->wdt_dev);
    if (ret) {
        dev->present = 0;
        return ret;
    }

    dev_info(&interface->dev, "USB watchdog initialized (timeout=%d sec)\n", wdt_dev->timeout);
    return 0;
}

static void usbwatchdog_disconnect(struct usb_interface *interface)
{
    struct usbwatchdog *dev = usb_get_intfdata(interface);

    mutex_lock(&dev->mutex);
    dev->present = 0;
    mutex_unlock(&dev->mutex);
}


static const struct usb_device_id usbwatchdog_table[] = {
        { USB_DEVICE(0x2e8a, 0xa43f) },
        { },
};
MODULE_DEVICE_TABLE(usb, usbwatchdog_table);

static struct usb_driver usbwatchdog_driver = {
        .name = "usbwatchdog",
        .probe = usbwatchdog_probe,
        .disconnect = usbwatchdog_disconnect,
        .id_table = usbwatchdog_table,
};

module_usb_driver(usbwatchdog_driver);

module_param(timeout, uint, 0);
MODULE_PARM_DESC(timeout, "Watchdog heartbeat in seconds");


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Derek Hageman <hageman@inthat.cloud>");
MODULE_DESCRIPTION("Simple RP2040 USB watchdog driver");
