#!/bin/bash

check() {
  return 0
}

depends() {
  return 0
}

installkernel() {
  instmods tpm_crb tpm_tis
}

install() {
  _arch=${DRACUT_ARCH:-$(uname -m)}
  inst_libdir_file \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-esys.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-fapi.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-mu.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-rc.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-sys.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-tcti-cmd.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-tcti-device.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-tcti-mssim.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-tcti-swtpm.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libtss2-tctildr.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libcryptsetup.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"/cryptsetup/libcryptsetup-token-systemd-tpm2.so" \
      {"tls/$_arch/",tls/,"$_arch/",}"libcurl.so.*" \
      {"tls/$_arch/",tls/,"$_arch/",}"libjson-c.so.*"

  # Workaround for a dracut bug a line with "force" to include all subsequent lines (#807), fixed in dracut 51
  sed -r -i -n -e 's/^[^[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+([^[:space:]]+,)?force(,[^[:space:]]+)?/\0/p' "${initdir}/etc/crypttab"
}