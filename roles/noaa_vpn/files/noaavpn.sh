#!/bin/sh

if [ "$1" == "mount" ]; then
  mkdir -p "/run/user/{{ user_info.uid }}/noaa" "/run/user/{{ user_info.uid }}/aer"
  sshfs -o idmap=user,reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 aeroproc.cmdl.noaa.gov:/home/aer/hageman "/run/user/{{ user_info.uid }}/noaa"
  sshfs -o idmap=user,reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 aeroproc.cmdl.noaa.gov:/nfs/aer "/run/user/{{ user_info.uid }}/aer"
elif [ "$1" == "unmount" ] || [ "$1" == "umount" ]; then
  umount "/run/user/{{ user_info.uid }}/noaa"
  umount "/run/user/{{ user_info.uid }}/aer"
elif [ "$1" == "windows" ]; then
  if [ -n "${WAYLAND_DISPLAY}" ]; then
    exec wlfreerdp /size:1920x1080 -sec-nla /cert-ignore +clipboard /smartcard-logon /smartcard: /v:monsoon.cmdl.noaa.gov
  else
    exec xfreerdp  /size:1920x1080 -sec-nla /cert-ignore +clipboard /smartcard-logon /smartcard: /v:monsoon.cmdl.noaa.gov
  fi
else
  exec sudo /usr/local/bin/connect_noaavpn
fi
