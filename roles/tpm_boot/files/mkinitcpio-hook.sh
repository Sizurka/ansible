#!/bin/bash

build() {
  add_module tpm_crb
  add_module tpm_tis

  add_file /etc/udev/rules.d/75-tpm-systemd.rules

  for lib in /usr/lib/libtss2-tcti-device.so* /usr/lib/libtss2-esys.so* /usr/lib/libtss2-mu.so*; do
    add_binary "$lib"
  done
}