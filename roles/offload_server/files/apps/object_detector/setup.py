#!/usr/bin/python3

import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'requirements.txt')) as requirements_txt:
    REQUIRES = requirements_txt.read().splitlines()

setup(
    name='object_detector',
    version='1.0.0',
    python_requires='>=3.6,<4.0',
    install_requires=REQUIRES,
    entry_points={'console_scripts': ['object-detector-server = object_detector.__main__:main']},
    packages=find_packages(),
)
