#!/usr/bin/python3

import requests

result = requests.post('{{ network.wireguard.api }}', data={
    'username': """{{ network.wireguard.user | trim }}""",
    'password': """{{ network.wireguard.password | trim }}""",
    'pubkey': """{{ lookup('file', playbook_dir + '/files/wireguard/' + inventory_hostname + '.public') }}""",
})

if result.status_code != 200:
    raise RuntimeError
existing = result.json()

if existing["status"] != "success":
    raise RuntimeError


wgconfig = f"""[Interface]
PrivateKey = {{ lookup('file', playbook_dir + '/files/wireguard/' + inventory_hostname + '.private') }}
Address = {existing["data"]["Address"]}
DNS = {existing["data"]["DNS"]}

[Peer]
PublicKey = {existing["data"]["PublicKey"]}
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = {existing["data"]["Endpoint"]}
"""

with open('/etc/wireguard/wg-provider.conf', 'w') as f:
    f.write(wgconfig)
