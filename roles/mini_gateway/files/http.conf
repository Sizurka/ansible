map $http_upgrade $connection_upgrade {
	default	upgrade;
	''	close;
}


server {
	server_name {{ domain_name }} www.{{ domain_name }};

	listen 80 default_server;
    listen [::]:80 default_server;
    listen 443 default_server ssl http2 default_server;
    listen [::]:443 default_server ssl http2 default_server;

    ssl_protocols TLSv1.3 TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_certificate /etc/nginx/server.crt;
    ssl_certificate_key /etc/nginx/server.key;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

	if ($host = "ha") {
		return 301 $scheme://ha.{{ domain_name }}$request_uri;
	}
	if ($host = "autoproxy") {
		return 301 $scheme://autoproxy.{{ domain_name }}$request_uri;
	}

	root /srv/www;
	index index.html;

	allow {{ network.internal.ipv4 }};
	{% if network.internal.ipv6 is defined %}allow {{ network.internal.ipv6 }};{% endif %}
	deny all;
}

server {
	server_name ha.{{ domain_name }};

	listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_protocols TLSv1.3 TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_certificate /etc/nginx/server.crt;
    ssl_certificate_key /etc/nginx/server.key;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

	proxy_buffering off;

	location / {
		proxy_pass http://localhost:8123;

		proxy_set_header Host $host;
		proxy_redirect http:// https://;
		proxy_http_version 1.1;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection $connection_upgrade;
	}

	allow {{ network.internal.ipv4 }};
	{% if network.internal.ipv6 is defined %}allow {{ network.internal.ipv6 }};{% endif %}
	deny all;
}

server {
	server_name autoproxy.{{ domain_name }};

	listen 80;
    listen [::]:80;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_protocols TLSv1.3 TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_certificate /etc/nginx/server.crt;
    ssl_certificate_key /etc/nginx/server.key;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

	root /srv/autoproxy;
	location = /proxy.js {
		 types { } default_type "application/x-ns-proxy-autoconfig";
	}

	allow {{ network.internal.ipv4 }};
	{% if network.internal.ipv6 is defined %}allow {{ network.internal.ipv6 }};{% endif %}
	deny all;
}
