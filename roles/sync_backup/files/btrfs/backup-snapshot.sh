#!/bin/sh

if [ "$1" = "acquire" ]; then
  btrfs subvolume delete "/.snapshots/backup" >/dev/null 2>&1
  exec btrfs subvolume snapshot -r / "/.snapshots/backup" >/dev/null
else
  exec btrfs subvolume delete "/.snapshots/backup" >/dev/null
fi
