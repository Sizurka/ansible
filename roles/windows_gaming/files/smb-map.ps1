while ($True) {
    Start-Sleep -Seconds 5
    try {
        New-PSDrive -Name Z -Root '\\{{ network.secure.ipv4.address | ansible.utils.ipmath(network.secure.static.kitsune) }}\Export' -Persist -PSProvider FileSystem
        break
    } catch {
    }
}