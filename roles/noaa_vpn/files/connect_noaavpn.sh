#!/bin/sh

ROUTES="
  140.172.144.0/24
  140.172.192.0/24
  140.172.192.3/24
  140.172.192.6/24
  140.172.196.0/24
  140.172.200.0/24
  140.172.202.0/24
"

openconnect gate.esrl.noaa.gov --protocol=gp --no-dtls --base-mtu=1500 \
  -c 'pkcs11:token=HAGEMAN.DEREK.CARL.1386905122;object=Certificate%20for%20PIV%20Authentication' \
  '--user=Derek.Hageman' \
  -s "vpn-slice --no-host-names --no-ns-hosts $(echo ${ROUTES} | tr -d '\n')"
