#!/usr/bin/python3
import os
import socket
import time
import subprocess
import argparse
import signal
import threading
import sys
from paramiko.client import SSHClient


parser = argparse.ArgumentParser(description="Attach USB-IP devices.")
parser.add_argument('host',
                    help="the host to attach to")
parser.add_argument('--mac',
                    dest='mac',
                    help="host mac address")
parser.add_argument('--ssh',
                    dest='ssh_forward', action='store_true',
                    help="forward over SSH instead of direct connection")
parser.add_argument('--device',
                    dest='device',
                    help="device (match=value,...) to attach",
                    nargs='*', action='extend')
parser.add_argument('--device-list',
                    dest='device_list',
                    help="file containing devices (match=value,...) to attach")
args = parser.parse_args()


attach_devices = set()
forward_socket = None
os.environ["HOME"] = '/home/{{ login.user }}'


def convert_device_match(raw):
    matcher = dict()
    for pair in raw.split(','):
        try:
            key, value = pair.split('=', 1)
        except ValueError:
            continue
        matcher[key] = value
    if matcher:
        keys = list(matcher.keys())
        keys.sort()
        attach_devices.add(",".join([f"{key}={matcher[key]}" for key in keys]))


if args.device_list:
    with open(args.device_list, "rt") as f:
        for line in f:
            line = line.strip()
            if line.startswith('#'):
                continue
            if not line:
                continue
            convert_device_match(line)
if args.device:
    for dev in args.device:
        convert_device_match(dev)


def start_connection():
    if args.mac:
        try:
            client = SSHClient()
            client.load_system_host_keys()
            client.connect(args.host, username='{{ login.user }}', timeout=3)
            return client
        except:
            pass

        print(f"Waking up {args.host}", flush=True)
        for t in range(5):
            subprocess.run(['wol', args.mac], stdout=subprocess.DEVNULL)
            time.sleep(5)

            try:
                client = SSHClient()
                client.load_system_host_keys()
                client.connect(args.host, username='{{ login.user }}', timeout=3)
                return client
            except:
                pass

    client = SSHClient()
    client.load_system_host_keys()
    client.connect(args.host, username='{{ login.user }}', timeout=5)
    return client


def forward_stdio(source, reader):
    while not source.channel.exit_status_ready():
        data = reader()
        if not data:
            return
        try:
            sys.stdout.buffer.write(data)
            sys.stdout.buffer.flush()
        except OSError:
            return


def handle_forwarded_connection(connection, source_port):
    channel = client.get_transport().open_channel(
        "direct-tcpip",
        ("localhost", 3240),
        (socket.gethostname(), source_port),
    )

    def shutdown():
        try:
            connection.close()
        except:
            pass
        try:
            channel.close()
        except:
            pass

    def send_to_remote():
        while True:
            data = connection.recv(4096)
            if not data:
                shutdown()
                return
            try:
                channel.send(data)
            except OSError:
                shutdown()
                return

    def send_to_local():
        while not channel.exit_status_ready():
            try:
                data = channel.recv(4096)
            except socket.timeout:
                continue
            if not data:
                shutdown()
                return
            try:
                connection.send(data)
            except OSError:
                shutdown()
                return
        shutdown()

    sr = threading.Thread(target=send_to_remote, daemon=True)
    sl = threading.Thread(target=send_to_local, daemon=True)

    sr.start()
    sl.start()
    sr.join()
    sl.join()
    shutdown()


def listen_forwarded_connection(server_socket):
    while True:
        connection, _ = server_socket.accept()
        threading.Thread(
            target=handle_forwarded_connection,
            args=(connection, server_socket.getsockname()[1]),
            daemon=True
        ).start()


def attach_device(remote_busid):
    print(f"Attaching device {remote_busid}", flush=True)
    if not forward_socket:
        subprocess.run(['usbip', 'attach', '-r', args.host, '-b', remote_busid],
                       stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    else:
        port = forward_socket.getsockname()[1]
        subprocess.run(['usbip', '--tcp-port', str(port), 'attach', '-r', 'localhost', '-b', remote_busid],
                       stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def read_attached_devices(input_stream):
    class LineIter:
        def __init__(self, source):
            self.source = source
            self.buffer = bytearray()
            self.got_eof = False

        def __iter__(self):
            return self

        def __next__(self) -> str:
            while True:
                try:
                    idx = self.buffer.index(b'\n')
                    chunk = self.buffer[:idx].decode('utf-8', 'ignore')
                    del self.buffer[:idx+1]
                    if chunk:
                        return chunk
                    continue
                except ValueError:
                    if self.got_eof:
                        if not self.buffer:
                            raise StopIteration
                        chunk = self.buffer.decode('utf-8', 'ignore')
                        self.buffer.clear()
                        return chunk
                    pass
                data = self.source.recv(128)
                if not data:
                    self.got_eof = True
                else:
                    self.buffer += data

    for line in LineIter(input_stream.channel):
        line = line.strip()
        try:
            key, value = line.split('=', 1)
        except ValueError:
            continue
        if key.lower() == 'busid':
            attach_device(value)


client = start_connection()

if args.ssh_forward:
    forward_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    forward_socket.bind(('localhost', 0))
    forward_socket.listen(8)
    threading.Thread(target=listen_forwarded_connection, args=(forward_socket,), daemon=True).start()

control_stdin, control_stdout, control_stderr = client.exec_command('sudo /usr/local/bin/run-usb-export')

threading.Thread(target=read_attached_devices, args=(control_stdout,), daemon=True).start()
threading.Thread(target=forward_stdio, args=(control_stderr, lambda: control_stderr.channel.recv_stderr(4096)), daemon=True).start()


for dev in attach_devices:
    control_stdin.write(f"{dev}\n")


signal.sigwait({signal.SIGINT, signal.SIGTERM, signal.SIGHUP})

client.close()
