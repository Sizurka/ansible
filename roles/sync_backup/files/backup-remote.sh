#!/bin/sh

server_control() {
  ssh \
    -o "IdentityFile /etc/backup/remote_id_ed25519" \
    -o "StrictHostKeyChecking no" \
    -o "UpdateHostKeys no" \
    -o "UserKnownHostsFile /dev/null" \
    "root@{{ network.secure.ipv4.address | ansible.utils.ipmath(network.secure.static.chimera) }}" \
    "$1"
}

server_control "acquire" || exit 75

if ! /usr/local/bin/backup-snapshot acquire; then
  server_control "release"
  exit 2
fi

nice -n 5 \
  rsync --recursive --links --perms --owner --group --acls --xattrs --specials --times \
   --delete --delete-during --delete-excluded --one-file-system \
  --exclude-from /etc/backup/archive_exclude \
  --password-file /etc/backup/rsync_password \
  "/.snapshots/backup/" "rsync://{{ inventory_hostname }}@{{ network.secure.ipv4.address | ansible.utils.ipmath(network.secure.static.chimera) }}/{{ inventory_hostname }}/"
RSYNC_STATUS="$?"

/usr/local/bin/backup-snapshot release

server_control "release" || exit 1

[ "${RSYNC_STATUS}" == "24" ] && RSYNC_STATUS=0
exit "${RSYNC_STATUS}"