#!/usr/bin/python3

import sys
import asyncio
import aiohttp
import systemd.daemon
import signal
import json
import traceback
import evdev
import typing
from enum import Enum


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


class Presence(Enum):
    AWAY = 1
    ACTIVE = 2
    SLEEP = 3
presence_state: typing.Optional[Presence] = None
presence_changed = asyncio.Event()


async def homeassistant_connection():
    def unpack_message(msg) -> typing.Dict[str, typing.Any]:
        if msg.type == aiohttp.WSMsgType.TEXT:
            return json.loads(msg.data)
        elif msg.type == aiohttp.WSMsgType.BINARY:
            return json.loads(msg.data.decode('utf-8'))
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise RuntimeError

    api_url = "https://ha.{{ domain_name }}/api"

    while True:
        try:
            timeout = aiohttp.ClientTimeout(connect=30, sock_read=60)
            async with aiohttp.ClientSession(timeout=timeout) as session:
                async with session.ws_connect(api_url + "/websocket", heartbeat=10.0) as websocket:
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_required':
                        raise RuntimeError(str(data))
                    await websocket.send_json({
                        'type': 'auth',
                        'access_token': api_key,
                    })
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_ok':
                        raise RuntimeError(str(data))

                    command_id: int = 0
                    async def subscribe_trigger(trigger: typing.Dict[str,typing.Any]) -> None:
                        nonlocal command_id
                        command_id += 1
                        await websocket.send_json({
                            'id': command_id,
                            'type': 'subscribe_trigger',
                            'trigger': trigger,
                        })
                        while True:
                            data = unpack_message(await websocket.receive())
                            if data['type'] != "result":
                                continue
                            if data['id'] != command_id:
                                continue
                            if data['success'] != True:
                                raise RuntimeError(data)
                            return

                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_select.presence',
                        'from': None,
                    })

                    def dispatch_state(entity_id: str, state: str, attributes: typing.Dict[str, typing.Any]) -> None:
                        if entity_id == 'input_select.presence':
                            global presence_state
                            if state == 'Home':
                                presence_state = Presence.ACTIVE
                            elif state == 'Sleep':
                                presence_state = Presence.SLEEP
                            else:
                                presence_state = Presence.AWAY
                            presence_changed.set()

                    command_id += 1
                    await websocket.send_json({
                        'id': command_id,
                        'type': 'get_states',
                    })
                    read_states_id = command_id
                    async for msg in websocket:
                        data = unpack_message(msg)
                        if data['type'] == 'event':
                            to_state = data['event']['variables']['trigger']['to_state']
                            dispatch_state(to_state['entity_id'], to_state['state'], to_state['attributes'])
                        elif data['type'] == 'result' and data['id'] == read_states_id:
                            for entity in data['result']:
                                dispatch_state(entity['entity_id'], entity['state'], entity['attributes'])
        except:
            traceback.print_exc()
        await asyncio.sleep(60.0)

homeassistant_connection_t = loop.create_task(homeassistant_connection())


async def set_dconf(key, value):
    proc = await asyncio.create_subprocess_exec("dconf", "write", key, value,
                                                stdin=asyncio.subprocess.DEVNULL)
    await proc.communicate()


async def update_screen_control():
    current_state = None
    while True:
        await presence_changed.wait()
        presence_changed.clear()

        if presence_state == current_state:
            continue
        current_state = presence_state

        lock_delay = 1200
        if current_state == Presence.ACTIVE:
            lock_delay = 10800
        elif current_state == Presence.SLEEP:
            lock_delay = 900

        await asyncio.gather(
            set_dconf("/org/gnome/desktop/session/idle-delay",
                      "uint32 {}".format(lock_delay)),
            set_dconf("/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout",
                      "{}".format(lock_delay + 300))
        )

update_screen_control_t = loop.create_task(update_screen_control())


def exit_handler(sig, frame):
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


main_t = loop.create_task(main())
loop.run_forever()
