import asyncio
import cv2
import numpy as np
import starlette.status
import object_detector.doods
from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.routing import Route
from starlette.exceptions import HTTPException
from .yolo import async_detect_objects as yolo_detect_objects
from .peoplenet import async_detect_objects as peoplenet_detect_objects


async def _detect(request):
    raw = np.frombuffer(await request.body(), dtype=np.uint8)
    img = cv2.imdecode(raw, cv2.IMREAD_COLOR)
    if img is None:
        raise HTTPException(starlette.status.HTTP_400_BAD_REQUEST, detail="Invalid image")

    detector = request.query_params.get('detector')
    if not detector or detector == 'default' or detector == 'yolo':
        size_limit = int(request.query_params.get('size', 512))
        detections = await yolo_detect_objects(img, size_limit)
    elif detector == 'peoplenet':
        detections = await peoplenet_detect_objects(img)
    else:
        raise HTTPException(starlette.status.HTTP_400_BAD_REQUEST, detail="Invalid detector")

    return JSONResponse(detections)


app = Starlette(routes=[
    Route('/detect', endpoint=_detect, methods=['POST']),
    Route('/doods/detect', endpoint=object_detector.doods.detect, methods=['POST']),
    Route('/doods/detectors', endpoint=object_detector.doods.detectors),
])
