#!/bin/bash
umask 0077

# http://0pointer.net/blog/unlocking-luks2-volumes-with-tpm2-fido2-pkcs11-security-hardware-on-systemd-248.html

{% if tpm_security == "secureboot" or tpm_security == "customshim" %}
SB_ENABLED=$(cat /sys/firmware/efi/efivars/SecureBoot-* 2>/dev/null | dd bs=1 skip=4 status=none | hexdump -n 2 -e '"%d"')
[ "${SB_ENABLED}" = "1" ] || exit 0
{% endif %}

set -e

export PASSWORD=$(< /root/keys/boot_crypt.bin)
{% if tpm_security == "secureboot" or tpm_security == "customshim"  %}
{% for device in root_devices %}
systemd-cryptenroll  --wipe-slot=tpm2 --tpm2-device=auto --tpm2-pcrs='{{ tpm_seal_pcrs }}' '{{ device }}-part2'
{% endfor %}
{% else %}
ALL_LOADED=1
{% for device in root_devices %}
RESULT=$(systemd-cryptenroll  --wipe-slot=tpm2 --tpm2-device=auto --tpm2-pcrs='{{ tpm_seal_pcrs }}' '{{ device }}-part2' 2>&1)
if [ "${RESULT}" != "This PCR set is already enrolled, executing no operation." ]; then
  ALL_LOADED=0
  echo "${RESULT}"
fi
{% endfor %}
{% endif %}
unset PASSWORD

{% if not (tpm_security == "secureboot" or tpm_security == "customshim") %}
[ "${ALL_LOADED}" = "1" ] || exit 0
{% endif %}
systemctl disable update-tpm.service || true
