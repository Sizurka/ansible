#!/usr/bin/python3

import argparse
import os

parser = argparse.ArgumentParser(description="WebCam control shortcut.")
subparsers = parser.add_subparsers(dest='command')

subparsers.add_parser('default', help="reset to defaults", aliases=['reset'])
subparsers.add_parser('plain', help="disable background masking", aliases=['nomask'])
subparsers.add_parser('starwars', help="StarWars mode")

args = parser.parse_args()


def write_command(command: str):
    with open(os.environ['XDG_RUNTIME_DIR'] + "/aiwebcam/control", "w") as f:
        f.write(command)


if args.command == 'default' or args.command == 'reset':
    write_command("reset\n")
elif args.command == 'plan' or args.command == 'nomask':
    write_command("background off\n")
elif args.command == 'starwars':
    write_command("background /home/{{ login.user }}/ansible/files/images/millennium_falcon.jpg\nhologram on\n")
else:
    parser.error("No command specified")
