import cv2
import numpy as np
from math import ceil


def detect_motion(before, after, mask, change_threshold: float = 0.1, size_limit: int = 512) -> float:
    if size_limit > 32:
        if after.shape[0] > size_limit and after.shape[0] >= after.shape[1]:
            scale = size_limit / after.shape[0]
            after = cv2.resize(after, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_AREA)
        elif after.shape[1] > size_limit:
            scale = size_limit / after.shape[1]
            after = cv2.resize(after, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_AREA)

    if before.shape[:2] != after.shape[:2]:
        if max(before.shape[:2]) > max(after.shape[:2]):
            before = cv2.resize(before, (after.shape[1], after.shape[0]), interpolation=cv2.INTER_AREA)
        else:
            before = cv2.resize(before, (after.shape[1], after.shape[0]))
    before = cv2.fastNlMeansDenoisingColored(before,None,10,10,7,21)

    after = cv2.fastNlMeansDenoisingColored(after,None,10,10,7,21)
    if mask is not None:
        if mask.shape[:2] != after.shape[:2]:
            if max(mask.shape[:2]) > max(after.shape[:2]):
                mask = cv2.resize(mask, (after.shape[1], after.shape[0]), interpolation=cv2.INTER_AREA)
            else:
                mask = cv2.resize(mask, (after.shape[1], after.shape[0]))
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
        mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)[1]

    before = cv2.cvtColor(before, cv2.COLOR_BGR2GRAY)
    after = cv2.cvtColor(after, cv2.COLOR_BGR2GRAY)

    blur_size = int(ceil(max(after.shape[0], after.shape[1]) * 0.01))
    blur_size = max(blur_size, 5)
    if blur_size % 2 == 0:
        blur_size += 1

    before = cv2.GaussianBlur(before, (blur_size, blur_size), 0)
    after = cv2.GaussianBlur(after, (blur_size, blur_size), 0)

    frame_delta = cv2.absdiff(before, after)
    thresh = cv2.threshold(frame_delta, int(round(change_threshold * 255)), 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)

    if mask is not None:
        thresh = cv2.bitwise_and(thresh, thresh, mask=mask)
        total_pixels = np.sum(mask)
    else:
        total_pixels = thresh.shape[0] * thresh.shape[1] * 255

    pixel_change = float(np.sum(thresh)) / float(total_pixels)

    return pixel_change
