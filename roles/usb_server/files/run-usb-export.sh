#!/bin/bash

exec systemd-inhibit --what=sleep --who="USB-IP export" --why="USB devices under a remote host control" /usr/local/bin/export-usb