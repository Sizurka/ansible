#!/bin/bash

virsh start hydra

for ((i=0; $i < 30; i=$i+1)); do
  if [ -e "/dev/shm/looking-glass "]; then
    break
  end
  if [ $i == 29 ]; then
    echo "No SHM detected"
    exit 1
  end
done

exec looking-glass-client app:shmFile=/dev/shm/looking-glass \
  win:size=3840x2160 \
  win:borderless \
  spice:host=/run/hydra-spice.sock spice:port=0 \
  egl:vsync \
  opengl:vsync \
  spice:captureOnStart
