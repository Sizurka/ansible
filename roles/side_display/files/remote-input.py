#!/usr/bin/python3

import evdev
import signal
import asyncio
import struct
import sys


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


async def stream_events():
    reader = asyncio.StreamReader()
    await loop.connect_read_pipe(lambda:  asyncio.StreamReaderProtocol(reader), sys.stdin)

    events = {
        # Unfortunately, using evdev.ecodes.keys.keys() breaks GNOME/logind, so we have to enumerate the real ones
        # evdev.ecodes.EV_KEY: list(evdev.ecodes.keys.keys()),
        evdev.ecodes.EV_KEY: list(range(1, 255)) + list(range(0x100, 0x118)),
        evdev.ecodes.EV_REL: [evdev.ecodes.REL_X, evdev.ecodes.REL_Y,
                              evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL],
    }
    output = evdev.UInput(events, name="remote-input")

    try:
        while True:
            data = await reader.readexactly(8)
            if not data:
                break

            event_type, event_code, event_value = struct.unpack("<HHi", data)
            valid_codes = events.get(event_type)
            if not valid_codes:
                continue
            if event_code not in valid_codes:
                continue

            if event_type == evdev.ecodes.EV_KEY:
                if event_value != 0 and event_value != 1:
                    continue

            output.write(event_type, event_code, event_value)
            output.syn()
    finally:
        output.close()


main_task = loop.create_task(stream_events())
loop.add_signal_handler(signal.SIGINT, main_task.cancel)
loop.add_signal_handler(signal.SIGTERM, main_task.cancel)
loop.run_until_complete(main_task)

exit(0)

