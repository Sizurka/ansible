# Copyright (c) 2020 Derek Hageman <hageman@inthat.cloud>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = """
    author: Derek Hageman <hageman@inthat.cloud>
    name: vm_guest_agent
    short_description: Interact with a local VM via a guest agent socket
    description:
        - Run commands or put/fetch files to a VM with a special guest agent socket on the Ansible controller.
    options:
      remote_addr:
        description:
            - The path of the local socket the agent is connected to.
        default: inventory_hostname
        vars:
            - name: ansible_host
"""

import os
import os.path
import socket
import struct
import shlex

from ansible.errors import AnsibleError, AnsibleConnectionFailure
from ansible.plugins.connection import ConnectionBase, BUFSIZE
from ansible.utils.display import Display
from ansible.module_utils._text import to_native, to_bytes
from ansible.module_utils.six import binary_type

display = Display()


STREAM_ID_CONTROL = 0xFFFF
COMMAND_END_STREAM = 0
COMMAND_START_PUT_FILE = 1
COMMAND_START_FETCH_FILE = 2
COMMAND_START_PROCESS = 3


class Connection(ConnectionBase):
    ''' Local VM agent based connections '''

    transport = 'vm_guest_agent'
    has_pipelining = True
    has_tty = False

    default_user = 'root'

    def __init__(self, play_context, new_stdin, *args, **kwargs):
        super(Connection, self).__init__(play_context, new_stdin, *args, **kwargs)

        self.guest_socket_path = self._play_context.remote_addr
        self._conn = None

        if not os.path.exists(self.guest_socket_path):
            raise AnsibleError("%s doest not exist" % self.guest_socket_path)

    def _write_exactly(self, data):
        offset = 0
        while len(data) - offset > 0:
            nwr = self._conn.send(data[offset:])
            offset += nwr

    def _read_exactly(self, total):
        result = binary_type()
        while total > 0:
            data = self._conn.recv(total)
            result += data
            total -= len(data)
        return result

    def _communicate(self, streams):
        while len(streams) > 0:
            stream_id, length = struct.unpack("<HH", self._read_exactly(4))
            if stream_id == STREAM_ID_CONTROL:
                command = length
                if command == COMMAND_END_STREAM:
                    stream_id, = struct.unpack("<H", self._read_exactly(2))
                    streams.pop(stream_id)
                else:
                    raise AnsibleError("invalid packet received from agent")
                continue

            data = self._read_exactly(length)
            target = streams.get(stream_id)
            if target is not None:
                target(data)

    def _connect(self):
        if self._conn is not None:
            return

        try:
            self._conn = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self._conn.connect(self.guest_socket_path)
        except (OSError, IOError) as e:
            if self._conn is not None:
                self._conn.close()
                self._conn = None
            raise AnsibleConnectionFailure("Error connecting to agent socket '%s': %s" %
                                           (self.guest_socket_path, to_native(e)))
        try:
            self._conn.settimeout(30.0)
            self._write_exactly(struct.pack("<BBB", 0x00, 0xAA, 0xFF))
            buffer = self._read_exactly(3)
            self._conn.settimeout(None)
        except (OSError, IOError) as e:
            raise AnsibleConnectionFailure("Error in agent socket '%s' handshake: %s" %
                                           (self.guest_socket_path, to_native(e)))

        if buffer[0] != 0x00 or buffer[1] != 0xFF or buffer[2] != 0xAA:
            self._conn.close()
            self._conn = None
            raise AnsibleConnectionFailure("Invalid agent handshake on '%s'" % (self.guest_socket_path))

        display.vvv("HANDSHAKE OK", host=self.guest_socket_path)

        self._connected = True
        super(Connection, self)._connect()

    def exec_command(self, cmd, in_data=None, sudoable=False):
        super(Connection, self).exec_command(cmd, in_data=in_data, sudoable=sudoable)

        display.vvv("EXEC %s" % (cmd), host=self.guest_socket_path)

        args = shlex.split(to_native(cmd, errors='surrogate_or_strict'))

        stream_id_stdin = STREAM_ID_CONTROL
        if in_data is not None:
            in_data = to_bytes(in_data, errors='surrogate_or_strict')
        if in_data is not None and len(in_data) > 0:
            stream_id_stdin = 1

        try:
            self._write_exactly(struct.pack("<HHHH", STREAM_ID_CONTROL, COMMAND_START_PROCESS,
                                            stream_id_stdin, len(args)))
            for arg in args:
                arg_data = to_bytes(arg, errors='surrogate_or_strict')
                self._write_exactly(struct.pack("<L", len(arg_data)))
                self._write_exactly(arg_data)
            buffer = self._read_exactly(12)
        except (OSError, IOError):
            raise AnsibleError("error communicating with agent")

        response_control, response_command, response_stream_id_stdin, \
            stream_id_stdout, stream_id_stderr, stream_id_monitor = struct.unpack("<HHHHHH", buffer)
        if response_control != STREAM_ID_CONTROL or response_command != COMMAND_START_PROCESS or \
                response_stream_id_stdin != stream_id_stdin or \
                stream_id_stdout == STREAM_ID_CONTROL or \
                stream_id_stderr == STREAM_ID_CONTROL or \
                stream_id_monitor == STREAM_ID_CONTROL:
            raise AnsibleError("failed to start command")

        if stream_id_stdin != STREAM_ID_CONTROL:
            try:
                offset = 0
                while True:
                    remaining = len(in_data) - offset
                    if remaining <= 0:
                        break
                    count = min(remaining, 0xFFFF)
                    self._write_exactly(struct.pack("<HH", stream_id_stdin, count))
                    self._write_exactly(in_data[offset:offset+count])
                    offset += count
                self._write_exactly(struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_END_STREAM, stream_id_stdin))
            except (OSError, IOError):
                raise AnsibleError("error sending stdin to agent")

        stdout = binary_type()
        stderr = binary_type()
        monitor = binary_type()

        def stdout_data(data):
            nonlocal stdout
            stdout += data

        def stderr_data(data):
            nonlocal stderr
            stderr += data

        def monitor_data(data):
            nonlocal monitor
            monitor += data

        try:
            self._communicate({
                stream_id_stdout: stdout_data,
                stream_id_stderr: stderr_data,
                stream_id_monitor: monitor_data,
            })
        except (OSError, IOError):
            raise AnsibleError("error communicating with agent")

        returncode = -1
        if len(monitor) >= 2:
            returncode, = struct.unpack_from("<h", monitor)
        return (returncode, to_native(stdout), to_native(stderr))

    def _prefix_login_path(self, remote_path):
        ''' Make sure that we put files into a standard path

            If a path is relative, then we need to choose where to put it.
            ssh chooses $HOME but we aren't guaranteed that a home dir will
            exist in any given chroot.  So for now we're choosing "/" instead.
            This also happens to be the former default.

            Can revisit using $HOME instead if it's a problem
        '''
        if not remote_path.startswith(os.path.sep):
            remote_path = os.path.join(os.path.sep, remote_path)
        return os.path.normpath(remote_path)

    def put_file(self, in_path, out_path):
        super(Connection, self).put_file(in_path, out_path)
        display.vvv("PUT %s TO %s" % (in_path, out_path), host=self.guest_socket_path)

        try:
            with open(to_bytes(in_path, errors='surrogate_or_strict'), 'rb') as in_file:
                stream_id = 1
                out_path = to_bytes(self._prefix_login_path(out_path), errors='surrogate_or_strict')
                try:
                    self._write_exactly(struct.pack("<HHHH", STREAM_ID_CONTROL, COMMAND_START_PUT_FILE,
                                                    stream_id, len(out_path)))
                    self._write_exactly(out_path)
                    buffer = self._read_exactly(6)
                except (OSError, IOError):
                    raise AnsibleError("error communicating with agent")

                response_control, response_command, response_stream_id = struct.unpack("<HHH", buffer)
                if response_control != STREAM_ID_CONTROL or response_command != COMMAND_START_PUT_FILE \
                        or response_stream_id != stream_id:
                    raise AnsibleError("failed to start file transfer from %s to %s" % (in_path, out_path))

                while True:
                    data = in_file.read(0xFFFF)
                    if not data:
                        break
                    try:
                        self._write_exactly(struct.pack("<HH", stream_id, len(data)))
                        self._write_exactly(data)
                    except (OSError, IOError):
                        raise AnsibleError("failed to transfer file from %s to %s" % (in_path, out_path))
                try:
                    self._write_exactly(struct.pack("<LHL", STREAM_ID_CONTROL, COMMAND_END_STREAM, stream_id))
                except (OSError, IOError):
                    raise AnsibleError("error communicating with agent")
        except IOError:
            raise AnsibleError("file or module does not exist at: %s" % in_path)

    def fetch_file(self, in_path, out_path):
        super(Connection, self).fetch_file(in_path, out_path)
        display.vvv("FETCH %s TO %s" % (in_path, out_path), host=self.guest_socket_path)

        in_path = to_bytes(self._prefix_login_path(in_path), errors='surrogate_or_strict')
        try:
            self._write_exactly(struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_START_FETCH_FILE, len(in_path)))
            self._write_exactly(in_path)
            buffer = self._read_exactly(6)
        except (OSError, IOError):
            raise AnsibleError("error communicating with agent")

        response_control, response_command, stream_id = struct.unpack("<HHH", buffer)
        if response_control != STREAM_ID_CONTROL or response_command != COMMAND_START_FETCH_FILE \
                or stream_id == STREAM_ID_CONTROL:
            raise AnsibleError("failed to start file transfer from %s to %s" % (in_path, out_path))

        try:
            with open(to_bytes(out_path, errors='surrogate_or_strict'), 'wb') as out_file:
                try:
                    self._communicate({
                        stream_id: out_file.write
                    })
                except (OSError, IOError):
                    raise AnsibleError("failed to transfer file from %s to %s" % (in_path, out_path))
        except IOError:
            raise AnsibleError("unable to open local file at: %s" % out_path)

    def close(self):
        super(Connection, self).close()
        if self._conn is not None:
            display.vvv("DISCONNECTED", host=self.guest_socket_path)
            self._conn.close()
            self._conn = None
        self._connected = False
