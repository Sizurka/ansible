#!/bin/sh
umask 0077

# https://medium.com/@pawitp/full-disk-encryption-on-arch-linux-backed-by-tpm-2-0-c0892cab9704
# https://medium.com/@pawitp/the-correct-way-to-use-secure-boot-with-linux-a0421796eade
# https://medium.com/@pawitp/its-certainly-annoying-that-tpm2-tools-like-to-change-their-command-line-parameters-d5d0f4351206
# https://github.com/anguianoewi/tpm2KeyUnlock/blob/master/setup2
# Read PCRs: tpm2_pcrread

{% if tpm_security == "secureboot" or tpm_security == "customshim" %}
SB_ENABLED=$(cat /sys/firmware/efi/efivars/SecureBoot-* 2>/dev/null | dd bs=1 skip=4 status=none | hexdump -n 2 -e '"%d"')
[ "${SB_ENABLED}" = "1" ] || exit 0
{% else %}
if tpm2_unseal -c 0x81000000 -p pcr:sha256:{{ tpm_seal_pcrs }} 2>/dev/null | diff - /root/keys/boot_crypt.bin >/dev/null 2>/dev/null; then
  echo "Key unsealed successfully, disabling update service"
  systemctl disable update-tpm.service
  exit 0
fi
{% endif %}

set -e

export TPM2TOOLS_TCTI_NAME=device
export TPM2TOOLS_DEVICE_FILE=/dev/tpmrm0

tpm2_clear
tpm2_evictcontrol -C o -c 0x81000000 || true

WORKDIR=$(mktemp -d)

tpm2_createpolicy --policy-pcr -l sha256:{{ tpm_seal_pcrs }} -L "${WORKDIR}/policy_digest"
tpm2_createprimary -C e -g sha256 -G rsa -c "${WORKDIR}/primary_context"
tpm2_create -g sha256 \
  -u "${WORKDIR}/obj_pub" -r "${WORKDIR}/obj_priv" -C "${WORKDIR}/primary_context" -L "${WORKDIR}/policy_digest" \
  -a "noda|adminwithpolicy|fixedparent|fixedtpm" -i /root/keys/boot_crypt.bin
tpm2_load -C "${WORKDIR}/primary_context" -u "${WORKDIR}/obj_pub" -r "${WORKDIR}/obj_priv" -c "${WORKDIR}/load_context"
tpm2_evictcontrol -C o -c "${WORKDIR}/load_context" 0x81000000

rm -rf "${WORKDIR}"

{% if tpm_security == "secureboot" or tpm_security == "customshim" %}
systemctl disable update-tpm.service || true
{% endif %}
