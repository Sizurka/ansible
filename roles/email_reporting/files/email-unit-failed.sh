#!/bin/bash

# Undo systemd path interpretation, since we need the actual unit name
UNIT=$(tr '/' '-' <<<"$1")

sendmail root <<EOF
To:root
Subject:$(uname -n) - $UNIT failed

$(uname -a)
$(uptime)

$(systemctl status $UNIT -l -n 50)

$(systemctl list-units --failed)
EOF

