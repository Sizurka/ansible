#!/bin/bash

if [ ! -r "ca.key" ]; then
  echo "ca.key does not exist"
  exit 1
fi

HOSTNAME="$1"
if [ -z "HOSTNAME" ]; then
  echo "hostname output required"
  exit 1
fi
if [ -r "${HOSTNAME}.key" ]; then
  echo "${HOSTNAME}.key already exists"
  exit 1
fi
CN="$2"
if [ -z "$CN" ]; then
  CN="${HOSTNAME}"
fi

umask 0077
set -e

openssl req -new -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 -nodes \
  -addext keyUsage=critical,digitalSignature,keyEncipherment,keyAgreement \
  -addext extendedKeyUsage=serverAuth,clientAuth \
  -subj "/CN=${CN}/" \
  -keyout "${HOSTNAME}.key" -out "${HOSTNAME}.csr"
ansible-vault encrypt "${HOSTNAME}.key"

ansible-vault view ca.key | openssl x509 -req \
  -in "${HOSTNAME}.csr" \
  -CA ca.crt -CAkey /dev/stdin \
  -days 3650 -out "${HOSTNAME}.crt"
chmod u=rw,g=r,o=r "${HOSTNAME}.crt"
rm -f "${HOSTNAME}.csr"
