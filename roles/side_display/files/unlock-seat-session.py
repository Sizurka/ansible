#!/usr/bin/python3


import dbus
import evdev
import time


bus = dbus.SystemBus()
manager = bus.get_object("org.freedesktop.login1", "/org/freedesktop/login1")
auto_proxy = bus.get_object("org.freedesktop.login1", "/org/freedesktop/login1/session/auto")


def find_active_session_proxy():
    seat = auto_proxy.Get("org.freedesktop.login1.Session", "Seat",
                          dbus_interface="org.freedesktop.DBus.Properties")
    seat = str(seat[0])
    if not seat:
        seat = 'seat0'

    uid = auto_proxy.Get("org.freedesktop.login1.Session", "User",
                         dbus_interface="org.freedesktop.DBus.Properties")
    uid = int(uid[0])
    if not uid:
        uid = int("{{ user_info.uid }}")

    for session in manager.ListSessions(dbus_interface="org.freedesktop.login1.Manager"):
        if session[1] != uid:
            continue
        if session[3] != seat:
            continue
        path = session[4]
        return bus.get_object("org.freedesktop.login1", path)
    return None


# Can't use the auto proxy because there's no active seat while locked
session_proxy = find_active_session_proxy()
if session_proxy is None:
    print("Cannot find session for active seat")
    exit(1)

session_proxy.Unlock(dbus_interface="org.freedesktop.login1.Session")

bus.close()


output = evdev.UInput({
    evdev.ecodes.EV_KEY: [evdev.ecodes.KEY_F24]
}, name="remote-input")

output.write(evdev.ecodes.EV_KEY, evdev.ecodes.KEY_F24, 1)
output.syn()
time.sleep(0.1)
output.write(evdev.ecodes.EV_KEY, evdev.ecodes.KEY_F24, 0)
output.syn()

output.close()
