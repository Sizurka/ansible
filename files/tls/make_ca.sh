#!/bin/bash

if [ -r "ca.key" ]; then
  echo "ca.key already exists"
  exit 1
fi

CN="$1"
if [ -z "$CN" ]; then
  CN="CA Root Certificate"
fi

umask 0077
set -e

openssl req -new  -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 -nodes \
  -addext keyUsage=critical,keyCertSign,cRLSign \
  -days 3650 -subj "/CN=${CN}/" \
  -keyout ca.key -x509 -out ca.crt
ansible-vault encrypt ca.key
chmod u=rw,g=r,o=r ca.crt