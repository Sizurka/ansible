#!/usr/bin/python3

import time
import hashlib
import struct
import base64
import sys
import os
import os.path
import re
import json


year, month = (time.localtime())[0:2]

def to_alias():
    with open('/etc/google_api/burner_alias.seed', 'rb') as f:
        seed = f.read()
    seed = seed.strip()
    m = hashlib.sha512()
    m.update(seed)
    m.update(struct.pack('<HB', year, month))
    result = base64.b32encode(m.digest())
    result = result[0:10]
    return 'X.' + result.decode('ascii').lower() + '@{{ domain_name }}'

if len(sys.argv) < 2:
    print(to_alias())
    exit(0)

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import Credentials

scope = [
    'https://www.googleapis.com/auth/admin.directory.user.alias'
]
api_service_name = 'admin'
api_version = 'directory_v1'

secrets_file = '/etc/google_api/burner_alias.secret'
credentials_file = '/etc/google_api/burner_alias.credentials'

credentials = None

def credentials_to_dict():
    return {'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}

def save_credentials():
    with open(credentials_file, 'w') as f:
        json.dump(credentials_to_dict(), f)

def get_authenticated_service():
    global credentials
    if not os.path.exists(credentials_file):
        flow = InstalledAppFlow.from_client_secrets_file(secrets_file, scope)
        credentials = flow.run_local_server()
        save_credentials()
    else:
        with open(credentials_file, 'r') as f:
            credentials = json.load(f)
        credentials = Credentials(**credentials)
    result = build(api_service_name, api_version, credentials=credentials)
    return result

service = get_authenticated_service()

def decrement_month():
    global year, month
    month = month - 1
    if month >= 1:
        return
    month = 12
    year = year - 1
desired = set()
desired.add(to_alias())
decrement_month()
desired.add(to_alias())
decrement_month()
desired.add(to_alias())

pattern = re.compile(r'^(X\.[0-9a-z]{10})@')

existing = service.users().aliases().list(userKey='{{ login.email }}').execute()
for check in existing['aliases']:
    check = check['alias']
    hit = pattern.match(check)
    if not hit:
        continue
    matched = hit.group(1) + "@{{ domain_name }}"
    if matched in desired:
        desired.remove(matched)
        continue

    print("Removing alias: " + check)
    service.users().aliases().delete(userKey='{{ login.email }}', alias=check).execute()

for add in desired:
    data = dict()
    data['alias'] = add
    #data['kind'] = 'admin#directory#alias'

    print("Adding alias: " + add)
    service.users().aliases().insert(userKey='{{ login.email }}', body=data).execute()

save_credentials()
