#!/usr/bin/python3

import urllib.request
import re
import tempfile
import subprocess
import os
import datetime

output_file_name = "/etc/dnsmasq/dynamic-block.hosts"

whitelist = {
    's3.amazonaws.com',
    'clients2.google.com'
    'clients3.google.com',
    'clients4.google.com',
    'clients5.google.com',
    'bit.ly',
    'ow.ly',
    'j.mp',
    'goo.gl',
    'tinyurl.com',
    'msftncsi.com',
    'ea.com',
    'cdn.optimizely.com',
    'gravatar.com',
    'rover.ebay.com',
    'imgs.xkcd.com',
    'netflix.com',
    'alluremedia.com.au',
    'tomshardware.com',
    'ocsp.apple.com',
    's.shopify.com',
    'keystone.mwbsys.com',
    'dl.dropbox.com',
    'api.ipify.org'
}

block = set()


class BlockList:
    def __init__(self, url):
        try:
            with urllib.request.urlopen(url) as response:
                self._lines = response.read().decode('utf-8').splitlines()
        except Exception as ex:
            self._lines = list()
            print("Error downloading %s" % url)
            print(ex)

    @staticmethod
    def _field_or_empty(fields, index):
        if index >= len(fields):
            return ''
        return fields[index]

    def remove_comments(self, identifier='#'):
        self._lines = [self._field_or_empty(l.split(identifier), 0) for l in self._lines]
        return self

    def extract_field(self, index=1, splitter=r'\s+'):
        self._lines = [self._field_or_empty(re.split(splitter, l), index) for l in self._lines]
        return self

    @staticmethod
    def _on_whitelist(hostname):
        for check in whitelist:
            if hostname.endswith(check):
                return True
        return False

    def apply(self):
        for l in self._lines:
            l = l.casefold().strip()
            if len(l) <= 0:
                continue
            if self._on_whitelist(l):
                continue
            block.add(l)


# Get lists from: https://firebog.net/

print("Downloading blocklists")
BlockList('https://v.firebog.net/hosts/static/w3kbl.txt'). \
    remove_comments(). \
    apply()
BlockList(
    'https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt'). \
    remove_comments(). \
    extract_field(). \
    apply()
BlockList('https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser'). \
    remove_comments(). \
    extract_field(). \
    apply()

output = tempfile.NamedTemporaryFile(mode='w', delete=False)
output.write('# Generated at %sZ\n\n' % (datetime.datetime.utcnow().isoformat()))
for host in sorted(block):
    output.write('0.0.0.0 %s\n' % host)
    output.write('::0 %s\n' % host)
output.close()


#print("Checking configuration")
#subprocess.check_call(["dnsmasq", "--test", "-C", output.name])

subprocess.call(["cp", "--no-preserve=mode,ownership,timestamps", output.name, output_file_name])
subprocess.call(["chmod", "u=rw,g=r,o=r", output_file_name])
os.unlink(output.name)

print("Reloading dnsmasq")
subprocess.call(["systemctl", "reload", "dnsmasq.service"])
