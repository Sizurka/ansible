#!/usr/bin/python3

import subprocess
import signal
import platform
import sys
import threading
import pyudev
import time
from pathlib import Path
from fnmatch import fnmatch


device_lock = threading.Lock()
known_devices = dict()
bound_devices = set()
match_devices = list()

usbipd_exec = "usbipd"
usbip_exec = "usbip"
try:
    exact_match = platform.uname().release
    for check in Path("/usr/lib/linux-tools").iterdir():
        if not check.is_dir():
            continue
        if check.name.startswith('.'):
            continue
        check_exec = check / "usbipd"
        if not check.exists():
            continue
        usbipd_exec = check_exec
        usbip_exec = check / "usbip"
        if check.name == exact_match:
            break
except FileNotFoundError:
    pass


def bind_device(busid):
    subprocess.run([usbip_exec, 'bind', '-b', busid],
                   stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    bound_devices.add(busid)
    sys.stdout.write(f"busid={busid}\n")
    sys.stdout.flush()


def unbind_device(busid):
    subprocess.run([usbip_exec, 'unbind', '-b', busid],
                   stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL)


def get_busid(device):
    if device.properties.get('DEVTYPE') != 'usb_device':
        return None
    bus = device.properties.get('BUSNUM')
    if not bus:
        return None
    bus = str(int(bus))
    busid = device.sys_name
    if not busid or not busid.startswith(f"{bus}-"):
        return None
    return busid


def convert_device(device):
    busid = get_busid(device)
    if not busid:
        return None, None
    return busid, {
        'idVendor': "%04x" % int(device.properties.get('ID_VENDOR_ID', '0'), 16),
        'idProduct': "%04x" % int(device.properties.get('ID_MODEL_ID', '0'), 16),
        'product': device.attributes.get('product', b'').decode('utf-8', 'ignore'),
        'manufacturer': device.attributes.get('manufacturer', b'').decode('utf-8', 'ignore'),
        'serial': device.attributes.get('serial', b'').decode('utf-8', 'ignore'),
    }


def convert_auxiliary(device):
    base_device = device
    while True:
        base_device = base_device.parent
        if base_device is None:
            return None, None
        busid = get_busid(base_device)
        if busid:
            break
    aux = dict()
    if device.properties.get('SUBSYSTEM') == 'tty':
        aux['tty'] = "1"
    return busid, aux



def compare_device(info, matcher):
    for key, value in matcher.items():
        check = info.get(key)
        if not check:
            return False
        if not fnmatch(check, value):
            return False
    return True


udev_context = pyudev.Context()
udev = pyudev.Monitor.from_netlink(udev_context)
with device_lock:
    for device in udev_context.list_devices(subsystem='usb', DEVTYPE='usb_device'):
        busid, info = convert_device(device)
        if not busid:
            continue
        known_devices[busid] = info
    for device in udev_context.list_devices():
        busid, aux = convert_auxiliary(device)
        if not busid:
            continue
        existing = known_devices.get(busid)
        if not existing:
            continue
        existing.update(aux)


def monitor_udev():
    for device in iter(udev.poll, None):
        if device.action == 'remove':
            busid = get_busid(device)
            with device_lock:
                known_devices.pop(busid, None)
                bound_devices.discard(busid)
            continue
        if device.action != 'add':
            continue
        busid, info = convert_device(device)
        if not busid:
            busid, aux = convert_auxiliary(device)
            if not busid:
                continue
            with device_lock:
                if busid in bound_devices:
                    continue
                existing = known_devices.get(busid)
                if not existing:
                    continue
                existing.update(aux)
                for matcher in match_devices:
                    if not compare_device(existing, matcher):
                        continue
                    bind_device(busid)
                    break
            continue
        with device_lock:
            known_devices[busid] = info
            for matcher in match_devices:
                if not compare_device(info, matcher):
                    continue
                bind_device(busid)
                break



daemon = subprocess.Popen([usbipd_exec],
                          stdin=subprocess.DEVNULL,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL)
time.sleep(2)

threading.Thread(target=monitor_udev, daemon=True).start()

try:
    def handler(sig, frame):
        signal.raise_signal(signal.SIGINT)
    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGHUP, handler)

    while True:
        try:
            line = sys.stdin.readline()
        except:
            break
        if not line:
            break
        line = line.strip()
        matcher = dict()
        for pair in line.split(','):
            try:
                key, value = pair.split('=', 1)
            except ValueError:
                continue
            matcher[key] = value
        if matcher:
            with device_lock:
                match_devices.append(matcher)
                for busid, info in known_devices.items():
                    if busid in bound_devices:
                        continue
                    if not compare_device(info, matcher):
                        continue
                    bind_device(busid)
finally:
    try:
        daemon.terminate()
        daemon.wait(timeout=5)
    except:
        pass
    try:
        daemon.kill()
        daemon.wait()
    except:
        pass

    with device_lock:
        match_devices.clear()
        known_devices.clear()
        for busid in bound_devices:
            unbind_device(busid)
