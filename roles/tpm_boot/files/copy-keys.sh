#!/bin/bash

DEST="$1"

if [ ! -d "${DEST}" ]; then
  echo "Usage: copy-keys.sh /target/usb/stick"
  exit 1
fi

# Copy *.cer *.esl *.auth to FAT32 flash drive for BIOS installation
# For hardware that needs OpROMs (e.x. Nvidia dGPU), add MS key, boot in secure, extract signatures, then remove MS key

exec cp *.cer *.esl *.auth "${DEST}/"
