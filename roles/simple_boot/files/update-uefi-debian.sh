#!/bin/sh
set -e

# avoid running multiple times
if [ -n "$DEB_MAINT_PARAMS" ]; then
	eval set -- "$DEB_MAINT_PARAMS"
	if [ -z "$1" ] || [ "$1" != "configure" ]; then
		exit 0
	fi
fi

cp -f -L /vmlinuz /boot/linux.efi
cp -f -L /initrd.img /boot/initramfs-linux.img