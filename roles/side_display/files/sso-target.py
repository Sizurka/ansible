#!/usr/bin/python3


import asyncio
import systemd.daemon
import socket
import signal



loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


async def connection(reader: asyncio.StreamReader, writer: asyncio.StreamWriter) -> None:
    data = await reader.read()
    if not data:
        return

    data = data.decode('utf-8')
    proc = await asyncio.create_subprocess_exec("xdg-open", data,
                                                stdin=asyncio.subprocess.DEVNULL)
    await proc.communicate()


def factory():
    reader = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(reader, connection)
    return protocol


for fd in systemd.daemon.listen_fds():
    sock = socket.socket(fileno=fd, type=socket.SOCK_STREAM, family=socket.AF_UNIX, proto=0)
    loop.run_until_complete(loop.create_server(factory, sock=sock))


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(15)
        systemd.daemon.notify("WATCHDOG=1")


main_task = loop.create_task(main())
loop.add_signal_handler(signal.SIGINT, main_task.cancel)
loop.add_signal_handler(signal.SIGTERM, main_task.cancel)
try:
    loop.run_until_complete(main_task)
except asyncio.CancelledError:
    pass
finally:
    systemd.daemon.notify("STOPPING=1")
    exit(0)
