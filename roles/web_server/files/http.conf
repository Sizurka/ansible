server {
	server_name autoproxy.{{ domain_name }};

	listen 80;
	listen [::]:80;

	location /.well-known/acme-challenge {
		root /srv/certbot;
		allow all;
	}

	root /srv/autoproxy;

	allow {{ network.internal.ipv4 }};
	{% if network.internal.ipv6 is defined %}allow {{ network.internal.ipv6 }};{% endif %}
	deny all;
}

server {
	server_name {{ domain_name }} www.{{ domain_name }} ha.{{ domain_name }};

	listen 80 default_server;
	listen [::]:80 default_server;

	location /.well-known/acme-challenge {
		root /srv/certbot;
	}

	location / {
		return 301 https://$host$request_uri;
	}

	if ($host = "www") {
		return 301 $scheme://www.{{ domain_name }}$request_uri;
	}
	if ($host = "ha") {
		return 301 $scheme://ha.{{ domain_name }}$request_uri;
	}
	if ($host = "autoproxy") {
		return 301 $scheme://ha.{{ domain_name }}$request_uri;
	}
}