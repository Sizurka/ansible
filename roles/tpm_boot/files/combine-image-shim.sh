#!/bin/bash
set -e

LINUX="/vmlinuz"
INITRD="/initrd.img"

if [ ! -r "${LINUX}" ]; then LINUX="/boot/vmlinuz-linux"; fi
if [ ! -r "${INITRD}" ]; then INITRD="/boot/initramfs-linux.img"; fi

IMAGE_ROOT=`mktemp -d`
function cleanup {
  rm -rf "${IMAGE_ROOT}"
}
trap cleanup EXIT

# GRUB "helpfully" skips verification of memdisk files, but shim requires something to be verified before it
# sets loader_is_participating.  So we have to keep the kernel image outside the memdisk.
sbsign \
  --key /root/keys/efi/MOK.key \
  --cert /root/keys/efi/MOK.crt \
  --output "/boot/vmlinuz.mok" \
  "${LINUX}"

cp "${INITRD}" "${IMAGE_ROOT}/initrd.img"

cat <<EOF > "${IMAGE_ROOT}/grub.cfg"
set superusers=""
export superusers

set root=(hd0,gpt1)
search --no-floppy --label --set=root efi_0

linux /vmlinuz.mok $(cat /etc/kernel/efi-command-line.txt)
initrd (memdisk)/initrd.img
boot

echo Rebooting the system in 10 seconds
sleep 10
reboot
EOF


cat <<EOF > "${IMAGE_ROOT}/sbat.csv"
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
grub,1,Free Software Foundation,grub,2.06,https://www.gnu.org/software/grub/
EOF


grub-mkstandalone  \
  --directory /usr/lib/grub/x86_64-efi \
  --format x86_64-efi \
  --modules "echo normal linux linuxefi reboot sleep part_gpt fat search search_label" \
  --sbat "${IMAGE_ROOT}/sbat.csv" \
  --output "/boot/grubx64.efi" \
  "/boot/grub/grub.cfg=${IMAGE_ROOT}/grub.cfg" \
  "/initrd.img=${IMAGE_ROOT}/initrd.img"

sbsign \
  --key /root/keys/efi/MOK.key \
  --cert /root/keys/efi/MOK.crt \
  --output "/boot/grubx64.efi" \
  "/boot/grubx64.efi"


systemctl enable update-tpm.service || true
