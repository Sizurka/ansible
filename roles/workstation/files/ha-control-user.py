#!/usr/bin/python3

import sys
import asyncio
import aiohttp
import systemd.daemon
import signal
import json
import traceback
import dbus
import typing
from enum import Enum


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


class Presence(Enum):
    AWAY = 1
    ACTIVE = 2
    SLEEP = 3
presence_state: typing.Optional[Presence] = None
presence_changed = asyncio.Event()

headphones_on: typing.Optional[bool] = None
headphones_changed = asyncio.Event()


async def homeassistant_connection():
    def unpack_message(msg) -> typing.Dict[str, typing.Any]:
        if msg.type == aiohttp.WSMsgType.TEXT:
            return json.loads(msg.data)
        elif msg.type == aiohttp.WSMsgType.BINARY:
            return json.loads(msg.data.decode('utf-8'))
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise RuntimeError

    api_url = "https://ha.{{ domain_name }}/api"

    while True:
        try:
            timeout = aiohttp.ClientTimeout(connect=30, sock_read=60)
            async with aiohttp.ClientSession(timeout=timeout) as session:
                async with session.ws_connect(api_url + "/websocket", heartbeat=10.0) as websocket:
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_required':
                        raise RuntimeError(str(data))
                    await websocket.send_json({
                        'type': 'auth',
                        'access_token': api_key,
                    })
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_ok':
                        raise RuntimeError(str(data))

                    command_id: int = 0
                    async def subscribe_trigger(trigger: typing.Dict[str,typing.Any]) -> None:
                        nonlocal command_id
                        command_id += 1
                        await websocket.send_json({
                            'id': command_id,
                            'type': 'subscribe_trigger',
                            'trigger': trigger,
                        })
                        while True:
                            data = unpack_message(await websocket.receive())
                            if data['type'] != "result":
                                continue
                            if data['id'] != command_id:
                                continue
                            if data['success'] != True:
                                raise RuntimeError(data)
                            return

                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_select.presence',
                        'from': None,
                    })
                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_boolean.headphones_in_use',
                        'from': None,
                    })

                    def dispatch_state(entity_id: str, state: str, attributes: typing.Dict[str, typing.Any]) -> None:
                        if entity_id == 'input_select.presence':
                            global presence_state
                            if state == 'Home':
                                presence_state = Presence.ACTIVE
                            elif state == 'Sleep':
                                presence_state = Presence.SLEEP
                            else:
                                presence_state = Presence.AWAY
                            presence_changed.set()
                        elif entity_id == 'input_boolean.headphones_in_use':
                            global headphones_on
                            if state == 'on':
                                headphones_on = True
                            elif state == 'off':
                                headphones_on = False
                            headphones_changed.set()

                    command_id += 1
                    await websocket.send_json({
                        'id': command_id,
                        'type': 'get_states',
                    })
                    read_states_id = command_id
                    async for msg in websocket:
                        data = unpack_message(msg)
                        if data['type'] == 'event':
                            to_state = data['event']['variables']['trigger']['to_state']
                            dispatch_state(to_state['entity_id'], to_state['state'], to_state['attributes'])
                        elif data['type'] == 'result' and data['id'] == read_states_id:
                            for entity in data['result']:
                                dispatch_state(entity['entity_id'], entity['state'], entity['attributes'])
        except:
            traceback.print_exc()
        await asyncio.sleep(60.0)


homeassistant_connection_t = loop.create_task(homeassistant_connection())


session_bus = None
query_bus_name_pid = None


def open_session_bus():
    global session_bus
    global query_bus_name_pid

    if session_bus is not None:
        session_bus.close()

    session_bus = dbus.SessionBus()
    query_bus_name_pid = session_bus.get_object("org.freedesktop.DBus", "/org/freedesktop/DBus"). \
        get_dbus_method("GetConnectionUnixProcessID", dbus_interface="org.freedesktop.DBus")


def bus_name_exists(name):
    try:
        pid = query_bus_name_pid(name)
        pid = int(pid)
    except dbus.exceptions.DBusException:
        return False
    except ValueError:
        return False
    return pid > 0


def apply_kde_config():
    try:
        obj = session_bus.get_object("org.freedesktop.PowerManagement", "/org/kde/Solid/PowerManagement")
        method = obj.get_dbus_method("refreshStatus", dbus_interface="org.kde.Solid.PowerManagement")
        method()
    except dbus.exceptions.DBusException:
        traceback.print_exc()
    try:
        obj = session_bus.get_object("org.kde.screensaver", "/org/freedesktop/ScreenSaver")
        method = obj.get_dbus_method("configure", dbus_interface="org.kde.screensaver")
        method()
    except dbus.exceptions.DBusException:
        traceback.print_exc()


async def set_dconf(key, value):
    try:
        proc = await asyncio.create_subprocess_exec("dconf", "write", key, value,
                                                    stdin=asyncio.subprocess.DEVNULL)
    except FileNotFoundError:
        return
    await proc.communicate()


async def set_kdeconfig(file, group, key, value):
    groups = []
    if isinstance(group, list):
        for g in group:
            groups.append("--group")
            groups.append(g)
    else:
        groups.append("--group")
        groups.append(group)
    try:
        proc = await asyncio.create_subprocess_exec("kwriteconfig5",
                                                    "--file", file,
                                                    *groups,
                                                    "--key", key, value,
                                                    stdin=asyncio.subprocess.DEVNULL)
    except FileNotFoundError:
        return
    await proc.communicate()


async def update_screen_control():
    current_state = None
    while True:
        await presence_changed.wait()
        presence_changed.clear()

        if presence_state == current_state:
            continue
        current_state = presence_state
        open_session_bus()

        lock_delay = 1200
        if current_state == Presence.ACTIVE:
            lock_delay = 10800
        elif current_state == Presence.SLEEP:
            lock_delay = 900

        applied_to_any = False
        if bus_name_exists("org.gnome.ScreenSaver"):
            await asyncio.gather(
                set_dconf("/org/gnome/desktop/session/idle-delay",
                          "uint32 {}".format(lock_delay)),
                set_dconf("/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout",
                          "{}".format(lock_delay + 300)),
            )
            applied_to_any = True
        if bus_name_exists("org.kde.screensaver"):
            await asyncio.gather(
                set_kdeconfig("powermanagementprofilesrc", ["AC", "DPMSControl"],
                              "idleTime", str(lock_delay)),
                set_kdeconfig("powermanagementprofilesrc", ["AC", "SuspendSession"],
                              "idleTime", str((lock_delay + 300) * 1000)),
                set_kdeconfig("powermanagementprofilesrc", ["AC", "SuspendSession"],
                              "suspendThenHibernate", "false"),
                set_kdeconfig("powermanagementprofilesrc", ["AC", "SuspendSession"],
                              "suspendType", "1"),
                set_kdeconfig("kscreenlockerrc", "Daemon",
                              "Timeout", str(int(lock_delay / 60))),
            )
            apply_kde_config()
            applied_to_any = True

        if not applied_to_any:
            current_state = None

update_screen_control_t = loop.create_task(update_screen_control())


async def set_headphones_enable(enabled: bool) -> None:
    # pactl list cards
    if enabled:
        profile = "output:analog-stereo+input:iec958-stereo"
    else:
        profile = "off"

    proc = await asyncio.create_subprocess_exec("pactl", "--format=json", "list", "cards",
                                                stdin=asyncio.subprocess.DEVNULL,
                                                stdout=asyncio.subprocess.PIPE)
    cards, _ = await proc.communicate()
    cards = json.loads(cards)
    for card in cards:
        properties = card.get("properties")
        if not properties:
            continue

        if properties.get("device.description") == "I'm Fulla Schiit":
            continue

        proc = await asyncio.create_subprocess_exec("pactl", "set-card-profile",
                                                    properties['device.name'],
                                                    profile,
                                                    stdin=asyncio.subprocess.DEVNULL)
        await proc.communicate()


async def update_audio_source():
    while True:
        await headphones_changed.wait()
        headphones_changed.clear()
        await set_headphones_enable(bool(headphones_on))
        await asyncio.sleep(2.0)

update_audio_source_t = loop.create_task(update_audio_source())


def exit_handler(sig, frame):
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


main_t = loop.create_task(main())
loop.run_forever()
