#!/usr/bin/python3

import subprocess
import re
import platform
import sys

counter = re.compile('\s(\d+)$')

error_lines = []

errors = subprocess.Popen(["btrfs", "device", "stats", "-z", sys.argv[1]], stdout=subprocess.PIPE)
for line in errors.stdout:
    line = line.decode('ascii').strip()
    result = counter.search(line)
    if not result:
        continue
    n = int(result.group(1))
    if n <= 0:
        continue
    error_lines.append(line)

errors.wait()
if len(error_lines) == 0:
    exit(0)

output = subprocess.Popen(["sendmail", "root"], stdin=subprocess.PIPE)
output.stdin.write("""To:root
Subject:{} - btrfs errors

""".format(platform.node()).encode('ascii'))
output.stdin.write("\n".join(error_lines).encode('ascii'))
output.stdin.close()
output.wait()
