#!/usr/bin/python3

import uvicorn


def main():
    uvicorn.run('object_detector.server:app', workers=1)


if __name__ == '__main__':
    main()
