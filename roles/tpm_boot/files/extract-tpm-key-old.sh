#!/bin/sh

TARGET="$1"
umask 0077

if [ -z "${TARGET}" ] || [ "${TARGET}" = "-" ]; then
  exec tpm2_unseal -H 0x81000000 -L sha256:{{ tpm_seal_pcrs }}
fi

exec tpm2_unseal -H 0x81000000 -L sha256:{{ tpm_seal_pcrs }} -o "${TARGET}"