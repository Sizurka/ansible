#!/bin/sh

GUEST="$1"
OPERATION="$2"

umask 0077

mkdir -p "/run/libvirt/hook"

if [ "${OPERATION}" = "prepare" ]; then
  cat - > "/run/libvirt/hook/${GUEST}"
  systemctl start "libvirt-guest-prepare@${GUEST}.target"
elif [ "${OPERATION}" = "start" ]; then
  cat - > "/run/libvirt/hook/${GUEST}"
  systemctl start "libvirt-guest-start@${GUEST}.target"
elif [ "${OPERATION}" = "started" ]; then
  cat - > "/run/libvirt/hook/${GUEST}"
  systemctl start "libvirt-guest-started@${GUEST}.target"
elif [ "${OPERATION}" = "stopped" ]; then
  systemctl start "libvirt-guest-stopped@${GUEST}.target"
elif [ "${OPERATION}" = "release" ]; then
  systemctl start "libvirt-guest-release@${GUEST}.target"
  rm -f "/run/libvirt/hook/${GUEST}"
fi

exit 0