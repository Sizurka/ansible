#!/usr/bin/python3

# Most of the RGB control logic taken from OpenRGB

import typing
import collections
import asyncio
import time
import glob
import re
import systemd.daemon
import signal
import psutil
import dbus
from pathlib import Path
from os.path import realpath
import os.path
import matplotlib.cm
import matplotlib.colors

HEAT_GRADIENT = matplotlib.cm.get_cmap('inferno')
# HEAT_GRADIENT = matplotlib.cm.get_cmap('magma')
# HEAT_GRADIENT = matplotlib.colors.LinearSegmentedColormap.from_list("", [[0, 0, 0], [1, 1, 1]])


def level_gradient(*args: typing.Tuple[float, typing.Union[str, typing.List[float]]]) -> matplotlib.colors.LinearSegmentedColormap:
    segments: typing.List[typing.Tuple[float, typing.Union[str, typing.List[float]]]] = list()
    if args[0][0] != 0.0:
        segments.append((0, [0, 0, 0]))
    for s in args:
        if len(segments) > 0:
            segments.append((s[0], segments[-1][1]))
        segments.append(s)
    if segments[-1][0] != 1.0:
        segments.append((1, segments[-1][1]))
    return matplotlib.colors.LinearSegmentedColormap.from_list("", segments)


def scaled_gradient(gradient: typing.Callable[[float], typing.Tuple[float, float, float]],
                    scale: typing.Tuple[float, float, float]) -> typing.Callable[[float], typing.Tuple[float, float, float]]:
    def apply(value: float) -> typing.Tuple[float, float, float]:
        raw = gradient(value)
        return raw[0] * scale[0], raw[1] * scale[1], raw[2] * scale[2]
    return apply


WARNING_GRADIENT = level_gradient(
    (0,     [0, 0, 0]),
    (0.25,  [0, 0, 1]),
    (0.5,   [1, 1, 0]),
    (0.75,  [1, 0, 0]),
)


def to_block_device_stats(*inputs: str) -> typing.List[Path]:
    result: typing.List[Path] = []

    def resolve_item(base: str, globed=False) -> typing.List[Path]:
        if not base.startswith('/'):
            check = Path(f'/sys/class/block/{base}/stat')
            if not check.exists():
                return []
            return [check]
        if not globed and '*' in base:
            contents = []
            for add in glob.glob(base):
                contents.extend(resolve_item(add, True))
            return contents

        lookup = Path(base)
        if not lookup.exists():
            return [Path(f'/sys/class/block/{base}/stat')]
        elif lookup.is_block_device():
            lookup = Path(realpath(str(lookup)))
            return [Path(f'/sys/class/block/{lookup.name}/stat')]
        elif lookup.is_dir():
            return [lookup / 'stat']
        return [lookup]

    for base in inputs:
        result.extend(resolve_item(base))
    return result


def find_hwmon_driver(target: str) -> typing.List[Path]:
    result: typing.List[Path] = list()
    for hwmon in Path("/sys/class/hwmon").iterdir():
        name = hwmon / "name"
        try:
            with name.open('r') as f:
                if f.read().strip() == target:
                    result.append(hwmon)
        except IOError:
            continue
    return result


def find_hwmon_sensor(root: Path, label: str, prefix: str = 'temp', suffix: str = '_input') -> typing.Optional[Path]:
    for check in root.glob(prefix + '*_label'):
        try:
            with check.open('r') as f:
                if f.read().strip() != label:
                    continue
            return check.parent / (check.name[:-6] + suffix)
        except IOError:
            continue
    return None


def fraction_as_byte(value: float) -> int:
    value **= 2.2  # Apply gamma before output to linear space
    return min(255, max(0, int(round(value * 255.0))))


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


class RollingAverage:
    class Value:
        def __init__(self):
            self.time = time.time()

    def __init__(self, total_time: float = 10.0, update_interval: float = 0.5,
                 output: typing.Optional[typing.Callable[[float], typing.Awaitable[None]]] = None):
        self._total_time = total_time
        self._update_interval = update_interval
        self._output = output
        self.points: typing.Deque[RollingAverage.Value] = collections.deque()

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        while True:
            now = time.time()
            add = await self.read()
            if add is not None:
                self.points.append(add)
            cutoff = now - self._total_time
            while len(self.points) > 0 and self.points[0].time < cutoff:
                self.points.popleft()
            value = self.convert()
            await self.output(value)
            sleep_duration = now + self._update_interval - time.time()
            if sleep_duration > 0.0:
                await asyncio.sleep(sleep_duration)

    async def read(self) -> typing.Optional["RollingAverage.Value"]:
        pass

    def convert(self) -> typing.Optional[float]:
        pass

    async def output(self, value: typing.Optional[float]) -> None:
        if self._output is not None:
            await self._output(value)


class RollingMean(RollingAverage):
    class Point(RollingAverage.Value):
        def __init__(self, value):
            super().__init__()
            self.value = value

    def __init__(self, weight: typing.Optional[typing.Callable[[float, "RollingMean.Point"], float]] = None,
                 **kwargs):
        super().__init__(**kwargs)
        self._weight = weight

    async def read(self) -> typing.Optional["RollingMean.Point"]:
        pass

    def weight(self, age: float, point: "RollingMean.Point") -> float:
        if self._weight:
            return self._weight(age, point)
        return 1.0

    def convert(self) -> typing.Optional[float]:
        if len(self.points) <= 0:
            return None
        sum_value = 0.0
        sum_weight = 0.0
        time_origin = self.points[-1].time
        for v in self.points:
            weight = self.weight(time_origin - v.time, v)
            sum_value += v.value * weight
            sum_weight += weight
        if sum_weight == 0.0:
            return None
        return sum_value / sum_weight


class ReadFile(RollingMean):
    def __init__(self, file_path: Path, **kwargs):
        super().__init__(**kwargs)
        self._path = file_path

    async def read(self) -> typing.Optional["RollingMean.Point"]:
        try:
            with self._path.open(mode='r') as f:
                value = float(f.read().strip())
        except (IOError, ValueError):
            return None
        return self.Point(value)


class RollingRate(RollingAverage):
    class Point(RollingAverage.Value):
        def __init__(self, total):
            super().__init__()
            self.total = total

    def __init__(self, weight: typing.Optional[typing.Callable[[float, "RollingRate.Point"], float]] = None,
                 wrap: float = 0xFFFFFFFF, **kwargs):
        super().__init__(**kwargs)
        self._weight = weight
        self.wrap = wrap

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        pass

    def weight(self, index: float, point: "RollingRate.Point") -> float:
        if self._weight:
            return self._weight(index, point)
        return 1.0

    def difference(self, begin: "RollingRate.Point", end: "RollingRate.Point") -> float:
        v_begin = begin.total
        v_end = end.total
        if v_end < v_begin:
            v_end += self.wrap
        return (v_end - v_begin) / (end.time - begin.time)

    def convert(self) -> typing.Optional[float]:
        if len(self.points) <= 1:
            return None
        sum_value = 0.0
        sum_weight = 0.0
        time_origin = self.points[-1].time
        for index in range(1, len(self.points)):
            v_end = self.points[index]
            v_begin = self.points[index-1]
            weight = self.weight(time_origin - v_end.time, v_end)
            sum_value += self.difference(v_begin, v_end) * weight
            sum_weight += weight
        if sum_weight == 0.0:
            return None
        return sum_value / sum_weight


class NormalizedRate(RollingRate):
    def __init__(self, *args, lower: typing.Optional[float] = None, upper: typing.Optional[float] = None,
                 threshold: float = 0.0, **kwargs):
        super().__init__(*args, **kwargs)
        self._lower = lower
        self._upper = upper
        self._threshold = threshold

    def difference(self, begin: RollingRate.Point, end: RollingRate.Point) -> float:
        total_begin: float = self.points[0].total
        total_end: float = self.points[-1].total
        if total_end < total_begin:
            total_end += self.wrap
        total_change = (total_end - total_begin) / (self.points[-1].time - self.points[0].time)
        if self._lower is not None and total_change < self._lower:
            total_change = self._lower
        if self._upper is not None and total_change > self._upper:
            total_change = self._upper
        if total_change <= 0:
            return 0.0

        v_begin = begin.total
        v_end = end.total
        if v_end < v_begin:
            v_end += self.wrap
        point_change = (v_end - v_begin) / (end.time - begin.time)
        point_change /= total_change

        if point_change < self._threshold:
            return 0.0

        return min(1.0, max(0.0, point_change))


class RateFile(RollingRate):
    def __init__(self, file_path: Path, **kwargs):
        super().__init__(**kwargs)
        self._path = file_path

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        try:
            with self._path.open(mode='r') as f:
                value = float(f.read().strip())
        except (IOError, ValueError):
            return None
        return self.Point(value)


class NormalizedRateFile(NormalizedRate):
    def __init__(self, file_path: Path, **kwargs):
        super().__init__(**kwargs)
        self._path = file_path

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        try:
            with self._path.open(mode='r') as f:
                value = float(f.read().strip())
        except (IOError, ValueError):
            return None
        return self.Point(value)


class CPUUtilization(RollingAverage):
    class Point(RollingAverage.Value):
        def __init__(self, total, idle):
            super().__init__()
            self.total = total
            self.idle = idle

    _STAT = Path("/proc/stat")

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        try:
            with self._STAT.open(mode='r') as f:
                counters = [int(v) for v in f.readline().strip().split()[1:]]
            idle = counters[3] + counters[4]
            total = sum(counters)
        except (IOError, ValueError, IndexError):
            return None
        return self.Point(total, idle)

    def convert(self) -> typing.Optional[float]:
        if len(self.points) <= 1:
            return None
        total_ticks = self.points[-1].total - self.points[0].total
        idle_ticks = self.points[-1].idle - self.points[0].idle
        fraction_idle = idle_ticks / total_ticks
        return min(1.0, max(0.0, 1.0 - fraction_idle))


class TotalNetworkRate(NormalizedRate):
    def __init__(self, interfaces: typing.List[str], **kwargs):
        super().__init__(**kwargs)
        self._counters: typing.List[Path] = list()
        for name in interfaces:
            self._counters.append(Path(f'/sys/class/net/{name}/statistics/rx_bytes'))
            self._counters.append(Path(f'/sys/class/net/{name}/statistics/tx_bytes'))

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        total = None
        for counter in self._counters:
            try:
                with counter.open(mode='r') as f:
                    total = (total or 0) + int(f.read().strip())
            except (IOError, ValueError, IndexError):
                continue
        if total is None:
            return None
        return self.Point(total)


class TotalBlockRate(NormalizedRate):
    _FIELD_READ = 2
    _FIELD_WRITE = 6

    def __init__(self, stats: typing.List[Path], **kwargs):
        super().__init__(**kwargs)
        self._stats = stats

    async def read(self) -> typing.Optional["RollingRate.Point"]:
        total = None
        for stats in self._stats:
            try:
                with stats.open(mode='r') as f:
                    fields = f.read().strip().split()
                value = int(fields[self._FIELD_READ]) * 512
                value += int(fields[self._FIELD_WRITE]) * 512
            except (IOError, ValueError, IndexError):
                continue
            total = (total or 0) + value
        if total is None:
            return None
        return self.Point(total)


class DeviceBlink:
    class Device:
        async def read(self) -> typing.List[bool]:
            pass

    def __init__(self, update_interval: float = 0.2,
                 output: typing.Optional[typing.Callable[..., typing.Awaitable[None]]] = None):
        self._update_interval = update_interval
        self._output = output
        self.devices: typing.List[DeviceBlink.Device] = list()

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        while True:
            result: typing.List[bool] = list()
            for interface in self.devices:
                read = await interface.read()
                for i in range(len(read)):
                    while i <= len(result):
                        result.append(False)
                    result[i] = result[i] or read[i]
            await self.output(*result)
            await asyncio.sleep(self._update_interval)

    async def output(self, *result: bool) -> None:
        if self._output is not None:
            await self._output(*result)


class BlinkAccumulator(DeviceBlink):
    class Device(DeviceBlink.Device):
        def __init__(self, threshold: int = 0):
            super().__init__()
            self._prior: typing.List[int] = list()
            self._threshold: int = threshold

        async def fetch(self) -> typing.List[typing.Optional[int]]:
            pass

        async def read(self) -> typing.List[bool]:
            values = await self.fetch()
            result: typing.List[bool] = list()
            for i in range(len(values)):
                if i >= len(self._prior) or not values[i] or not self._prior[i]:
                    result.append(False)
                else:
                    result.append((values[i] - self._prior[i]) > self._threshold)
            self._prior = values
            return result


class NetworkBlink(BlinkAccumulator):
    class Device(BlinkAccumulator.Device):
        def __init__(self, name: str, **kwargs):
            super().__init__(**kwargs)
            root = Path(f'/sys/class/net/{name}/statistics')
            self._rx = root / 'rx_bytes'
            self._tx = root / 'tx_bytes'

        async def fetch(self) -> typing.List[typing.Optional[int]]:
            try:
                with self._rx.open(mode='r') as f:
                    rx = int(f.read().strip())
                with self._tx.open(mode='r') as f:
                    tx = int(f.read().strip())
            except (IOError, ValueError):
                return [None, None]
            return [rx, tx]

    def __init__(self, *interface_names: str, threshold_rate: typing.Optional[float] = None, **kwargs):
        super().__init__(**kwargs)
        threshold = 0
        if threshold_rate:
            threshold = round(threshold_rate / self._update_interval)
        for name in interface_names:
            self.devices.append(self.Device(name, threshold=threshold))


class BlockDeviceBlink(BlinkAccumulator):
    class Device(BlinkAccumulator.Device):
        def __init__(self, stats: Path, **kwargs):
            super().__init__(**kwargs)
            self._stats = stats

        async def fetch(self) -> typing.List[typing.Optional[int]]:
            try:
                with self._stats.open(mode='r') as f:
                    fields = f.read().strip().split()
                read = int(fields[2]) * 512
                write = int(fields[6]) * 512
            except (IOError, ValueError):
                return [None, None]
            return [read, write]

    def __init__(self, device_stats: typing.List[Path], threshold_rate: typing.Optional[float] = None, **kwargs):
        super().__init__(**kwargs)
        threshold = 0
        if threshold_rate:
            threshold = round(threshold_rate / self._update_interval)
        for stats in device_stats:
            self.devices.append(self.Device(stats, threshold=threshold))


class ConnectedUsers:
    _LOCAL_TERMINAL = re.compile(r'ttyS?\d+')

    def __init__(self, update_interval: float = 1.0,
                 output: typing.Optional[typing.Callable[..., typing.Awaitable[None]]] = None):
        self._update_interval = update_interval
        self._output = output

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        while True:
            local = 0
            remote = 0
            for user in psutil.users():
                if user.terminal and self._LOCAL_TERMINAL.fullmatch(user.terminal):
                    local += 1
                else:
                    remote += 1
            await self.output(local, remote)
            await asyncio.sleep(self._update_interval)

    async def output(self, local: int, remote: int) -> None:
        if self._output is not None:
            await self._output(local, remote)


class UnitsFailed:
    def __init__(self, update_interval: float = 1.0,
                 output: typing.Optional[typing.Callable[[float], typing.Awaitable[None]]] = None):
        self._update_interval = update_interval
        self._output = output

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        bus = dbus.SystemBus()
        proxy = bus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
        while True:
            await asyncio.sleep(self._update_interval)
            try:
                failed = proxy.ListUnitsFiltered(['failed'], dbus_interface='org.freedesktop.systemd1.Manager')
            except dbus.DBusException:
                continue
            count_failed = 0
            for f in failed:
                # snapd update process breaks systemd mount status by deleting the unit files while still lazy mounted
                if f[2] == 'not-found' and f[0].startswith('var-lib-snapd-snap-') and f[0].endswith('.mount'):
                    continue
                count_failed += 1
            await self._output(float(count_failed))

    async def output(self, value: typing.Optional[float]) -> None:
        if self._output is not None:
            await self._output(value)


class MemoryUtilization:
    def __init__(self, update_interval: float = 1.0,
                 output: typing.Optional[typing.Callable[[float], typing.Awaitable[None]]] = None):
        self._update_interval = update_interval
        self._output = output

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        while True:
            await asyncio.sleep(self._update_interval)
            mem = psutil.virtual_memory()
            await self._output(float(mem.percent) / 100.0)

    async def output(self, value: typing.Optional[float]) -> None:
        if self._output is not None:
            await self._output(value)


class SingleColorGradient:
    def __init__(self, color: typing.Union[str, typing.List[float]],
                 begin: typing.Union[str, typing.List[float]] = None,
                 threshold: float = 0.0):
        if begin is None:
            begin = "black"
        self._gradient = matplotlib.colors.LinearSegmentedColormap.from_list("", [
            (threshold, begin),
            (1.0, color)
        ])
        self._threshold = threshold

    def __call__(self, fraction: float) -> typing.Tuple[float, float, float]:
        if fraction is None:
            return 0.0, 0.0, 0.0
        if fraction < self._threshold:
            return 0.0, 0.0, 0.0
        fraction = min(1.0, max(0.0, fraction))
        return self._gradient(fraction)


class InterpolateGradient:
    def __init__(self, lower: float = 0.0, upper: float = 1.0, threshold: float = 0.0,
                 gradient: typing.Callable[[float], typing.Tuple[float, float, float]] = HEAT_GRADIENT):
        self._threshold = threshold
        self._gradient = gradient
        self._offset = lower
        self._convert = 1.0 / (upper - lower)

    def __call__(self, value: float) -> typing.Tuple[float, float, float]:
        if value is None:
            return 0.0, 0.0, 0.0
        fraction = (value - self._offset) * self._convert
        if fraction < self._threshold:
            return 0.0, 0.0, 0.0
        fraction = min(1.0, max(0.0, fraction))
        return self._gradient(fraction)


class DecayWeight:
    def __init__(self, factor: float = 0.4):
        self._factor = factor

    def __call__(self, age: float, *args, **kwargs) -> float:
        return self._factor ** age


class DebugRGB:
    async def __call__(self, index, rgb: typing.Tuple[float, float, float]):
        index = str(index)
        print(f"{index} = {rgb[0]:.2f} {rgb[1]:.2f} {rgb[2]:.2f}")

    def __getattr__(self, item):
        return item


# {% if rgb_controller == "Gigabyte" +%}
"""
class GigabyteFusion2I2C:
    _ADDRESS = 0x68
    _REG_APPLY = 0x17
    _REG_LED = 0x20

    _ACTION_APPLY = 0x01ff

    _DATA_SIZE = 16

    _IDX_MODE = 0x1
    _IDX_BRIGHTNESS = 0x2
    _IDX_BLUE = 0x4
    _IDX_GREEN = 0x5
    _IDX_RED = 0x6

    _MODE_STATIC = 0x4

    OUTPUT_CPU = 0
    OUTPUT_MOTHERBOARD_LOGO = 2
    OUTPUT_CASE_REAR = 3
    OUTPUT_CASE = 4
    OUTPUT_ARGB_HEADER_1 = 7
    OUTPUT_ARGB_HEADER_2 = 8

    def __init__(self, devname="/dev/i2c-rgb"):
        import smbus
        if type(devname) is int:
            busnumber = devname
        else:
            busnumber = int(os.path.realpath(devname).split('-')[-1])
        self._bus = smbus.SMBus(busnumber)
        self._data = bytearray(10 * self._DATA_SIZE)

    def _set_data(self, index: int, rgb: typing.Tuple[float, float, float]):
        offset = index * self._DATA_SIZE
        self._data[offset + self._IDX_MODE] = self._MODE_STATIC
        self._data[offset + self._IDX_RED] = fraction_as_byte(rgb[0])
        self._data[offset + self._IDX_GREEN] = fraction_as_byte(rgb[1])
        self._data[offset + self._IDX_BLUE] = fraction_as_byte(rgb[2])
        self._data[offset + self._IDX_BRIGHTNESS] = 0x64

    def _send_update(self, index: int):
        begin = int(index * self._DATA_SIZE / 32) * 32
        self._bus.write_block_data(self._ADDRESS, self._REG_LED + begin, self._data[begin:begin+32])

    def _apply(self):
        self._bus.write_byte_data(self._ADDRESS, self._REG_APPLY, self._ACTION_APPLY)

    async def __call__(self, index: int, rgb: typing.Tuple[float, float, float]):
        self._set_data(index, rgb)
        self._send_update(index)
        self._apply()
motherboard = GigabyteFusion2I2C()
"""


class GigabyteFusion2USB:
    def __init__(self):
        import hid
        self._device = hid.device()
        try:
            self._device.open(0x048D, 0x5702)
        except IOError:
            self._device.open(0x048D, 0x8297)

        # Set calibration
        self._write(bytes([
            0xCC, 0x33,

            # D_LED1 WS2812 GRB, 0x00RRGGBB to 0x00GGRRBB
            0x02, 0x00, 0x01, 0x00,
            # D_LED2 WS2812 GRB
            0x02, 0x00, 0x01, 0x00,
            # LED C1/C2 12vGRB, seems pins already connect to LEDs correctly
            0x00, 0x01, 0x02, 0x00,
            # Spare
            0x00, 0x01, 0x02, 0x00,
        ]))

        # Init
        self._write(bytes([0xCC, 0x60]))
        # Get configuration
        report = self._device.get_feature_report(0xCC, 64)
        total_leds = int(report[3])
        self._byteorder: typing.List[typing.Tuple[int, int, int]] = [
            (int(report[44 + 2]), int(report[44 + 1]), int(report[44 + 0])),
            (int(report[48 + 2]), int(report[48 + 1]), int(report[48 + 0])),
            (int(report[52 + 2]), int(report[52 + 1]), int(report[52 + 0])),
        ]

        # Disable beat
        self._write(bytes([0xCC, 0x31, 0x00]))
        # Disable builtin effects
        self._write(bytes([0xCC, 0x32, 0x00]))
        # Turn off all LEDs
        for index in range(0x20, 0x28):
            self._write(bytes([0xCC, index, 1 << (index - 0x20)]))
        self._write(bytes([0xCC, 0x28, 0xFF]))

    def _write(self, data: bytes):
        if len(data) < 64:
            data = data.ljust(64, b'\0')
        self._device.send_feature_report(data)

    async def __call__(self, index: int, rgb: typing.Tuple[float, float, float]):
        self._write(bytes([
            0xCC, index,
            1 << (index - 0x20), 0x00, 0x00, 0x00,  # Zone 0
            0x00, 0x00, 0x00, 0x00,  # Zone 1
            0x00,   # Reserved 0
            0x01,   # Static
            100,    # Max brightness
            0,      # Min brightness
            fraction_as_byte(rgb[2]),
            fraction_as_byte(rgb[1]),
            fraction_as_byte(rgb[0]),
        ]))
        self._write(bytes([0xCC, 0x28, 0xFF]))
motherboard = GigabyteFusion2USB()
# {% endif +%}


# {% if rgb_controller == "ASRock" +%}
"""
class AsrockPolychromeV2:
    _ADDRESS = 0x6A
    _REG_MODE = 0x30
    _REG_LED_SELECT = 0x31
    _REG_LED_COUNT = 0x32
    _REG_LED_COLOR = 0x34

    _MODE_OFF = 0x10
    _MODE_STATIC = 0x11

    OUTPUT_RGB_1 = 0
    OUTPUT_RGB_2 = 1
    OUTPUT_AUDIO = 2
    OUTPUT_PCH = 3
    OUTPUT_IO_COVER = 4

    def __init__(self, devname="/dev/i2c-rgb"):
        import smbus
        if type(devname) is int:
            busnumber = devname
        else:
            busnumber = int(os.path.realpath(devname).split('-')[-1])
        self._bus = smbus.SMBus(busnumber)
        self._set_mode()

    async def _select_led(self, index: int):
        self._bus.write_byte_data(self._ADDRESS, self._REG_LED_SELECT, index)
        await asyncio.sleep(0.001)

    async def _set_data(self, rgb: typing.Tuple[float, float, float]):
        data = bytes((fraction_as_byte(rgb[0]), fraction_as_byte(rgb[1]), fraction_as_byte(rgb[2])))
        self._bus.write_block_data(self._ADDRESS, self._REG_LED_COLOR, data)
        await asyncio.sleep(0.001)

    def _set_mode(self):
        self._bus.write_byte_data(self._ADDRESS, self._REG_MODE, self._MODE_STATIC)
        time.sleep(0.001)
        self._bus.write_byte(self._ADDRESS, self._REG_LED_COUNT)
        time.sleep(0.001)

    async def __call__(self, index: int, rgb: typing.Tuple[float, float, float]):
        await self._select_led(index)
        await self._set_data(rgb)
motherboard = AsrockPolychromeV2()
"""
# {% endif +%}


# {% if rgb_controller == "Asus" +%}
class AsusAuraUSB:
    _CHANNEL_BOARD = 0x04

    def __init__(self):
        import hid
        self._device = hid.device()
        try:
            self._device.open(0x0B05, 0x18F3)
        except OSError:
            self._device.open(0x0B05, 0x1939)

        self._write(bytes([0xEC, 0xB0]))
        config_table = self._read()
        assert config_table[1] == 0x30
        config_table = config_table[4:]
        self._n_addressable = config_table[0x02]
        self._n_board = config_table[0x1B]

        # Set both types to direct mode
        self._write(bytes([
            0xEC, 0x35,
            0x00,
            0x00, 0x00, 0xFF
        ]))
        self._write(bytes([
            0xEC, 0x35,
            0x01,
            0x00, 0x00, 0xFF
        ]))

        self._pending_update = asyncio.Event()
        self._pending_data: typing.Dict[int, typing.List[int]] = dict()
        self._active_data: typing.Dict[int, typing.List[int]] = dict()

        self._board_led_data = [0] * self._n_board * 3
        self._addressable_led_data: typing.Dict[int, typing.List[int]] = dict()
        self._send_update(self._CHANNEL_BOARD, self._board_led_data)

        loop.create_task(self._update_task())

    def _write(self, data: bytes):
        if len(data) < 65:
            data = data.ljust(65, b'\0')
        self._device.write(data)

    def _read(self):
        return self._device.read(65)

    def _apply(self, direct_id: int):
        self._write(bytes([
            0xEC, 0x40,
            0x80 | direct_id,
        ]))

    def _send_update(self, direct_id: int, data: typing.List[int]):
        total_leds = int(len(data)/3)
        for index in range(0, total_leds, 20):
            packet_led_count = min(total_leds - index, 20)
            self._write(bytes([
                  0xEC, 0x40,
                  direct_id,
                  index, packet_led_count,
              ] + data[(index*3):((index+packet_led_count)*3)]))
        self._apply(direct_id)

    async def _update_task(self):
        next_full_update = time.time() + 30
        while True:
            try:
                time_until_update = next_full_update - time.time()
                if time_until_update <= 0.1:
                    raise asyncio.TimeoutError()
                await asyncio.wait_for(self._pending_update.wait(), timeout=time_until_update)
            except asyncio.TimeoutError:
                next_full_update = time.time() + 30
                self._pending_data = dict(self._active_data)
                self._active_data.clear()
            self._pending_update.clear()
            for direct_id, data in self._pending_data.items():
                self._send_update(direct_id, data)
                self._active_data[direct_id] = list(data)
            self._pending_data.clear()

    async def __call__(self, index: typing.Union[int, typing.Tuple[int, int]], rgb: typing.Tuple[float, float, float]):
        if isinstance(index, tuple):
            direct_id = index[0]
            if index[0] not in self._addressable_led_data:
                self._addressable_led_data[index[0]] = list()
            data = self._addressable_led_data[index[0]]
            index = index[1]
            while len(data) <= index*3:
                data.extend([0, 0, 0])
            # WS2812 byte ordering
            rgb = (rgb[1], rgb[0], rgb[2])
        else:
            direct_id = self._CHANNEL_BOARD
            data = self._board_led_data
        index *= 3
        data[index:index + 3] = [
            fraction_as_byte(rgb[0]),
            fraction_as_byte(rgb[1]),
            fraction_as_byte(rgb[2]),
        ]

        if self._active_data.get(direct_id) == data:
            return
        self._pending_data[direct_id] = data
        self._pending_update.set()
motherboard = AsusAuraUSB()
# {% endif +%}


# motherboard = DebugRGB()


class ColorMerge:
    class _Input:
        def __init__(self, merger: "ColorMerge", interpolate: typing.Callable[[float], typing.Tuple[float, float, float]]):
            self._interpolate = interpolate
            self._merge = merger
            self.rgb = None
            self.ready = asyncio.Event()

        async def __call__(self, value: float) -> None:
            self.rgb = self._interpolate(value)
            self.ready.set()

    def __init__(self, output):
        self._inputs: typing.List[ColorMerge._Input] = list()
        self._output = output

    async def output(self, rgb: typing.Tuple[float, float, float]) -> None:
        await motherboard(self._output, rgb)

    def input(self, interpolate: typing.Callable[[float], typing.Tuple[float, float, float]]) -> typing.Callable[[float], typing.Awaitable[None]]:
        add = self._Input(self, interpolate)
        self._inputs.append(add)
        return add

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        while True:
            await asyncio.wait([asyncio.create_task(i.ready.wait()) for i in self._inputs],
                               return_when=asyncio.FIRST_COMPLETED)
            for i in self._inputs:
                i.ready.clear()

            rgb = [0.0, 0.0, 0.0]
            for i in self._inputs:
                add = i.rgb
                if add is None:
                    continue
                rgb[0] += add[0]
                rgb[1] += add[1]
                rgb[2] += add[2]
            for i in range(len(rgb)):
                rgb[i] = min(1.0, max(0.0, rgb[i]))

            await self.output(tuple(rgb))


class BasicTemperature(ReadFile):
    def __init__(self, output, hwmon: typing.Union[str, Path],
                 t_min: float, t_max: float, sensor: str = 'temp1_input',
                 gradient: typing.Callable[[float], typing.Tuple[float, float, float]] = HEAT_GRADIENT,
                 **kwargs):
        if isinstance(hwmon, str):
            if '*' in hwmon:
                hwmon = glob.glob(hwmon)[0]
            hwmon = Path(hwmon)
        super().__init__(hwmon / sensor, **kwargs)
        self._output = output
        self._interpolate = InterpolateGradient(t_min * 1000, t_max * 1000, gradient=gradient)

    async def output(self, value: typing.Optional[float]) -> None:
        await motherboard(self._output, self._interpolate(value))


class NVidiaGPU(RollingMean):
    def __init__(self, output, v_min: float, v_max: float, column: int = 2,
                 gradient: typing.Callable[[float], typing.Tuple[float, float, float]] = HEAT_GRADIENT,
                 **kwargs):
        super().__init__(**kwargs)
        self._output = output
        self._column = column
        self._interpolate = InterpolateGradient(v_min, v_max, gradient=gradient)
        self._smi = None

    async def _launch_smi(self):
        if self._smi:
            try:
                self._smi.terminate()
            except:
                pass
            try:
                await self._smi.wait()
            except:
                pass
            await asyncio.sleep(5)
        else:
            await asyncio.sleep(30)
        self._smi = await asyncio.create_subprocess_exec("nvidia-smi", "dmon",
                                                         stdout=asyncio.subprocess.PIPE,
                                                         stdin=asyncio.subprocess.DEVNULL)

    async def __call__(self, *args, **kwargs) -> typing.NoReturn:
        await self._launch_smi()

    async def output(self, value: typing.Optional[float]) -> None:
        await motherboard(self._output, self._interpolate(value))

    async def read(self) -> typing.Optional["RollingMean.Point"]:
        try:
            line = await self._smi.stdout.readline()
            if not line:
                raise IOError
            line = line.decode("ascii").strip()
            if line.startswith('#'):
                return None
            fields = line.split()
            value = float(fields[self._column])
        except (IOError, ValueError, IndexError):
            await self._launch_smi()
            return None
        return self.Point(value)


# {% if 'cpu' in rgb_status +%}
cpu_sensor = None
cpu_temp = find_hwmon_driver('coretemp')
if cpu_temp:
    cpu_sensor = 'temp1_input'

if not cpu_temp:
    cpu_temp = find_hwmon_driver('k10temp')
    if cpu_temp:
        cpu_sensor = find_hwmon_sensor(cpu_temp[0], 'Tdie')
        if not cpu_sensor:
            cpu_sensor = find_hwmon_sensor(cpu_temp[0], 'Tctl')

if cpu_temp and cpu_sensor:
    # noinspection PyUnresolvedReferences,PyTypeChecker
    loop.create_task(BasicTemperature(
        {{ rgb_status.cpu.output }},
        cpu_temp[0],
        {{ rgb_status.cpu.min | default(50) }},
        {{ rgb_status.cpu.max | default(90) }},
        cpu_sensor,
        gradient={{ rgb_status.cpu.gradient | default("HEAT_GRADIENT") }},
    )())
# {% endif +%}


# {% if 'gpu' in rgb_status +%}
if Path("/sys/class/drm/card1/device/driver").is_symlink() and \
        os.readlink("/sys/class/drm/card1/device/driver").endswith("/nvidia"):
    # noinspection PyUnresolvedReferences,PyTypeChecker
    loop.create_task(NVidiaGPU(
        {{ rgb_status.gpu.output }},
        {{ rgb_status.gpu.min | default(50) }},
        {{ rgb_status.gpu.max | default(90) }},
        gradient={{ rgb_status.gpu.gradient | default("HEAT_GRADIENT") }},
    )())
else:
    # noinspection PyUnresolvedReferences,PyTypeChecker
    loop.create_task(BasicTemperature(
        {{ rgb_status.gpu.output }},
        '/sys/class/drm/card1/device/hwmon/*',
        {{ rgb_status.gpu.min | default(50) }},
        {{ rgb_status.gpu.max | default(95) }},
        gradient={{ rgb_status.gpu.gradient | default("HEAT_GRADIENT") }},
    )())
# {% endif +%}


class NetworkRate(ColorMerge):
    def __init__(self, device: str, output: typing.Any, **kwargs):
        super().__init__(output)

        self._stats = Path(f'/sys/class/net/{device}/statistics')
        self._output = output

        minimum_rate = kwargs.get('minimum_rate', 10E3)
        maximum_rate = kwargs.get('maximum_rate', 10E6)

        gradient = kwargs.get('rx_gradient', SingleColorGradient([0, 1, 0]))
        loop.create_task(NormalizedRateFile(
            self._stats / 'rx_bytes',
            lower=kwargs.get('rx_minimum_rate', minimum_rate),
            upper=kwargs.get('rx_maximum_rate', maximum_rate),
            update_interval=0.25, total_time=5.0, weight=DecayWeight(),
            output=self.input(gradient))())

        gradient = kwargs.get('tx_gradient', SingleColorGradient([0, 0, 1]))
        loop.create_task(NormalizedRateFile(
            self._stats / 'tx_bytes',
            lower=kwargs.get('tx_minimum_rate', minimum_rate),
            upper=kwargs.get('tx_maximum_rate', maximum_rate),
            update_interval=0.25, total_time=5.0, weight=DecayWeight(),
            output=self.input(gradient))())


# {% if 'network' in rgb_status +%}
# {% for network in rgb_status.network +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(NetworkRate(
    '{{ network.device }}',
    {{ network.output }},
    # {% if 'minimum' in network +%}
    minimum_rate={{ network.minimum }},
    # {% endif +%}
    # {% if 'maximum' in network +%}
    maximum_rate={{ network.maximum }},
    # {% endif +%}
    
    # {% if 'rx_minimum' in network +%}
    rx_minimum_rate={{ network.rx_minimum }},
    # {% endif +%}
    # {% if 'rx_maximum' in network +%}
    rx_maximum_rate={{ network.rx_maximum }},
    # {% endif +%}
    # {% if 'rx_gradient' in network +%}
    rx_gradient={{ network.rx_gradient }},
    # {% endif +%}
    
    # {% if 'tx_minimum' in network +%}
    tx_minimum_rate={{ network.tx_minimum }},
    # {% endif +%}
    # {% if 'tx_maximum' in network +%}
    tx_maximum_rate={{ network.tx_maximum }},
    # {% endif +%}
    # {% if 'tx_gradient' in network +%}
    tx_gradient={{ network.tx_gradient }},
    # {% endif +%}
)())
# {% endfor +%}
# {% endif +%}


def network_blink_output(output: typing.Any, **kwargs) -> typing.Callable[[bool, bool], typing.Awaitable[None]]:
    rgb_rx = kwargs.get('rx', [0, 1, 0])
    rgb_tx = kwargs.get('tx', [0, 0, 1])

    async def result(rx: bool, tx: bool) -> None:
        rgb = [0.0, 0.0, 0.0]
        if rx:
            rgb[0] += rgb_rx[0]
            rgb[1] += rgb_rx[1]
            rgb[2] += rgb_rx[2]
        if tx:
            rgb[0] += rgb_tx[0]
            rgb[1] += rgb_tx[1]
            rgb[2] += rgb_tx[2]
        for i in range(len(rgb)):
            rgb[i] = min(1.0, max(0.0, rgb[i]))
        # noinspection PyTypeChecker
        await motherboard(output, tuple(rgb))

    return result

# {% if 'network_blink' in rgb_status +%}
# {% for network in rgb_status.network_blink +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(NetworkBlink(
    '{{ network.device }}',
    output=network_blink_output(
        {{ network.output }},
        # {% if 'rx' in network +%}
        rx={{ network.rx }},
        # {% endif +%}
        # {% if 'tx' in network +%}
        tx={{ network.tx }},
        # {% endif +%}
    ),
    threshold_rate={{ network.threshold|default("None") }}
)())
# {% endfor +%}
# {% endif +%}


class BlockRate(ColorMerge):
    _FIELD_READ = 2
    _FIELD_WRITE = 6
    
    class _Rate(NormalizedRate):
        def __init__(self, stats: typing.List[Path], field: int, **kwargs):
            super().__init__(**kwargs)
            self._stats = stats
            self._field = field

        async def read(self) -> typing.Optional["RollingRate.Point"]:
            total = None
            for stats in self._stats:
                try:
                    with stats.open(mode='r') as f:
                        fields = f.read().strip().split()
                    value = int(fields[self._field]) * 512
                except (IOError, ValueError, IndexError):
                    continue
                total = (total or 0) + value
            if total is None:
                return None
            return self.Point(total)

    def __init__(self, stats: typing.List[Path], output, **kwargs):
        super().__init__(output)

        minimum_rate = kwargs.get('minimum_rate', None)
        maximum_rate = kwargs.get('maximum_rate', 100E6)

        gradient = kwargs.get('read_gradient', SingleColorGradient([0, 0, 1]))
        loop.create_task(self._Rate(stats, field=self._FIELD_READ,
                                    lower=kwargs.get('read_minimum_rate', minimum_rate),
                                    upper=kwargs.get('read_maximum_rate', maximum_rate),
                                    update_interval=0.25, total_time=5.0, weight=DecayWeight(),
                                    output=self.input(gradient))())

        gradient = kwargs.get('write_gradient', SingleColorGradient([0, 0, 1]))
        loop.create_task(self._Rate(stats, field=self._FIELD_WRITE,
                                    lower=kwargs.get('write_minimum_rate', minimum_rate),
                                    upper=kwargs.get('write_maximum_rate', maximum_rate),
                                    update_interval=0.25, total_time=5.0, weight=DecayWeight(),
                                    output=self.input(gradient))())


# {% if 'block' in rgb_status +%}
# {% for block in rgb_status.block +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(BlockRate(
    to_block_device_stats(
        # {% for device in block.devices|default(root_devices) +%}
        '{{ device }}',
        # {% endfor +%}
    ),
    {{ block.output }},
    # {% if 'minimum' in block +%}
    minimum_rate={{ block.minimum }},
    # {% endif +%}
    # {% if 'maximum' in block +%}
    maximum_rate={{ block.maximum }},
    # {% endif +%}

    # {% if 'read_minimum' in block +%}
    read_minimum_rate={{ block.read_minimum }},
    # {% endif +%}
    # {% if 'read_maximum' in block +%}
    read_maximum_rate={{ block.read_maximum }},
    # {% endif +%}
    # {% if 'read_gradient' in block +%}
    read_gradient={{ block.read_gradient }},
    # {% endif +%}

    # {% if 'write_minimum' in block +%}
    write_minimum_rate={{ block.write_minimum }},
    # {% endif +%}
    # {% if 'write_maximum' in block +%}
    write_maximum_rate={{ block.write_maximum }},
    # {% endif +%}
    # {% if 'write_gradient' in block +%}
    write_gradient={{ block.write_gradient }},
    # {% endif +%}
)())
# {% endfor +%}
# {% endif +%}


def block_blink_output(output: typing.Any, **kwargs) -> typing.Callable[[bool, bool], typing.Awaitable[None]]:
    rgb_read = kwargs.get('read', [0, 0, 1])
    rgb_write = kwargs.get('write', [1, 0, 0])

    async def result(read: bool, write: bool) -> None:
        rgb = [0.0, 0.0, 0.0]
        if read:
            rgb[0] += rgb_read[0]
            rgb[1] += rgb_read[1]
            rgb[2] += rgb_read[2]
        if write:
            rgb[0] += rgb_write[0]
            rgb[1] += rgb_write[1]
            rgb[2] += rgb_write[2]
        for i in range(len(rgb)):
            rgb[i] = min(1.0, max(0.0, rgb[i]))
        # noinspection PyTypeChecker
        await motherboard(output, tuple(rgb))

    return result


# {% if 'block_blink' in rgb_status +%}
# {% for block in rgb_status.block_blink +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(BlockDeviceBlink(
    to_block_device_stats(
        # {% for device in block.devices|default(root_devices) +%}
        '{{ device }}',
        # {% endfor +%}
    ),
    output=block_blink_output(
        {{ block.output }},
        # {% if 'read' in block +%}
        read={{ network.read }},
        # {% endif +%}
        # {% if 'write' in block +%}
        write={{ network.write }},
        # {% endif +%}
    ),
    threshold_rate={{ block.threshold|default("None") }}
)())
# {% endfor +%}
# {% endif +%}


# Path(glob.glob('/sys/devices/platform/nct*/hwmon/*')[0])
# {% if 'sensors' in rgb_status +%}
# {% for sensor in rgb_status.sensors +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(BasicTemperature(
    {{ sensor.output}},
    {{ sensor.path }},
    {{ sensor.min }},
    {{ sensor.max }},
    {{ sensor.sensor|default("temp1_input") }},
    gradient={{ sensor.gradient | default("HEAT_GRADIENT") }},
)())
# {% endfor +%}
# {% endif +%}


class CombinedLoad(ColorMerge):
    def __init__(self, output,
                 blocks: typing.List[typing.Dict[str, typing.Any]],
                 networks: typing.List[typing.Dict[str, typing.Any]],
                 **kwargs):
        super().__init__(output)

        gradient = kwargs.get('cpu_gradient', SingleColorGradient([0, 1, 0], [0, 0.2, 0]))
        loop.create_task(CPUUtilization(
            output=self.input(gradient)
        )())

        alternator = False

        minimum_rate = kwargs.get('block_minimum_rate', None)
        maximum_rate = kwargs.get('block_maximum_rate', 100E6)
        for block_info in blocks:
            alternator = not alternator
            gradient = block_info.get(
                'gradient',
                SingleColorGradient([alternator and 0 or 1, 0, alternator and 1 or 0]))
            loop.create_task(TotalBlockRate(
                block_info['devices'],
                lower=block_info.get('minimum_rate', minimum_rate),
                upper=block_info.get('maximum_rate', maximum_rate),
                update_interval=0.25, total_time=5.0, weight=DecayWeight(),
                output=self.input(gradient)
            )())

        minimum_rate = kwargs.get('net_minimum_rate', None)
        maximum_rate = kwargs.get('net_maximum_rate', 10E6)
        for net_info in networks:
            alternator = not alternator
            gradient = net_info.get(
                'gradient',
                SingleColorGradient([alternator and 0 or 1, 0, alternator and 1 or 0]))
            loop.create_task(TotalNetworkRate(
                net_info['devices'],
                lower=net_info.get('minimum_rate', minimum_rate),
                upper=net_info.get('maximum_rate', maximum_rate),
                update_interval=0.25, total_time=5.0, weight=DecayWeight(),
                output=self.input(gradient)
            )())


# {% if 'combined_load' in rgb_status +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(CombinedLoad(
    {{ rgb_status.combined_load.output }},
    blocks=[
    # {% for block in rgb_status.combined_load.block|default([]) +%}
    {
        'devices': to_block_device_stats(
            # {% for device in block.devices|default(root_devices) +%}
            '{{ device }}',
            # {% endfor +%}
        ),
        # {% if 'minimum' in block +%}
        'minimum_rate': {{ block.minimum }},
        # {% endif +%}
        # {% if 'maximum' in block +%}
        'maximum_rate': {{ block.maximum }},
        # {% endif +%}
        # {% if 'gradient' in block +%}
        'gradient': {{ block.gradient }},
        # {% endif +%}
    },
    # {% endfor +%}
    ],
    networks=[
    # {% for net in rgb_status.combined_load.network|default([]) +%}
    {
        'devices': [
            # {% for device in net.devices +%}
            '{{ device }}',
            # {% endfor +%}
        ],
        # {% if 'minimum' in net +%}
        'minimum_rate': {{ net.minimum }},
        # {% endif +%}
        # {% if 'maximum' in net +%}
        'maximum_rate': {{ net.maximum }},
        # {% endif +%}
        # {% if 'gradient' in net +%}
        'gradient': {{ net.gradient }},
        # {% endif +%}
    },
    # {% endfor +%}
    ],

    # {% if 'cpu_gradient' in rgb_status.combined_load +%}
    cpu_gradient={{ rgb_status.combined_load.cpu_gradient }},
    # {% endif +%}
    # {% if 'block_minimum' in rgb_status.combined_load +%}
    block_minimum_rate={{ rgb_status.combined_load.block_minimum }},
    # {% endif +%}
    # {% if 'block_maximum' in rgb_status.combined_load +%}
    block_maximum_rate={{ rgb_status.combined_load.block_maximum }},
    # {% endif +%}
    # {% if 'net_minimum' in rgb_status.combined_load +%}
    net_minimum_rate={{ rgb_status.combined_load.net_minimum }},
    # {% endif +%}
    # {% if 'net_maximum' in rgb_status.combined_load +%}
    net_maximum_rate={{ rgb_status.combined_load.net_maximum }},
    # {% endif +%}
)())
# {% endif %}


class CombinedAlert(ColorMerge):
    @staticmethod
    def _threshold_output(threshold: float, output: typing.Callable[[float], typing.Awaitable[None]]) -> typing.Callable[[float], typing.Awaitable[None]]:
        threshold = float(threshold)
        async def result(value: float) -> None:
            if value < threshold:
                return await output(0.0)
            return await output(1.0)
        return result

    @staticmethod
    def _threshold_gradient(color: typing.Union[str, typing.List[float]]) -> matplotlib.colors.LinearSegmentedColormap:
        return level_gradient((1.0, color))

    def __init__(self, output,
                 sensors: typing.List[typing.Dict[str, typing.Any]],
                 units_failed: typing.Optional[typing.Union[str, typing.List[float]]] = None,
                 memory_low: typing.Optional[typing.Union[str, typing.List[float]]] = None,
                 **kwargs):
        super().__init__(output)

        if units_failed:
            loop.create_task(UnitsFailed(
                output=self._threshold_output(1, self.input(self._threshold_gradient(units_failed)))
            )())

        if memory_low:
            loop.create_task(MemoryUtilization(
                output=self._threshold_output(0.9, self.input(self._threshold_gradient(memory_low)))
            )())

        for sensor_info in sensors:
            threshold = sensor_info.get('threshold', 90.0) * 1000.0
            sensor = sensor_info.get('sensor', 'temp1_input')
            color = sensor_info.get('color', [1, 0, 0])

            hwmon = sensor_info.get('path')
            if hwmon:
                if '*' in hwmon:
                    hwmon = glob.glob(hwmon)
                elif not isinstance(hwmon, list):
                    hwmon = [hwmon]
            else:
                hwmon = find_hwmon_driver(sensor_info['driver'])

            for add in hwmon:
                loop.create_task(ReadFile(
                    Path(add) / sensor,
                    output=self._threshold_output(threshold, self.input(self._threshold_gradient(color)))
                )())


# {% if 'combined_alert' in rgb_status +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(CombinedAlert(
    {{ rgb_status.combined_alert.output }},

    sensors=[
    # {% for sensor in rgb_status.combined_alert.sensors|default([]) +%}
    {
        # {% if 'path' in sensor +%}
        'path': '{{ sensor.path }}',
        # {% endif +%}
        # {% if 'driver' in sensor +%}
        'driver': '{{ sensor.driver }}',
        # {% endif +%}
        # {% if 'threshold' in sensor +%}
        'threshold': {{ sensor.threshold }},
        # {% endif +%}
        # {% if 'sensor' in sensor +%}
        'sensor': {{ sensor.sensor }},
        # {% endif +%}
        # {% if 'color' in sensor +%}
        'color': {{ sensor.color }},
        # {% endif +%}
    },
    # {% endfor +%}
    ],

    # {% if 'units_failed' in rgb_status.combined_alert +%}
    units_failed={{ rgb_status.combined_alert.units_failed }},
    # {% endif +%}

    # {% if 'memory_low' in rgb_status.combined_alert +%}
    memory_low={{ rgb_status.combined_alert.memory_low }},
    # {% endif +%}
)())
# {% endif %}


def connected_users_output(output: typing.Any, **kwargs) -> typing.Callable[[int, int], typing.Awaitable[None]]:
    rgb_local = kwargs.get('local', [1, 1, 0])
    rgb_remote = kwargs.get('remote', [0, 0, 1])

    async def result(local: int, remote: int) -> None:
        rgb = [0.0, 0.0, 0.0]
        if local > 0:
            rgb = rgb_local
        if remote > 0:
            rgb = rgb_remote
        # noinspection PyTypeChecker
        await motherboard(output, tuple(rgb))

    return result


# {% if 'connected_users' in rgb_status +%}
# noinspection PyUnresolvedReferences,PyTypeChecker
loop.create_task(ConnectedUsers(
    output=connected_users_output(
        {{ rgb_status.connected_users.output }},
        # {% if 'local' in rgb_status.connected_users +%}
        local={{ rgb_status.connected_users.local }},
        # {% endif +%}
        # {% if 'remote' in rgb_status.connected_users +%}
        remote={{ rgb_status.connected_users.remote }},
        # {% endif +%}
    ),
)())
# {% endif +%}


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")

main_task = loop.create_task(main())
loop.add_signal_handler(signal.SIGINT, main_task.cancel)
loop.add_signal_handler(signal.SIGTERM, main_task.cancel)
try:
    loop.run_until_complete(main_task)
except asyncio.CancelledError:
    pass
finally:
    systemd.daemon.notify("STOPPING=1")
    exit(0)
