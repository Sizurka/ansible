#!/usr/bin/python3

import sys
import asyncio
import aiohttp
import systemd.daemon
import signal
import json
import traceback
import random
import evdev
import typing
import platform


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


command_queue = asyncio.Queue()

desk_activity_detected = asyncio.Event()
source_name = platform.node()


async def homeassistant_connection():
    def unpack_message(msg) -> typing.Dict[str, typing.Any]:
        if msg.type == aiohttp.WSMsgType.TEXT:
            return json.loads(msg.data)
        elif msg.type == aiohttp.WSMsgType.BINARY:
            return json.loads(msg.data.decode('utf-8'))
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise RuntimeError

    api_url = "https://ha.{{ domain_name }}/api"

    while True:
        try:
            timeout = aiohttp.ClientTimeout(connect=30, sock_read=60)
            async with aiohttp.ClientSession(timeout=timeout) as session:
                async with session.ws_connect(api_url + "/websocket", heartbeat=10.0) as websocket:
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_required':
                        raise RuntimeError(str(data))
                    await websocket.send_json({
                        'type': 'auth',
                        'access_token': api_key,
                    })
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_ok':
                        raise RuntimeError(str(data))

                    command_id: int = 0
                    async def subscribe_event(event_type: str) -> None:
                        nonlocal command_id
                        command_id += 1
                        await websocket.send_json({
                            'id': command_id,
                            'type': 'subscribe_events',
                            'event_type': event_type,
                        })
                        while True:
                            data = unpack_message(await websocket.receive())
                            if data['type'] != "result":
                                continue
                            if data['id'] != command_id:
                                continue
                            if data['success'] != True:
                                raise RuntimeError(data)
                            return

                    await subscribe_event("desk_computer_activity")

                    async def process_messages():
                        async for msg in websocket:
                            data = unpack_message(msg)
                            if data['type'] == 'event':
                                event_type = data['event']['event_type']
                                event_data = data['event']['data']
                                if event_type == "desk_computer_activity":
                                    if event_data.get('source_name') == source_name:
                                        continue
                                    desk_activity_detected.set()

                    async def send_commands():
                        nonlocal command_id
                        while True:
                            data = await command_queue.get()
                            command_id += 1
                            data['id'] = command_id
                            await websocket.send_json(data)

                    done, pending = await asyncio.wait([
                        asyncio.ensure_future(process_messages()),
                        asyncio.ensure_future(send_commands()),
                    ], return_when=asyncio.FIRST_COMPLETED)
                    for task in done:
                        await task
                    for task in pending:
                        try:
                            task.cancel()
                        except:
                            pass
                        try:
                            await task
                        except:
                            pass
        except:
            traceback.print_exc()
        await asyncio.sleep(60.0)

homeassistant_connection_t = loop.create_task(homeassistant_connection())


async def send_local_input():
    def should_wait_for_device(caps):
        if evdev.ecodes.EV_KEY in caps:
            if caps[evdev.ecodes.EV_KEY] == [evdev.ecodes.KEY_F24]:
                return False
            return True
        if evdev.ecodes.EV_REL in caps:
            return True
        if evdev.ecodes.EV_ABS in caps:
            return True
        return False

    def is_wake_event(event):
        if event.type == evdev.ecodes.EV_KEY:
            if event.value == evdev.ecodes.KEY_F24:
                return False
            if event.value != 0:
                return True
        elif event.type == evdev.ecodes.EV_REL:
            if event.value != 0:
                return True
        elif event.type == evdev.ecodes.EV_ABS:
            return True
        return False

    async def wait_for_device(device):
        loop = asyncio.get_event_loop()

        device_done = asyncio.Event()
        event_detected = False

        def ready():
            nonlocal event_detected
            try:
                for event in device.read():
                    if not is_wake_event(event):
                        continue
                    event_detected = True
                    break
            except IOError:
                pass
            device_done.set()

        loop.add_reader(device, ready)
        try:
            await device_done.wait()
        finally:
            loop.remove_reader(device)
            try:
                device.close()
            except:
                pass
        return event_detected

    async def send_activity_detected():
        await command_queue.put({
            "type": "fire_event",
            "event_type": "desk_computer_activity",
            "event_data": {
                "source_name": source_name,
            },
        })

    while True:
        device_waiters = list()
        for path in evdev.list_devices():
            try:
                device = evdev.InputDevice(path)
                if not should_wait_for_device(device.capabilities()):
                    continue
                device_waiters.append(asyncio.ensure_future(wait_for_device(device)))
            except:
                pass
        if not device_waiters:
            await asyncio.sleep(60)
            continue

        done, pending = await asyncio.wait(device_waiters, return_when=asyncio.FIRST_COMPLETED, timeout=60.0)
        for task in pending:
            try:
                task.cancel()
            except:
                continue
            try:
                await task
            except asyncio.CancelledError:
                pass
            except:
                traceback.print_exc()
                pass
        any_detected = False
        for task in done:
            if task.result():
                any_detected = True
        if not any_detected:
            continue

        await send_activity_detected()
        await asyncio.sleep(60)

send_local_input_t = loop.create_task(send_local_input())


# It'd be far too easy to just expose a simulate input event (it even exists, as
# org.gnome.Mutter.IdleMonitor.ResetIdletime or org.freedesktop.ScreenSaver.SimulateUserActivity, it's just
# disabled), and inhibits (org.freedesktop.ScreenSaver.[Inhibit/UnInhibit]) don't reliably reset the sleep timer.
# So we have to do it with an evdev generating a "real" input.
async def receive_remote_activity():
    reset_output = evdev.UInput({
        evdev.ecodes.EV_KEY: [evdev.ecodes.KEY_F24]
    }, name="sleep-reset")

    while True:
        await asyncio.sleep(60)
        await desk_activity_detected.wait()
        desk_activity_detected.clear()

        reset_output.write(evdev.ecodes.EV_KEY, evdev.ecodes.KEY_F24, 1)
        reset_output.syn()
        await asyncio.sleep(0.1)
        reset_output.write(evdev.ecodes.EV_KEY, evdev.ecodes.KEY_F24, 0)
        reset_output.syn()

receive_remote_activity_t = loop.create_task(receive_remote_activity())


def exit_handler(sig, frame):
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


main_t = loop.create_task(main())
loop.run_forever()
