#!/usr/bin/python3

import urllib.request
import re
import tempfile
import subprocess
import os
import datetime
import ipaddress

output_file_name = "/etc/nftables.d/90-blacklist.nft"

whitelist = {
    ipaddress.IPv4Network('127.0.0.0/8'),
    ipaddress.IPv4Network('0.0.0.0/32'),
    ipaddress.IPv6Network('::1/128')
}


class RangeTracker:
    def __init__(self, nettype, minprefix):
        self._nettype = nettype
        self._minprefix = minprefix
        self._blocks = []

    def add(self, ip):
        try:
            ip = self._nettype(ip, strict=False)
        except ValueError:
            return
        if ip.prefixlen <= self._minprefix:
            return

        for check in whitelist:
            if type(check) != type(ip):
                continue
            if check.overlaps(ip):
                return

        self._blocks.append(ip)

    def unwind(self):
        begin = None
        end = None
        result = list()
        for block in sorted(self._blocks):
            if begin is None:
                begin = block.network_address
                end = block.network_address + (block.num_addresses - 1)
                continue
            if block.network_address <= end:
                compare = block.network_address + (block.num_addresses - 1)
                if compare > end:
                    end = compare
                continue
            result.append((begin, end))
            begin = block.network_address
            end = block.network_address + (block.num_addresses - 1)
        return result


block_ipv4 = RangeTracker(ipaddress.IPv4Network, 8)
block_ipv6 = RangeTracker(ipaddress.IPv6Network, 24)


def apply_whois_ipv4(id):
    output = subprocess.check_output(["/usr/bin/whois", "-h", "whois.radb.net", id])
    for ip in re.findall(r'(?:\d+\.\d+\.\d+\.\d+/\d+)', output.decode('ascii')):
        block_ipv4.add(ip)


def apply_whois_ipv6(id):
    output = subprocess.check_output(["/usr/bin/whois", "-h", "whois.radb.net", id])
    for ip in re.findall(r'(?:[0-9a-fA-f][0-9a-fA-f:]{8,}::/\d+)', output.decode('ascii')):
        block_ipv6.add(ip)


print("Querying Autonomous System Numbers")
# Facebook
apply_whois_ipv4("!gAS32934")
apply_whois_ipv6("!6AS32934")


class BlockList:
    def __init__(self, url):
        try:
            with urllib.request.urlopen(url) as response:
                self._lines = response.read().decode('utf-8').splitlines()
        except Exception as ex:
            self._lines = list()
            print("Error downloading %s" % url)
            print(ex)

    @staticmethod
    def _field_or_empty(fields, index):
        if index >= len(fields):
            return ''
        return fields[index]

    def remove_comments(self, identifier='#'):
        self._lines = [self._field_or_empty(l.split(identifier), 0) for l in self._lines]
        return self

    def extract_field(self, index=1, splitter=r'\s+'):
        self._lines = [self._field_or_empty(re.split(splitter, l), index) for l in self._lines]
        return self

    def apply_ipv4(self):
        for l in self._lines:
            l = l.strip()
            if len(l) <= 0:
                continue
            match = re.match(r'(\d+\.\d+\.\d+\.\d+(?:/\d+)?)', l)
            if not match:
                continue
            block_ipv4.add(match.group(1))

    def apply_ipv6(self):
        for l in self._lines:
            l = l.strip()
            if len(l) <= 0:
                continue
            match = re.match(r'([0-9a-fA-f][0-9a-fA-f:]{8,}::(?:/\d+)?)', l)
            if not match:
                continue
            block_ipv6.add(match.group(1))


# http://iplists.firehol.org/

print("Downloading blocklists")
BlockList(
    'https://raw.githubusercontent.com/firehol/blocklist-ipsets/master/firehol_level1.netset'). \
    remove_comments('#'). \
    apply_ipv4()
# Part of Firehol Level 1
# BlockList(
#    'https://www.spamhaus.org/drop/drop.txt'). \
#    remove_comments(';'). \
#    apply_ipv4()
# BlockList(
#    'https://www.spamhaus.org/drop/edrop.txt'). \
#    remove_comments(';'). \
#    apply_ipv4()
BlockList('https://www.spamhaus.org/drop/dropv6.txt'). \
    remove_comments(';'). \
    apply_ipv6()

# Just us the DNS variant, since these might be on multihosting, etc
# BlockList(
#   'https://raw.githubusercontent.com/ZeroDot1/CoinBlockerLists/master/MiningServerIPList.txt'). \
#   remove_comments(). \
#   apply_ipv4()


output = tempfile.NamedTemporaryFile(mode='w+', delete=False)


def output_set(input, prefix):
    if len(input) <= 0:
        return
    global output
    output.write("\n")
    output.write(prefix)
    output.write(" {\n")
    first = True
    for net in input:
        if not first:
            output.write(",\n")
        first = False
        if net[0] != net[1]:
            output.write("\t%s-%s" % (str(net[0]), str(net[1])))
        else:
            output.write("\t%s" % (str(net[0])))
    output.write("\n}\n")


output.write("#!/usr/sbin/nft -f\n")
output.write("\n")
output.write("# Generated at %sZ\n" % (datetime.datetime.utcnow().isoformat()))
output.write("\n")
output.write("\n")
output.write("flush set ip filter ip-blacklist\n")
output.write("flush set ip6 filter ip-blacklist\n")
output_set(block_ipv4.unwind(), "add element ip filter ip-blacklist")
output_set(block_ipv6.unwind(), "add element ip6 filter ip-blacklist")
output.write("\n")
output.write("\n")
output.close()

print("Reloading blacklist")
# Doesn't look like NFT always considers the previous flush correctly
subprocess.call(["/usr/sbin/nft", "flush", "set", "ip", "filter", "ip-blacklist"])
subprocess.call(["/usr/sbin/nft", "flush", "set", "ip6", "filter", "ip-blacklist"])
try:
    subprocess.check_call(["/usr/sbin/nft", "-f", output.name])
except:
    print("Error reloading IP blacklist")
    os.unlink(output.name)
    subprocess.call(["/usr/sbin/nft", "flush", "set", "ip", "filter", "ip-blacklist"])
    subprocess.call(["/usr/sbin/nft", "flush", "set", "ip6", "filter", "ip-blacklist"])
    subprocess.call(["/usr/sbin/nft", "-f", output_file_name])
    exit(1)

subprocess.call(["cp", output.name, output_file_name])
os.unlink(output.name)
