#!/bin/sh

version="$1"
bootdir=/boot

[ -x /usr/bin/dracut ] || exit 0

# passing the kernel version is required
[ -z "${version}" ] && exit 0

# Just rely on the fact that RPi kernels specify no initrd required, so normal dracut does not run

# Only generate for a single kernel arch
{% if ansible_facts.architecture == "aarch64" %}
echo "${version}" | grep -E -q -- '-v8\+?$' || exit 0
{% elif ansible_facts.architecture == "armv7l" %}
echo "${version}" | grep -E -q -- '-v7l$' || exit 0
{% elif ansible_facts.architecture == "armv7" %}
echo "${version}" | grep -E -q -- '-v7$' || exit 0
{% else %}
echo "${version}" | grep -E -q '\.[[:digit:]]+\+?$' || exit 0
{% endif %}

# absolute file name of kernel image may be passed as a second argument;
# create the initrd in the same directory
if [ -n "$2" ]; then
	bootdir=$(dirname "$2")
fi

# avoid running multiple times
if [ -n "$DEB_MAINT_PARAMS" ]; then
	eval set -- "$DEB_MAINT_PARAMS"
	if [ -z "$1" ] || [ "$1" != "configure" ]; then
		exit 0
	fi
fi

# check if modules.dep already exists. If not create it
# maybe this problem could also be solved via Debian triggers
if [ ! -f $bootdir/../lib/modules/$version/modules.dep ]; then
    depmod -a -F $bootdir/System.map-$version $version
fi

# we're good - create initramfs
echo "dracut: Generating $bootdir/initrd.img-${version}"
dracut -q --force $bootdir/initrd.img-${version} "${version}" >&2

# Update boot config for the latest initrd
LATEST=$(find /boot -mindepth 1 -maxdepth 1 -type f -name "initrd.img-*" -printf '%f\n' | sort --version-sort | tail -n 1)
echo "rpiboot: set initrd to ${LATEST}"
sed -i -r -e "s/^initramfs[[:space:]].*$/initramfs ${LATEST} followkernel/" /boot/config.txt