#!/usr/bin/python3

import evdev
import pyudev
import signal
import asyncio
import sys
import os
import systemd.daemon
import xml.etree.ElementTree as ET

if len(sys.argv) < 2:
    print("Invalid command line")
    exit(1)

domain = sys.argv[1]

root = ET.parse("/run/libvirt/hook/" + domain)

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
_background_tasks = set()
def background_task(coro):
    r = asyncio.get_event_loop().create_task(coro)
    _background_tasks.add(r)
    r.add_done_callback(lambda task: _background_tasks.discard(r))
    return r


output = evdev.UInput({
    evdev.ecodes.EV_KEY: evdev.ecodes.keys.keys(),
    evdev.ecodes.EV_REL: [evdev.ecodes.REL_X, evdev.ecodes.REL_Y,
                          evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL],
}, name="uinput-aggregate")

enable_aggregation = root.find("devices/graphics/[@type]") is None



def zero_keys_list():
    result = list()
    for code in evdev.ecodes.keys.keys():
        while code >= len(result):
            result.append(0)
    return result
key_down_reference_counter = zero_keys_list()


def key_down(code):
    key_down_reference_counter[code] += 1
    if key_down_reference_counter[code] != 1:
        return
    output.write(evdev.ecodes.EV_KEY, code, 1)
    output.syn()

def key_up(code):
    key_down_reference_counter[code] -= 1
    if key_down_reference_counter[code] != 0:
        return
    output.write(evdev.ecodes.EV_KEY, code, 0)
    output.syn()


active_devices = set()
async def forward_device(device):
    #device.grab()
    local_keys_down = zero_keys_list()
    try:
        async for event in device.async_read_loop():
            if event.type == evdev.ecodes.EV_REL:
                if event.code in (evdev.ecodes.REL_X, evdev.ecodes.REL_Y,
                                  evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL):
                    output.write(event.type, event.code, event.value)
                    output.syn()
            elif event.type == evdev.ecodes.EV_KEY:
                if event.value == 1:
                    if local_keys_down[event.code] == 0:
                        local_keys_down[event.code] = 1
                        key_down(event.code)
                elif event.value == 0:
                    if local_keys_down[event.code] != 0:
                        local_keys_down[event.code] = 0
                        key_up(event.code)
        active_devices.remove(device.path)
    except OSError:
        pass
    finally:
        print("Releasing device {}".format(device.path), flush=True)
        for code in range(0, len(local_keys_down)):
            if local_keys_down[code] != 0:
                key_up(code)
        try:
            device.close()
        except:
            pass


def is_valid_device(device):
    if device.info.bustype == 0x19:
        # BUS_HOST (power button, etc)
        return False
    if device.name == "uinput-aggregate":
        return False
    caps = device.capabilities()
    if evdev.ecodes.EV_KEY in caps:
        return True
    if evdev.ecodes.EV_REL in caps:
        relcaps = caps[evdev.ecodes.EV_REL]
        for check in (evdev.ecodes.REL_X, evdev.ecodes.REL_Y, evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL):
            if check in relcaps:
                return True
    return True


have_seen_aggregate_device = False
def check_aggregate_device(device):
    global have_seen_aggregate_device
    if have_seen_aggregate_device:
        return
    if device.name != "uinput-aggregate":
        return
    have_seen_aggregate_device = True
    print("Aggregate device ready at {}".format(device.path), flush=True)
    systemd.daemon.notify("READY=1")


async def evdev_rescan():
    for path in evdev.list_devices():
        if path in active_devices:
            continue
        try:
            device = evdev.InputDevice(path)
        except OSError:
            continue

        check_aggregate_device(device)
        if not enable_aggregation:
            device.close()
            continue
        if not is_valid_device(device):
            device.close()
            continue
        active_devices.add(path)
        print("Attaching to device {}".format(path), flush=True)
        background_task(forward_device(device))


udev_context = pyudev.Context()
udev = pyudev.Monitor.from_netlink(udev_context)
udev.filter_by('input')
def udev_ready():
    changed = False
    while True:
        device = udev.poll(0)
        if device is None:
            break
        if device.action != 'add' and device.action != 'change':
            continue
        changed = True
    if not changed:
        return
    background_task(evdev_rescan())
udev.start()
loop.add_reader(udev.fileno(), udev_ready)


async def main():
    await asyncio.sleep(0.25)
    background_task(evdev_rescan())
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")

main_task = loop.create_task(main())
loop.add_signal_handler(signal.SIGINT, main_task.cancel)
loop.add_signal_handler(signal.SIGTERM, main_task.cancel)
loop.add_signal_handler(signal.SIGHUP, main_task.cancel)
try:
    loop.run_until_complete(main_task)
except asyncio.CancelledError:
    pass

systemd.daemon.notify("STOPPING=1")
print("Shutting down")
output.close()

# Weirdness about releasing the device gracefully, so just do it the hard way
os._exit(os.EX_OK)
