#!/bin/bash

check() {
  return 0
}

depends() {
  return 0
}

installkernel() {
  instmods tpm_crb tpm_tis
}

install() {
  inst_simple /etc/systemd/system/extract-boot-key.service $systemdsystemunitdir/extract-boot-key.service
  inst_multiple -o $systemdsystemunitdir/cryptsetup-pre.target
  inst_script /etc/kernel/extract-tpm-key.sh /bin/extract-tpm-key.sh
  inst_rules 75-tpm-systemd.rules

  inst tpm2_unseal
  inst_libdir_file "libtss2-tcti-device.so*"

  # Workaround for a dracut bug a line with "force" to include all subsequent lines (#807), fixed in dracut 51
  sed -r -i -n -e 's/^[^[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+([^[:space:]]+,)?force(,[^[:space:]]+)?/\0/p' "${initdir}/etc/crypttab"

  mkdir -p "${initdir}${systemdsystemunitdir}/sysinit.target.wants"
  ln_r "${systemdsystemunitdir}/extract-boot-key.service" \
    "${systemdsystemunitdir}/sysinit.target.wants/extract-boot-key.service"
  mkdir -p "${initdir}${systemdsystemunitdir}/cryptsetup.target.wants"
  ln_r "${systemdsystemunitdir}/cryptsetup-pre.target" \
    "${systemdsystemunitdir}/cryptsetup.target.wants/cryptsetup-pre.target"
}