import asyncio

LABELS = [
    'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
    'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant',
    'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard',
    'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket', 'bottle',
    'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli',
    'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'dining table', 'toilet',
    'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator',
    'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'
]

_MODEL = None

def _get_model():
    from ultralytics import YOLO

    global _MODEL
    if _MODEL is None:
        _MODEL = YOLO("/opt/models/yolov8s.pt")

    return _MODEL

def detect_objects(image, size_limit: int = 512):
    kwargs = {}
    if size_limit > 32:
        kwargs['imgsz'] = size_limit
    output = _get_model().predict(image, verbose=False, **kwargs)[0]
    class_names = output.names
    boxes = output.boxes

    result = []
    for idx in range(len(boxes.cls)):
        class_id = int(boxes.cls[idx])
        x_min, y_min, x_max, y_max = boxes.xyxyn[idx]

        result.append({
            'left': float(x_min),
            'top': float(y_min),
            'right': float(x_max),
            'bottom': float(y_max),
            'label': class_names.get(int(class_id), ''),
            'confidence': float(boxes.conf[idx]) * 100.0,
        })

    return result


_LOCK = None

async def async_labels():
    global _LOCK
    if _LOCK is None:
        _LOCK = asyncio.Lock()

    async with _LOCK:
        return await asyncio.get_event_loop().run_in_executor(
            None, labels
        )

async def async_detect_objects(image, size_limit):
    global _LOCK
    if _LOCK is None:
        _LOCK = asyncio.Lock()

    async with _LOCK:
        return await asyncio.get_event_loop().run_in_executor(
            None, detect_objects, image, size_limit
        )
