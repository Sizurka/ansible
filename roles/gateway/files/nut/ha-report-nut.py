#!/usr/bin/python3

import sys
import asyncio
import aiohttp
import signal
import traceback
import systemd.daemon
import platform
import json
import re


ups_name = "local"
if len(sys.argv) > 1:
    ups_name = sys.argv[1]
nut_host = "localhost"
if len(sys.argv) > 2:
    nut_host = sys.argv[2]
nut_port = "nut"
if len(sys.argv) > 3:
    nut_port = sys.argv[3]


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()

root_id = platform.node().split('.', 1)[0]

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
_background_tasks = set()
def background_task(coro):
    r = asyncio.get_event_loop().create_task(coro)
    _background_tasks.add(r)
    r.add_done_callback(lambda task: _background_tasks.discard(r))
    return r
http_session = None


async def update_state(parameter, state, attributes=None):
    global http_session
    if http_session is None:
        http_session = aiohttp.ClientSession()

    if type(state) is float:
        state = "{:.2f}".format(state)
    else:
        state = str(state)

    entity_id = root_id + "." + parameter
    content = {
        "state": state,
    }
    if attributes is not None:
        content["attributes"] = attributes
    try:
        await http_session.post("https://ha.{{ domain_name }}/api/states/" + entity_id,
                                data=json.dumps(content),
                                headers={
                                    'Authorization': 'Bearer ' + api_key,
                                    'Content-Type': 'application/json',
                                })
    except:
        traceback.print_exc()


async def run_ups():
    var_response = re.compile(r'VAR\s+{}\s+(\S+)\s+"([^"]+)"'.format(ups_name))
    while True:
        try:
            reader, writer = await asyncio.open_connection(nut_host, nut_port)
        except:
            traceback.print_exc()
            await asyncio.sleep(10)
            continue

        async def query_ups():
            while True:
                writer.write("GET VAR {} battery.charge\n".format(ups_name).encode())
                await writer.drain()
                await asyncio.sleep(1)
                writer.write("GET VAR {} battery.runtime\n".format(ups_name).encode())
                await writer.drain()
                await asyncio.sleep(1)
                writer.write("GET VAR {} ups.load\n".format(ups_name).encode())
                await writer.drain()
                await asyncio.sleep(1)
                writer.write("GET VAR {} ups.temperature\n".format(ups_name).encode())
                await writer.drain()
                await asyncio.sleep(1)
                writer.write("GET VAR {} ups.status\n".format(ups_name).encode())
                await writer.drain()
                await asyncio.sleep(56)

        periodic = loop.create_task(query_ups())

        while True:
            line = await reader.readline()
            if line is None or len(line) == 0:
                break
            line = line.decode('utf-8', 'ignore').strip()
            match = var_response.fullmatch(line)
            if match is None:
                continue
            var = match[1]
            value = match[2]

            if var == "battery.charge":
                background_task(update_state("ups_charge", value, {
                    "friendly_name": "Battery Charge",
                    "icon": "mdi:gauge",
                    "unit_of_measurement": "%",
                }))
            elif var == "battery.runtime":
                background_task(update_state("ups_runtime", value, {
                    "friendly_name": "Battery Runtime",
                    "icon": "mdi:timer",
                    "unit_of_measurement": "s",
                }))
            elif var == "ups.load":
                background_task(update_state("ups_load", value,  {
                    "friendly_name": "UPS Load",
                    "icon": "mdi:gauge",
                    "unit_of_measurement": "%",
                }))
            elif var == "ups.temperature":
                background_task(update_state("ups_temperature", value,  {
                    "friendly_name": "UPS Internal Temperature",
                    "icon": "mdi:thermometer-lines",
                    "unit_of_measurement": "°C",
                }))
            elif var == "ups.status":
                keys = set()
                for st in value.split():
                    keys.add(st.upper())

                on_battery = "off"
                if "OB" in keys:
                    on_battery = "on"
                background_task(update_state("ups_on_battery", on_battery, {
                    "friendly_name": "UPS Using Battery Power",
                    "icon": "mdi:battery",
                }))

                if "ALARM" in keys:
                    background_task(update_state("ups_alarm", "alarm", {
                        "friendly_name": "UPS Alarm",
                        "description": "Alarm reported",
                        "icon": "mdi:battery-alert",
                    }))
                elif "OVER" in keys:
                    background_task(update_state("ups_alarm", "overload", {
                        "friendly_name": "UPS Alarm",
                        "description": "Overload",
                        "icon": "mdi:battery-alert",
                    }))
                elif "RB" in keys:
                    background_task(update_state("ups_alarm", "replace", {
                        "friendly_name": "UPS Alarm",
                        "description": "Replace battery",
                        "icon": "mdi:battery-alert",
                    }))
                else:
                    background_task(update_state("ups_alarm", "off", {
                        "friendly_name": "UPS Alarm",
                        "description": "No alarm reported",
                        "icon": "mdi:battery-alert",
                    }))

                if "CHRG" in keys:
                    background_task(update_state("ups_charging", "charge", {
                        "friendly_name": "UPS Charging State",
                        "description": "Charging",
                        "icon": "mdi:battery-charging",
                    }))
                elif "DISCHRG" in keys:
                    background_task(update_state("ups_charging", "discharge", {
                        "friendly_name": "UPS Charging State",
                        "description": "Discharging",
                        "icon": "mdi:battery-minus",
                    }))
                else:
                    background_task(update_state("ups_charging", "full", {
                        "friendly_name": "UPS Charging State",
                        "description": "Full",
                        "icon": "mdi:battery",
                    }))

                if "TRIM" in keys:
                    background_task(update_state("ups_line", "trim", {
                        "friendly_name": "UPS Line Status",
                        "description": "Trimming",
                        "icon": "mdi:power-plug",
                    }))
                elif "BOOST" in keys:
                    background_task(update_state("ups_line", "boost", {
                        "friendly_name": "UPS Line Status",
                        "description": "Boosting",
                        "icon": "mdi:power-plug",
                    }))
                else:
                    background_task(update_state("ups_line", "normal", {
                        "friendly_name": "UPS Line Status",
                        "description": "Normal",
                        "icon": "mdi:power-plug",
                    }))

        periodic.cancel()
run_ups_t = loop.create_task(run_ups())


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGHUP):
    loop.add_signal_handler(sig, lambda: loop.stop())


main_t = loop.create_task(main())
loop.run_forever()
