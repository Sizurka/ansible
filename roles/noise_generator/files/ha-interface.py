#!/usr/bin/python3

import sys
import asyncio
import aiohttp
import systemd.daemon
import signal
import json
import traceback
import typing
import numpy as np
from enum import Enum
from tempfile import TemporaryFile
from math import ceil


with open('/etc/ha_api.key', 'r') as f:
    api_key = f.read()
api_key = api_key.strip()


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

GLOBAL_VOLUME = 1.0
CROSSFADE_TIME = 30.0
SAMPLE_RATE = 48000

class NoiseGenerator(Enum):
    OFF = 1
    RAIN = 2
    BIRDSONG = 3
noise_generator_state: NoiseGenerator = NoiseGenerator.OFF

class NoiseGeneratorSample:
    def __init__(self, source: typing.Optional[str] = None):
        self.source_file = source
        if not source:
            self.samples: np.ndarray = np.full((1,), 0.0, dtype=np.float32)
        else:
            self.samples: np.ndarray = None
        self.index: int = 0
        self.crossfade_fraction: float = 0.0

    async def load(self) -> None:
        if not self.source_file:
            return

        self._decompressed = TemporaryFile()

        proc = await asyncio.create_subprocess_exec(
            "ffmpeg",
            "-i", self.source_file,
            "-f", "f32le", "-acodec", "pcm_f32le",
            "-ac", "1", "-r", str(SAMPLE_RATE),
            "-",
            stdin=asyncio.subprocess.DEVNULL,
            stdout=self._decompressed,
        )
        await proc.communicate()

        self._decompressed.seek(0)
        self.samples = np.memmap(self._decompressed, dtype=np.float32, mode='r')

    def fill_buffer(self, dest: np.ndarray) -> None:
        dest_offset = 0
        if self.index != 0:
            max_copy = min(dest.shape[0] - dest_offset, self.samples.shape[0] - self.index)
            dest[dest_offset:dest_offset+max_copy] = self.samples[self.index:self.index+max_copy]
            self.index = (self.index + max_copy) % self.samples.shape[0]
            dest_offset += max_copy

        remaining_samples = dest.shape[0] - dest_offset
        if remaining_samples == 0:
            return
        required_tiles = int(ceil(remaining_samples / self.samples.shape[0]))
        source_tiled = np.tile(self.samples, required_tiles)
        dest[dest_offset:] = source_tiled[:remaining_samples]
        self.index = (self.index + remaining_samples) % self.samples.shape[0]
noise_generator_input: typing.Dict[NoiseGenerator, NoiseGeneratorSample] = {
    NoiseGenerator.OFF: NoiseGeneratorSample(),
    NoiseGenerator.RAIN: NoiseGeneratorSample("/opt/rain.flac"),
    NoiseGenerator.BIRDSONG: NoiseGeneratorSample("/opt/birdsong.flac"),
}

withings_bed_sensor_unstable: typing.Optional[bool] = None
withings_bed_sensor_awake: typing.Optional[bool] = None
withings_bed_sensor_not_waiting: typing.Optional[bool] = None
withings_bed_sensor_unstable_changed = asyncio.Event()


async def homeassistant_connection():
    def unpack_message(msg) -> typing.Dict[str, typing.Any]:
        if msg.type == aiohttp.WSMsgType.TEXT:
            return json.loads(msg.data)
        elif msg.type == aiohttp.WSMsgType.BINARY:
            return json.loads(msg.data.decode('utf-8'))
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise RuntimeError

    api_url = "https://ha.{{ domain_name }}/api"

    while True:
        try:
            timeout = aiohttp.ClientTimeout(connect=30, sock_read=60)
            async with aiohttp.ClientSession(timeout=timeout) as session:
                async with session.ws_connect(api_url + "/websocket", heartbeat=10.0) as websocket:
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_required':
                        raise RuntimeError(str(data))
                    await websocket.send_json({
                        'type': 'auth',
                        'access_token': api_key,
                    })
                    data = unpack_message(await websocket.receive())
                    if data['type'] != 'auth_ok':
                        raise RuntimeError(str(data))

                    command_id: int = 0
                    async def subscribe_trigger(trigger: typing.Dict[str,typing.Any]) -> None:
                        nonlocal command_id
                        command_id += 1
                        await websocket.send_json({
                            'id': command_id,
                            'type': 'subscribe_trigger',
                            'trigger': trigger,
                        })
                        while True:
                            data = unpack_message(await websocket.receive())
                            if data['type'] != "result":
                                continue
                            if data['id'] != command_id:
                                continue
                            if data['success'] != True:
                                raise RuntimeError(data)
                            return

                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'binary_sensor.withings_in_bed_unstable',
                        'from': None,
                    })
                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_select.sleep_noise_generator',
                        'from': None,
                    })
                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_select.presence',
                        'from': None,
                    })
                    await subscribe_trigger({
                        'platform': 'state',
                        'entity_id': 'input_select.bedroom_button_action',
                        'from': None,
                    })

                    def dispatch_state(entity_id: str, state: str, attributes: typing.Dict[str, typing.Any]) -> None:
                        if entity_id == 'binary_sensor.withings_in_bed_unstable':
                            global withings_bed_sensor_unstable
                            if state == 'on':
                                withings_bed_sensor_unstable = True
                            elif state == 'off':
                                withings_bed_sensor_unstable = False
                            withings_bed_sensor_unstable_changed.set()
                        elif entity_id == 'input_select.presence':
                            global withings_bed_sensor_awake
                            if state == 'Sleep':
                                withings_bed_sensor_awake = False
                            else:
                                withings_bed_sensor_awake = True
                            withings_bed_sensor_unstable_changed.set()
                        elif entity_id == 'input_select.bedroom_button_action':
                            global withings_bed_sensor_not_waiting
                            if state == 'None':
                                withings_bed_sensor_not_waiting = True
                            else:
                                withings_bed_sensor_not_waiting = False
                            withings_bed_sensor_unstable_changed.set()
                        elif entity_id == 'input_select.sleep_noise_generator':
                            global noise_generator_state
                            if state == 'Rain':
                                noise_generator_state = NoiseGenerator.RAIN
                            elif state == 'Birdsong':
                                noise_generator_state = NoiseGenerator.BIRDSONG
                            else:
                                noise_generator_state = NoiseGenerator.OFF

                    command_id += 1
                    await websocket.send_json({
                        'id': command_id,
                        'type': 'get_states',
                    })
                    read_states_id = command_id
                    async for msg in websocket:
                        data = unpack_message(msg)
                        if data['type'] == 'event':
                            to_state = data['event']['variables']['trigger']['to_state']
                            dispatch_state(to_state['entity_id'], to_state['state'], to_state['attributes'])
                        elif data['type'] == 'result' and data['id'] == read_states_id:
                            for entity in data['result']:
                                dispatch_state(entity['entity_id'], entity['state'], entity['attributes'])
        except:
            traceback.print_exc()
        await asyncio.sleep(60.0)


homeassistant_connection_t = loop.create_task(homeassistant_connection())


def set_withings_bed_sensor_power(on: bool) -> None:
    with open("/sys/class/gpio/gpio508/value", "wb") as f:
        f.write(b'1' if on else b'0')


async def power_cycle_withings_bed_sensor():
    while True:
        await asyncio.sleep(3600.0)
        while not withings_bed_sensor_unstable or not withings_bed_sensor_awake or not withings_bed_sensor_not_waiting:
            await withings_bed_sensor_unstable_changed.wait()
            withings_bed_sensor_unstable_changed.clear()
        print("Withings bed sensor unstable, initiating power cycle")
        set_withings_bed_sensor_power(False)
        await asyncio.sleep(600.0)
        set_withings_bed_sensor_power(True)
        print("Withings bed sensor power cycle complete, waiting for stability")
        while withings_bed_sensor_unstable:
            await withings_bed_sensor_unstable_changed.wait()
            withings_bed_sensor_unstable_changed.clear()
        print("Withings bed sensor stabilized")


power_cycle_withings_bed_sensor_t = loop.create_task(power_cycle_withings_bed_sensor())


async def generate_noise():
    for s in noise_generator_input.values():
        await s.load()

    # player = await asyncio.create_subprocess_exec(
    #     "ffplay",
    #     "-nodisp", "-autoexit",
    #     "-f", "f32le", "-ac", "1", "-ar", str(SAMPLE_RATE),
    #     "-probesize", "4096",
    #     "-analyzeduration", "100000",
    #     "-sync", "ext",
    #     "-fflags", "nobuffer",
    #     "-",
    #     stdin=asyncio.subprocess.PIPE,
    #     stdout=asyncio.subprocess.DEVNULL,
    # )
    player = await asyncio.create_subprocess_exec(
        "aplay",
        "-t", "raw",
        "-c", "1", "-f", "FLOAT_LE", "-r", str(SAMPLE_RATE),
        "-B", "100000",
        "-",
        stdin=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.DEVNULL,
    )
    audio_out = player.stdin

    async def monitor_player():
        await player.wait()
        # Probably means a USB fault, to exit and let systemd restart us
        print("aplay exited, aborting", flush=True)
        exit(1)

    monitor_player_t = loop.create_task(monitor_player())

    output_samples = np.empty((8192,), dtype=np.float32)
    mix_samples = np.empty(output_samples.shape, dtype=np.float32)

    crossfade_increment = 1.0 / (SAMPLE_RATE * CROSSFADE_TIME)

    while True:
        output_samples[:] = 0.0
        for t, sample in noise_generator_input.items():
            if t == noise_generator_state:
                fade_direction = 1
            else:
                fade_direction = -1
            if fade_direction < 0 and sample.crossfade_fraction <= 0.0:
                continue

            sample.fill_buffer(mix_samples)
            if fade_direction > 0 and sample.crossfade_fraction >= 1.0:
                output_samples[:] = mix_samples[:]
                break

            final_fraction = sample.crossfade_fraction + crossfade_increment * mix_samples.shape[0] * fade_direction
            mix_fraction = np.linspace(sample.crossfade_fraction, final_fraction, mix_samples.shape[0],
                                       endpoint=False, dtype=np.float32)
            sample.crossfade_fraction = min(1.0, max(0.0, final_fraction))

            mix_fraction[mix_fraction < 0.0] = 0.0
            mix_fraction[mix_fraction > 1.0] = 1.0
            # mix_fraction *= mix_fraction
            output_samples += mix_samples * mix_fraction

        output_samples *= GLOBAL_VOLUME
        audio_out.write(output_samples.tobytes())
        await audio_out.drain()


generate_noise_t = loop.create_task(generate_noise())


def exit_handler(sig, frame):
    sys.exit(0)
signal.signal(signal.SIGINT, exit_handler)


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(10)
        systemd.daemon.notify("WATCHDOG=1")


main_t = loop.create_task(main())
loop.run_forever()
