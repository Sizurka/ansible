#!/usr/bin/python3


import dbus
import time
import subprocess
import systemd.daemon
import argparse
from gi.repository import GLib
import dbus.mainloop.glib
from paramiko.client import SSHClient


parser = argparse.ArgumentParser(description="Side display unlock and wakeup.")
parser.add_argument('--unlock',
                    dest='unlock', action='store_true',
                    help="perform immediate unlock and exit")
args = parser.parse_args()


def perform_unlock():
    print("Waking up side display", flush=True)

    for t in range(5):
        systemd.daemon.notify("WATCHDOG=1")
        subprocess.run(['wol', '{{ nue.network_mac }}'])
        time.sleep(5)

        try:
            client = SSHClient()
            client.load_system_host_keys()
            client.connect('nue.{{ domain_name }}', timeout=3)
            client.exec_command('unlock-seat-session')
            client.close()
            break
        except:
            pass


if args.unlock:
    perform_unlock()
    exit(0)


dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
loop = GLib.MainLoop()
bus = dbus.SystemBus()
manager = bus.get_object("org.freedesktop.login1", "/org/freedesktop/login1")
auto_proxy = bus.get_object("org.freedesktop.login1", "/org/freedesktop/login1/session/auto")


def find_active_session_proxy():
    seat = auto_proxy.Get("org.freedesktop.login1.Session", "Seat",
                          dbus_interface="org.freedesktop.DBus.Properties")
    seat = str(seat[0])
    if not seat:
        return None

    uid = auto_proxy.Get("org.freedesktop.login1.Session", "User",
                         dbus_interface="org.freedesktop.DBus.Properties")
    uid = int(uid[0])
    if not uid:
        return None

    for session in manager.ListSessions(dbus_interface="org.freedesktop.login1.Manager"):
        if session[1] != uid:
            continue
        if session[3] != seat:
            continue
        path = session[4]
        return bus.get_object("org.freedesktop.login1", path)
    return None


last_locked_state = None


def session_properties_changed(interface_name, changed, invalidated):
    global last_locked_state
    is_locked = changed.get("LockedHint")
    if is_locked is None:
        return

    is_locked = bool(is_locked)
    was_locked = last_locked_state
    last_locked_state = is_locked

    if was_locked is None:
        if is_locked:
            return
    else:
        if is_locked:
            return
        if not was_locked:
            return

    perform_unlock()


session_proxy = None
session_proxy_signal = None


def poll_session():
    global last_locked_state
    global session_proxy
    global session_proxy_signal

    try:
        is_locked = auto_proxy.Get("org.freedesktop.login1.Session", "LockedHint",
                                   dbus_interface="org.freedesktop.DBus.Properties")
    except dbus.DBusException:
        return True
    is_locked = bool(is_locked)
    was_locked = last_locked_state
    last_locked_state = is_locked

    if not is_locked and was_locked:
        perform_unlock()

    # The signal should hit first, so if this fallback hits it the signal has become disconnected
    if was_locked != is_locked:
        if session_proxy_signal:
            session_proxy_signal.remove()
            session_proxy_signal = None
        session_proxy = None

    if session_proxy is None:
        session_proxy = find_active_session_proxy()
    if session_proxy_signal is None and session_proxy is not None:
        session_proxy_signal = session_proxy.connect_to_signal("PropertiesChanged", session_properties_changed,
                                                               dbus_interface="org.freedesktop.DBus.Properties")
    return True


GLib.timeout_add_seconds(5, poll_session)


def watchdog_ping():
    systemd.daemon.notify("WATCHDOG=1")
    return True


GLib.timeout_add_seconds(15, watchdog_ping)

systemd.daemon.notify("READY=1")
loop.run()

