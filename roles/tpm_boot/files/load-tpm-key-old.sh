#!/bin/sh
umask 0077

# For tpm2-tools < 4.0

# https://medium.com/@pawitp/full-disk-encryption-on-arch-linux-backed-by-tpm-2-0-c0892cab9704
# https://medium.com/@pawitp/the-correct-way-to-use-secure-boot-with-linux-a0421796eade
# https://github.com/anguianoewi/tpm2KeyUnlock/blob/master/setup2
# Read PCRs: tpm2_pcrlist

{% if tpm_security == "secureboot" or tpm_security == "customshim" %}
SB_ENABLED=$(cat /sys/firmware/efi/efivars/SecureBoot-* 2>/dev/null | dd bs=1 skip=4 status=none | hexdump -n 2 -e '"%d"')
[ "${SB_ENABLED}" = "1" ] || exit 0
{% else %}
if tpm2_unseal -H 0x81000000 -L sha256:{{ tpm_seal_pcrs }} 2>/dev/null | diff - /root/keys/boot_crypt.bin >/dev/null 2>/dev/null; then
  echo "Key unsealed successfully, disabling update service"
  systemctl disable update-tpm.service
  exit 0
fi
{% endif %}

set -e

export TPM2TOOLS_TCTI_NAME=device
export TPM2TOOLS_DEVICE_FILE=/dev/tpmrm0

tpm2_takeownership -c || true
tpm2_evictcontrol -H 0x81000000 -A o || true

WORKDIR=$(mktemp -d)

tpm2_createpolicy -P -L sha256:{{ tpm_seal_pcrs }} -f "${WORKDIR}/policy_digest"
tpm2_createprimary -H e -g sha256 -G rsa -C "${WORKDIR}/primary_context"
tpm2_create -g sha256 -G keyedhash \
  -u "${WORKDIR}/obj_pub" -r "${WORKDIR}/obj_priv" -c "${WORKDIR}/primary_context" -L "${WORKDIR}/policy_digest" \
  -A "noda|adminwithpolicy|fixedparent|fixedtpm" \
  -I /root/keys/boot_crypt.bin
tpm2_load -c "${WORKDIR}/primary_context" -u "${WORKDIR}/obj_pub" -r "${WORKDIR}/obj_priv" -C "${WORKDIR}/load_context"
tpm2_evictcontrol -c "${WORKDIR}/load_context" -A o -S 0x81000000

rm -rf "${WORKDIR}"

{% if tpm_security == "secureboot" or tpm_security == "customshim" %}
systemctl disable update-tpm.service || true
{% endif %}
