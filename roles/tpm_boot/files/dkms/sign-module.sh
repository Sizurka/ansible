#!/bin/bash

set -e
set -x


{% if tpm_security != "shim" %}
SIGNING_KEY=/root/keys/efi/db
{% else %}
SIGNING_KEY=/root/keys/efi/MOK
{% endif %}
[ -r "${SIGNING_KEY}.key" ] || exit 0

KVERSION="$1"
MODULE_SEARCH_PATH="../$kernelver/$arch/module/"
if [ -z "${kernel_source_dir}" ]; then
  kernel_source_dir="/lib/modules/$kernelver/build"
fi

SIGNING_HASH=sha256
if [ -r "/proc/config.gz" ]; then
  if zgrep CONFIG_MODULE_SIG_SHA512=y /proc/config.gz >/dev/null; then
    SIGNING_HASH=sha512
  fi
fi

function sign_module {
  local FILE="$1"
  strip --strip-debug "${FILE}" || true
  "${kernel_source_dir}/scripts/sign-file" ${SIGNING_HASH} "${SIGNING_KEY}.key" "${SIGNING_KEY}.crt" "${FILE}"
}

function sign_compressed_module {
  local FILE="$1"
  local COMPRESSOR="$2"

  "${COMPRESSOR}" -d -c "${FILE}" > "${FILE}.tmp"
  sign_module "${FILE}.tmp"
  "${COMPRESSOR}" -z -c "${FILE}.tmp" > "${FILE}"
  rm -f "${FILE}.tmp" || true
}

shopt -s nullglob
for fil in "${MODULE_SEARCH_PATH}"/*.ko; do
  if [ ! -f "${fil}" ]; then
    continue
  fi

  sign_module "${fil}"
done

for fil in "${MODULE_SEARCH_PATH}"/*.ko.zst; do
  if [ ! -f "${fil}" ]; then
    continue
  fi

  sign_compressed_module "${fil}" zstd
done

for fil in "${MODULE_SEARCH_PATH}"/*.ko.xz; do
  if [ ! -f "${fil}" ]; then
    continue
  fi

  sign_compressed_module "${fil}" xz
done

for fil in "${MODULE_SEARCH_PATH}"/*.ko.gz; do
  if [ ! -f "${fil}" ]; then
    continue
  fi

  sign_compressed_module "${fil}" gzip
done
