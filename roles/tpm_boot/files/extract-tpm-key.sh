#!/bin/sh

TARGET="$1"
umask 0077

if [ -z "${TARGET}" ] || [ "${TARGET}" = "-" ]; then
  exec tpm2_unseal -c 0x81000000 -p pcr:sha256:{{ tpm_seal_pcrs }}
fi

exec tpm2_unseal -c 0x81000000 -p pcr:sha256:{{ tpm_seal_pcrs }} -o "${TARGET}"