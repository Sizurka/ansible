# Desktop

Right click -> Configure Desktop and Wallpaper

# Settings

 1. Window Management
    1. Window Rules
       * Fix IDE Menus
         * Match window class, Substring Match, "jetbrains-"
         * Match Window types: Dialog Window
         * Match window titile (regex): "^win.*$"
         * Set window type: Force, Torn-Off Menu
 1. Shortcuts
    1. Custom Shortcuts (Right click > New > Global)
       * Home Page > `wezterm`
 1. Display and Monitor
    1. Display Configuration
       * Refresh rate

# Panel

 1. System Tray (right click arrow)
    1. System Services > Clipboard > Disabled 

Right click -> Enter edit mode

 1. More settings
    * Left align
    * Windows Can Cover
    * Maximize
 1. Remove widgets
    * Show Desktop
    * Pager
 1. Move widgets
    * Clock
    * System tray
 1. Settings
    1. Window list
        * Max rows > 1
    1. Clock
        * Disable date
 1. Set height: 32
 1. Shrink minimum size (bottom arrow)