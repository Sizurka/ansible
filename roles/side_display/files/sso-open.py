#!/usr/bin/python3

import sys
import os
import socket


user_run = os.environ.get('XDG_RUNTIME_DIR')
if not user_run:
    user_run = f'/run/user/{os.getuid()}'


sock = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_STREAM)
sock.connect(f'{user_run}/sso-target.sock')
sock.send(sys.argv[1].encode('utf-8'))
sock.close()
