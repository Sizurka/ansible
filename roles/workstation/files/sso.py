#!/usr/bin/python3

import os.path
import sys
from pathlib import Path
from paramiko.client import SSHClient


if len(sys.argv) <= 1:
    print("Usage: sso <target>")
    exit(1)


target = sys.argv[1]

client = SSHClient()
client.load_system_host_keys()
client.connect('nue.{{ domain_name }}')


def upload_file(local: Path) -> Path:
    sftp = client.open_sftp()
    remote = Path(f'/var/tmp') / local.name
    sftp.put(str(local), str(remote))
    sftp.close()
    return remote


def open_on_remote(remote: str) -> None:
    client.exec_command(f"sso-open '{remote}'")


local = Path(target)
if local.exists() and local.is_file():
    local = Path(os.path.realpath(str(local), strict=True))
    nfs_mapped = {
        '/home/{{ login.user }}/Downloads': '/home/{{ login.user }}/Downloads',
        '/home/{{ login.user }}/projects': '/home/{{ login.user }}/projects',
    }
    mapped = nfs_mapped.get(str(local.parent))
    if mapped:
        target = Path(mapped) / local.name
    else:
        target = upload_file(local)
    target = str(target)


open_on_remote(target)
client.close()
