#!/usr/bin/python3

import typing
import evdev
import pyudev
import signal
import asyncio
import systemd.daemon
import struct
import time
import sys
import threading
import traceback
from paramiko.client import SSHClient


client = SSHClient()
client.load_system_host_keys()
client.connect('nue.{{ domain_name }}')
event_target, remote_stdout, remote_stderr = client.exec_command("remote-input")


def forward_stdio(source, reader):
    while not source.channel.exit_status_ready():
        data = reader()
        if not data:
            return
        try:
            sys.stdout.buffer.write(data)
            sys.stdout.buffer.flush()
        except OSError:
            return


threading.Thread(target=forward_stdio, args=(remote_stdout, lambda: remote_stdout.channel.recv(4096)), daemon=True).start()
threading.Thread(target=forward_stdio, args=(remote_stderr, lambda: remote_stdout.channel.recv_stderr(4096)), daemon=True).start()


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
_background_tasks = set()
def background_task(coro):
    r = asyncio.get_event_loop().create_task(coro)
    _background_tasks.add(r)
    r.add_done_callback(lambda task: _background_tasks.discard(r))
    return r


def send_event_packet(event):
    try:
        event_target.write(struct.pack('<HHi', event.type, event.code, event.value))
    except OSError:
        print("Error sending data to remote target, shutting down")
        try:
            main_task.cancel()
        except:
            pass



active_devices = set()
async def forward_device(device: evdev.InputDevice):
    device.grab()
    try:
        end_key_down = False
        end_detect_delay = time.time() + 0.25
        async for event in device.async_read_loop():
            if event.type == evdev.ecodes.EV_REL:
                if event.code in (evdev.ecodes.REL_X, evdev.ecodes.REL_Y,
                                  evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL):
                    send_event_packet(event)
            elif event.type == evdev.ecodes.EV_KEY:
                if event.value == 1:
                    if event.code == evdev.ecodes.KEY_BOOKMARKS or event.code == evdev.ecodes.KEY_FAVORITES:
                        if end_detect_delay < time.time():
                            end_key_down = True
                        continue
                    send_event_packet(event)
                elif event.value == 0:
                    if event.code == evdev.ecodes.KEY_BOOKMARKS or event.code == evdev.ecodes.KEY_FAVORITES:
                        if end_key_down:
                            main_task.cancel()
                            break
                        continue
                    send_event_packet(event)
        try:
            device.ungrab()
        except:
            pass
        device.close()
    finally:
        active_devices.remove(device.path)
        print("Releasing device {}".format(device.path))


def is_valid_device(device):
    if device.phys is None:
        return False
    if len(device.phys) <= 0:
        return False
    if device.phys == 'py-evdev-uinput':
        return False
    if device.info.bustype == 0x03:
        # Probably a virtual device
        if device.info.vendor <= 1 and device.info.product <= 1 and device.info.version <= 1:
            return False
    if device.info.bustype == 0x19:
        # BUS_HOST (power button, etc)
        return False
    caps = device.capabilities()
    if evdev.ecodes.EV_KEY in caps:
        return True
    if evdev.ecodes.EV_REL in caps:
        relcaps = caps[evdev.ecodes.EV_REL]
        for check in (evdev.ecodes.REL_X, evdev.ecodes.REL_Y, evdev.ecodes.REL_WHEEL, evdev.ecodes.REL_HWHEEL):
            if check in relcaps:
                return True
    return True


def evdev_rescan():
    for path in evdev.list_devices():
        if path in active_devices:
            continue
        device = evdev.InputDevice(path)
        if not is_valid_device(device):
            device.close()
            continue
        active_devices.add(path)
        print("Attaching to device {}".format(path))
        background_task(forward_device(device))


udev = pyudev.Monitor.from_netlink(pyudev.Context())
udev.filter_by('input')
def udev_ready():
    changed = False
    while True:
        device = udev.poll(0)
        if device is None:
            break
        if device.action != 'add':
            continue
        changed = True
    if not changed:
        return
    evdev_rescan()
udev.start()
loop.add_reader(udev.fileno(), udev_ready)
evdev_rescan()


async def main():
    systemd.daemon.notify("READY=1")
    while True:
        await asyncio.sleep(5)
        systemd.daemon.notify("WATCHDOG=1")


main_task = loop.create_task(main())
loop.add_signal_handler(signal.SIGINT, main_task.cancel)
loop.add_signal_handler(signal.SIGTERM, main_task.cancel)
try:
    loop.run_until_complete(main_task)
except asyncio.CancelledError:
    pass
finally:
    systemd.daemon.notify("STOPPING=1")
    client.close()
    exit(0)
