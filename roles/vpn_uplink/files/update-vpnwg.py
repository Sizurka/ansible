#!/usr/bin/python3

import requests
import socket
import urllib.parse

result = requests.get('{{ network.wireguard.api }}/locations')
if result.status_code != 200:
    raise RuntimeError
location_info = result.json()
if location_info["status"] != "success":
    raise RuntimeError

result = requests.post('{{ network.wireguard.api }}/ip', data={
    'username': """{{ network.wireguard.user | trim }}""",
    'password': """{{ network.wireguard.password | trim }}""",
    'key': """{{ lookup('file', playbook_dir + '/files/wireguard/' + inventory_hostname + '.public') }}""",
})
if result.status_code != 200:
    raise RuntimeError
address_info = result.json()
if address_info["status"] != "success":
    raise RuntimeError


ipv4_address = address_info["ipv4"]["address"].strip() + "/" + str(address_info["ipv4"]["netmask"]).strip()
ipv6_address = address_info["ipv6"]["address"].strip() + "/" + str(address_info["ipv6"]["netmask"]).strip()
local_addresses = [ipv4_address, ipv6_address]

for server_info in location_info["locations"]:
    if server_info["name"] != '{{ network.wireguard.server }}':
        continue
    remote_pubkey = server_info["pubkey"].strip()
    remote_endpoint = server_info["pool"].strip()
    break
else:
    raise RuntimeError

remote_endpoint = urllib.parse.urlparse('//' + remote_endpoint)
remote_endpoint = socket.getaddrinfo(remote_endpoint.hostname, int(remote_endpoint.port or 51820),
                                     proto=socket.IPPROTO_UDP)[0][4]
remote_endpoint = remote_endpoint[0] + ':' + str(remote_endpoint[1])


netdev = f"""[NetDev]
Name=wgup
Kind=wireguard

[WireGuard]
PrivateKey={{ lookup('file', playbook_dir + '/files/wireguard/' + inventory_hostname + '.private') }}
ListenPort=5343

[WireGuardPeer]
PublicKey={remote_pubkey}
AllowedIPs=0.0.0.0/0, ::/0
Endpoint={remote_endpoint}
PersistentKeepalive=25
RouteTable=off
"""

network = f"""[Match]
Name=wgup

[Link]
RequiredForOnline=no

[Network]
DHCP=no
IPv6AcceptRA=0
IPv6DuplicateAddressDetection=0
LLMNR=false
ConfigureWithoutCarrier=yes

[Route]
Table=1
Destination=0.0.0.0/0
Gateway=1.1.1.1
GatewayOnLink=true

[Route]
Table=1
Destination=::/0
Gateway=1::1
GatewayOnLink=true
"""
for addr in local_addresses:
    network += f"""
[Address]
Address={addr}
"""

with open('/etc/systemd/network/10-provider.netdev', 'w') as f:
    f.write(netdev)
with open('/etc/systemd/network/20-wgup.network', 'w') as f:
    f.write(network)
