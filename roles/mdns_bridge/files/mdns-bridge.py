#!/usr/bin/python3
import argparse
import logging
import selectors
import ipaddress
import socket
import struct
import time
import ctypes
import os
import systemd.daemon
import netifaces

_LOGGER = logging.getLogger(__name__)
MDNS_PORT = 5353
MDNS_IPV4 = ipaddress.IPv4Address('224.0.0.251')
MDNS_IPV6 = ipaddress.IPv6Address('ff02::fb')

try:
    IP_PKTINFO = socket.IP_PKTINFO
except AttributeError:
    IP_PKTINFO = 8
try:
    IPV6_RECVPKTINFO = socket.IPV6_RECVPKTINFO
except AttributeError:
    IPV6_RECVPKTINFO = 49


parser = argparse.ArgumentParser(description="Bridge mDNS between networks.")
parser.add_argument('--debug',
                    dest='debug', action='store_true',
                    help="enable debug output")
parser.add_argument('--ignore-query-to',
                    dest='ignore_query_to', nargs='*', action="extend", type=str,
                    help="ignore queries to the specified network")
parser.add_argument('--ignore-response-from',
                    dest='ignore_response_from', nargs='*', action="extend", type=str,
                    help="ignore responses from the specified network")
parser.add_argument("network", nargs='+', type=str,
                    help="network interface to listen on")
args = parser.parse_args()
if args.debug:
    import sys
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    root_logger.addHandler(handler)


select = selectors.DefaultSelector()
networks = []
loopback_ipv4 = set()
loopback_ipv6 = set()
awaiting_ipv4_unicast = dict()
awaiting_ipv6_unicast = dict()


for name in netifaces.interfaces():
    addrs = netifaces.ifaddresses(name)
    ipv4 = addrs.get(netifaces.AF_INET, [])
    for addr in ipv4:
        loopback_ipv4.add(ipaddress.IPv4Address(addr['addr']))
    ipv6 = addrs.get(netifaces.AF_INET6, [])
    for addr in ipv6:
        loopback_ipv6.add(ipaddress.IPv6Address(addr['addr']))


def mask_to_prefix(packed):
    for bno in range(len(packed)):
        bdata = struct.unpack_from('<B', packed, bno)[0]
        for bit in range(8):
            if bdata & (1 << (7-bit)) == 0:
                return bno * 8 + bit
    return 8 * len(packed)


class AwaitingUnicast:
    def __init__(self, address, sock, network):
        self.address = address
        self.sock = sock
        self.network = network
        self.expires_at = time.monotonic() + 30.0

    def response(self, packet, source_network) -> None:
        if source_network == self.network:
            return
        if self.is_expired:
            return
        # We don't try and figure out if THIS packet is the response we're waiting for, just pass all unicast responses

        self.sock.sendto(packet, (str(self.address), MDNS_PORT))
        _LOGGER.debug("Sent %d unicast bytes to %s", len(packet), self.address)

    @property
    def is_expired(self) -> bool:
        return self.expires_at <= time.monotonic()


class DNSPacket:
    def __init__(self, packet):
        if len(packet) < 12:
            raise ValueError
        idnumber, flags, qdcount, anacount, nscount, arcount = struct.unpack_from('!HHHHHH', packet, 0)
        if qdcount == 0 and anacount == 0 and nscount == 0 and arcount == 0:
            raise ValueError
        packet = packet[12:]

        self.is_response = (flags & (1 << 15)) != 0

        def parse_labels(data, depth_limit: int = 10):
            if depth_limit <= 0:
                raise ValueError
            result = list()
            while True:
                label_length = int(data[0])
                data = data[1:]
                if not label_length:
                    return result, data
                if (label_length & 0b1100_0000) == 0b1100_0000:
                    offset = (label_length & 0b0011_1111)
                    pointed_labels, _ = parse_labels(packet[offset:], depth_limit-1)
                    result.extend(pointed_labels)
                    return result, data
                result.append(data[:label_length])
                data = data[label_length:]

        def next_query():
            nonlocal packet
            try:
                names, remaining = parse_labels(packet)
                packet = remaining
                type_code, dns_class = struct.unpack_from('!HH', packet, 0)
                packet = packet[4:]

                return names, type_code, dns_class
            except struct.error:
                raise ValueError

        def next_record():
            nonlocal packet
            try:
                names, remaining = parse_labels(packet)
                packet = remaining
                type_code, dns_class, ttl, rdlength = struct.unpack_from('!HHIH', packet, 0)
                packet = packet[10:]
                rdata = packet[:rdlength]
                packet = packet[:rdlength]

                return names, type_code, dns_class, ttl, rdata
            except struct.error:
                raise ValueError

        self.unicast_response = False
        for i in range(qdcount):
            _, _, dns_class = next_query()
            if dns_class & (1 << 15):
                self.unicast_response = True
        # for i in range(anacount):
        #     next_record()
        # for i in range(nscount):
        #     next_record()
        # for i in range(arcount):
        #     next_record()

    def __str__(self) -> str:
        return f"Response={self.is_response},Unicast={self.unicast_response}"


class Network:
    def __init__(self, name: str):
        self.name = name
        self.ignore_query_to = args.ignore_query_to and name in args.ignore_query_to
        self.ignore_response_from = args.ignore_response_from and name in args.ignore_response_from

        addrs = netifaces.ifaddresses(name)
        ipv4 = addrs.get(netifaces.AF_INET)
        if ipv4:
            self.addr_ipv4 = ipaddress.IPv4Interface((
                ipv4[0]['addr'],
                ipv4[0].get('mask', ipv4[0].get('netmask')).split('/')[0]
            ))
        else:
            self.addr_ipv4 = None
        ipv6 = addrs.get(netifaces.AF_INET6)
        if ipv6:
            self.addr_ipv6 = None
            for info in ipv6:
                addr = ipaddress.IPv6Interface((
                    info['addr'],
                    mask_to_prefix(ipaddress.IPv6Address(info.get('mask', info.get('netmask')).split('/')[0]).packed)
                ))
                if addr.ip.is_link_local:
                    continue
                if not self.addr_ipv6:
                    self.addr_ipv6 = addr

                if self.addr_ipv6 is not None and self.addr_ipv6.ip not in addr.network:
                    continue

                if addr.network.prefixlen > self.addr_ipv6.network.prefixlen:
                    continue

                self.addr_ipv6 = ipaddress.IPv6Interface((self.addr_ipv6.ip, addr.network.prefixlen))
        else:
            self.addr_ipv6 = None
        if not self.addr_ipv4 and not self.addr_ipv6:
            raise ValueError(f"No addresses for interface {name}")

        if self.addr_ipv4:
            addr = str(self.addr_ipv4.ip)

            self.mcast_ipv4 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.mcast_ipv4.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.mcast_ipv4.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, name.encode('ascii') + b'\0')
            self.mcast_ipv4.bind((addr, MDNS_PORT))
            self.mcast_ipv4.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)
            self.mcast_ipv4.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
            self.mcast_ipv4.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(addr))
            self.mcast_ipv4.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP,
                                       MDNS_IPV4.packed + self.addr_ipv4.ip.packed)

            self.sock_ipv4 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.sock_ipv4.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock_ipv4.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, name.encode('ascii') + b'\0')
            self.sock_ipv4.bind(('', MDNS_PORT))
            self.sock_ipv4.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
            self.sock_ipv4.setsockopt(socket.SOL_IP, IP_PKTINFO, 1)

            select.register(self.sock_ipv4, selectors.EVENT_READ, self.read_ipv4)

            _LOGGER.debug("Bound IPv4 on %s to %s", name, self.addr_ipv4)

        if self.addr_ipv6:
            addr = str(self.addr_ipv6.ip)
            ifidxarg = struct.pack('I', socket.if_nametoindex(name))

            self.mcast_ipv6 = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.mcast_ipv6.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.mcast_ipv6.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, name.encode('ascii') + b'\0')
            self.mcast_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 1)
            self.mcast_ipv6.bind((addr, MDNS_PORT))
            self.mcast_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, 1)
            self.mcast_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_LOOP, 1)
            self.mcast_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_IF, ifidxarg)
            self.mcast_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, MDNS_IPV6.packed + ifidxarg)

            self.sock_ipv6 = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.sock_ipv6.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock_ipv6.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, name.encode('ascii') + b'\0')
            self.sock_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 1)
            self.sock_ipv6.bind(('', MDNS_PORT))
            self.sock_ipv6.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_LOOP, 1)
            self.sock_ipv6.setsockopt(socket.IPPROTO_IPV6, IPV6_RECVPKTINFO, 1)

            select.register(self.sock_ipv6, selectors.EVENT_READ, self.read_ipv6)

            _LOGGER.debug("Bound IPv6 on %s to %s", name, self.addr_ipv6)

    def broadcast_ipv4(self, packet) -> None:
        self.mcast_ipv4.sendto(packet, (str(MDNS_IPV4), MDNS_PORT))
        _LOGGER.debug("%s multicast IPv4 %d bytes", self.addr_ipv4, len(packet))

    def _dispatch_ipv4_unicast(self, raw, packet) -> None:
        for dest in awaiting_ipv4_unicast.values():
            if dest.network.ignore_query_to and not packet.is_response:
                continue
            dest.response(raw, self)

    def _dispatch_ipv4_multicast(self, raw, packet) -> None:
        for net in networks:
            if net == self:
                continue
            if net.ignore_query_to and not packet.is_response:
                continue
            net.broadcast_ipv4(raw)

    def read_ipv4(self, events) -> None:
        if not (events & selectors.EVENT_READ):
            return
        raw, ancdata, _, source = self.sock_ipv4.recvmsg(8192, socket.CMSG_SPACE(12))
        source = ipaddress.IPv4Address(source[0])
        if source not in self.addr_ipv4.network:
            return
        if source in loopback_ipv4:
            return
        destination_address = None
        for cmsg_level, cmsg_type, cmsg_data in ancdata:
            if cmsg_level == socket.SOL_IP and cmsg_type == IP_PKTINFO and len(cmsg_data) >= 12:
                destination_address = ipaddress.IPv4Address(cmsg_data[8:12])
        _LOGGER.debug("%s received IPv4 %d bytes from %s->%s", self.addr_ipv4, len(raw), source,
                      destination_address or "UNKNOWN")

        try:
            packet = DNSPacket(raw)
        except ValueError:
            return
        _LOGGER.debug("Packet from IPv4 %s: %s", source, packet)

        if packet.is_response and self.ignore_response_from:
            _LOGGER.debug("Response ignored from %s", source)
            return

        if not packet.is_response and packet.unicast_response:
            awaiting_ipv4_unicast[source] = AwaitingUnicast(source, self.sock_ipv4, self)
            _LOGGER.debug("%s now waiting for unicast", source)

        if destination_address and not destination_address.is_multicast:
            if destination_address not in loopback_ipv4:
                return
            if not packet.is_response:
                _LOGGER.debug("Dropped unicast query from %s", source)
                return
            self._dispatch_ipv4_unicast(raw, packet)
        else:
            self._dispatch_ipv4_multicast(raw, packet)

    def broadcast_ipv6(self, packet) -> None:
        self.mcast_ipv6.sendto(packet, (str(MDNS_IPV6), MDNS_PORT))
        _LOGGER.debug("%s multicast IPv6 %d bytes", self.addr_ipv6, len(packet))

    def _dispatch_ipv6_unicast(self, raw, packet) -> None:
        for dest in awaiting_ipv6_unicast.values():
            if dest.network.ignore_query_to and not packet.is_response:
                continue
            dest.response(raw, self)

    def _dispatch_ipv6_multicast(self, raw, packet) -> None:
        for net in networks:
            if net == self:
                continue
            if net.ignore_query_to and not packet.is_response:
                continue
            net.broadcast_ipv6(raw)

    def read_ipv6(self, events) -> None:
        if not (events & selectors.EVENT_READ):
            return
        raw, ancdata, _, source = self.sock_ipv6.recvmsg(8192, socket.CMSG_SPACE(20))
        source = ipaddress.IPv6Address(source[0])
        if source not in self.addr_ipv6.network:
            return
        if source in loopback_ipv6:
            return
        destination_address = None
        for cmsg_level, cmsg_type, cmsg_data in ancdata:
            if cmsg_level == socket.IPPROTO_IPV6 and cmsg_type == IPV6_RECVPKTINFO and len(cmsg_data) >= 16:
                destination_address = ipaddress.IPv6Address(cmsg_data[0:16])
        _LOGGER.debug("%s received IPv6 %d bytes from %s->%s", self.addr_ipv6, len(raw), source,
                      destination_address or "UNKNOWN")

        try:
            packet = DNSPacket(raw)
        except ValueError:
            return
        _LOGGER.debug("Packet from IPv6 %s: %s", source, packet)

        if packet.is_response and self.ignore_response_from:
            _LOGGER.debug("Response ignored from %s", source)
            return

        if not packet.is_response and packet.unicast_response:
            awaiting_ipv6_unicast[source] = AwaitingUnicast(source, self.sock_ipv6, self)
            _LOGGER.debug("%s now waiting for unicast", source)

        if destination_address and not destination_address.is_multicast:
            if destination_address not in loopback_ipv6:
                return
            if not packet.is_response:
                _LOGGER.debug("Dropped unicast query from %s", source)
                return
            self._dispatch_ipv6_unicast(raw, packet)
        else:
            self._dispatch_ipv6_multicast(raw, packet)


def make_timerfd(interval):
    class timespec(ctypes.Structure):
        _fields_ = [
            ("tv_sec", ctypes.c_longlong),
            ("tv_nsec", ctypes.c_longlong),
        ]

    class itimerspec(ctypes.Structure):
        _fields_ = [
            ("it_interval", timespec),
            ("it_value", timespec),
        ]

    lc = ctypes.CDLL("libc.so.6")
    fd = lc.timerfd_create(
        1,              # CLOCK_MONOTONIC
        0o02000000 |    # TFD_CLOEXEC
        0o00004000      # TFD_NONBLOCK
    )
    assert fd >= 0
    rc = lc.timerfd_settime(
        fd, 0,
        ctypes.pointer(itimerspec(
            it_value=timespec(tv_sec=1, tv_nsec=0),
            it_interval=timespec(tv_sec=int(interval), tv_nsec=int((interval % 1) * 1E9))
        )),
        None,
    )
    assert rc == 0
    return fd


def heartbeat(events):
    if not (events & selectors.EVENT_READ):
        return
    os.read(heartbeat_timer_fd, 8)
    systemd.daemon.notify("WATCHDOG=1")

    for addr in list(awaiting_ipv4_unicast.keys()):
        if awaiting_ipv4_unicast[addr].is_expired:
            del awaiting_ipv4_unicast[addr]
            _LOGGER.debug("%s unicast wait expired", addr)
    for addr in list(awaiting_ipv6_unicast.keys()):
        if awaiting_ipv6_unicast[addr].is_expired:
            del awaiting_ipv6_unicast[addr]
            _LOGGER.debug("%s unicast wait expired", addr)


heartbeat_timer_fd = make_timerfd(10.0)
select.register(heartbeat_timer_fd, selectors.EVENT_READ, heartbeat)


for name in args.network:
    networks.append(Network(name))

systemd.daemon.notify("READY=1")
_LOGGER.debug("mDNS bridge ready")
while True:
    for key, mask in select.select():
        key.data(mask)
