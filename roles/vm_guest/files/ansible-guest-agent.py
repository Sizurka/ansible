#!/usr/bin/python3
# Copyright (c) 2020 Derek Hageman <hageman@inthat.cloud>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import sys

sys.dont_write_bytecode = True

import syslog

syslog.openlog("ansible-guest-agent", syslog.LOG_NDELAY | syslog.LOG_CONS, syslog.LOG_DAEMON)

import os
import fcntl
import struct
import signal
import select
import errno
import subprocess
import random
import threading
import time
import shlex


VIRTIO_PORT = "cloud.inthat.ansible_agent.0"

poll = select.poll()
poll_dispatch = dict()
port = None
tx_buffer = bytearray()
rx_buffer = bytearray()
read_processor = None

active_receive_streams = dict()
active_send_streams = dict()

process_lock = threading.Lock()
active_processes = set()

STREAM_ID_CONTROL = 0xFFFF
COMMAND_END_STREAM = 0
COMMAND_START_PUT_FILE = 1
COMMAND_START_FETCH_FILE = 2
COMMAND_START_PROCESS = 3


def close_port(reason):
    global port
    global tx_buffer
    global rx_buffer

    if port is None:
        return
    syslog.syslog("Host disconnected: {}".format(reason))

    try:
        poll.unregister(port)
    except:
        pass
    poll_dispatch.pop(port)
    try:
        os.close(port)
    except:
        pass
    port = None
    tx_buffer.clear()
    rx_buffer.clear()
    for c in active_receive_streams.values():
        c.close()
    active_receive_streams.clear()
    for c in active_send_streams.values():
        c.close()
    active_send_streams.clear()
    with process_lock:
        for c in active_processes:
            c.close()
        active_processes.clear()


def end_receive_stream(stream_id):
    active = active_receive_streams.pop(stream_id, None)
    if active is None:
        return
    active.close()


class ReceiveStream:
    def __init__(self, stream_id, target):
        end_receive_stream(stream_id)

        self._stream_id = stream_id
        self._target = target
        self._pending_write = bytearray()

        try:
            fcntl.fcntl(self._target.fileno(), fcntl.F_SETFL,
                        fcntl.fcntl(self._target.fileno(), fcntl.F_GETFL) | os.O_NONBLOCK)
        except (OSError, IOError):
            pass

        active_receive_streams[stream_id] = self

        global poll
        global poll_dispatch
        poll.register(self._target.fileno(), select.POLLERR | select.POLLHUP | select.POLLNVAL)
        poll_dispatch[self._target.fileno()] = self.poll_event

    def close(self):
        global poll
        global poll_dispatch
        poll.unregister(self._target.fileno())
        poll_dispatch.pop(self._target.fileno())
        return self._target.close()

    def _abort_stream(self):
        global active_receive_streams
        self.close()
        active_receive_streams.pop(self._stream_id)

    def poll_event(self, event):
        if event & (select.POLLERR | select.POLLHUP | select.POLLNVAL):
            return self._abort_stream()
        if not (event & select.POLLOUT):
            return
        if len(self._pending_write) == 0:
            return

        try:
            nwr = self._target.write(self._pending_write)
        except OSError as e:
            if e.errno != errno.EWOULDBLOCK:
                self._abort_stream()
            return

        if nwr <= 0:
            return
        del self._pending_write[:nwr]
        if len(self._pending_write) == 0:
            global poll
            poll.modify(self._target.fileno(), select.POLLERR | select.POLLHUP | select.POLLNVAL)

    def received_data(self, data):
        self._pending_write += data
        if len(self._pending_write) > 0:
            global poll
            poll.modify(self._target.fileno(), select.POLLOUT | select.POLLERR | select.POLLHUP | select.POLLNVAL)


class SendStream:
    def __init__(self, stream_id, source):
        assert stream_id not in active_send_streams

        self._stream_id = stream_id
        self._source = source

        try:
            fcntl.fcntl(self._source.fileno(), fcntl.F_SETFL,
                        fcntl.fcntl(self._source.fileno(), fcntl.F_GETFL) | os.O_NONBLOCK)
        except (OSError, IOError):
            pass

        active_send_streams[stream_id] = self

        global poll
        poll.register(self._source.fileno(), select.POLLIN | select.POLLERR | select.POLLHUP | select.POLLNVAL)
        poll_dispatch[self._source.fileno()] = self.poll_event

    def close(self):
        global poll
        global poll_dispatch
        poll.unregister(self._source.fileno())
        poll_dispatch.pop(self._source.fileno())
        self._source.close()

    def _send_end(self):
        global tx_buffer
        global active_send_streams
        tx_buffer += struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_END_STREAM, self._stream_id)
        self.close()
        active_send_streams.pop(self._stream_id)

    def poll_event(self, event):
        if event & select.POLLIN:
            try:
                data = self._source.read(0xFFFF)
                if data is None or len(data) == 0:
                    return self._send_end()
            except OSError as e:
                if e.errno != errno.EWOULDBLOCK:
                    self._send_end()
                return

            global tx_buffer
            tx_buffer += struct.pack("<HH", self._stream_id, len(data))
            tx_buffer += data

        if event & (select.POLLERR | select.POLLHUP | select.POLLNVAL):
            return self._send_end()


def attach_send_stream(source):
    while True:
        stream_id = random.randint(1, STREAM_ID_CONTROL - 1)
        if stream_id not in active_receive_streams:
            break
        stream_id += 1
        if stream_id not in active_receive_streams:
            break
    SendStream(stream_id, source)
    return stream_id


def start_put_file(name, stream_id):
    global tx_buffer
    if len(name) == 0:
        syslog.syslog(syslog.LOG_ERR, "No target file name specified")
        return
    try:
        target = open(name, "wb")
    except OSError as e:
        tx_buffer += struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_START_PUT_FILE, STREAM_ID_CONTROL)
        syslog.syslog(syslog.LOG_ERR, "Unable to open file {} for writing: {}".format(name, e.strerror))
        return
    ReceiveStream(stream_id, target)
    tx_buffer += struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_START_PUT_FILE, stream_id)
    syslog.syslog(syslog.LOG_DEBUG, "PUT {}".format(name))


def start_fetch_file(name):
    global tx_buffer
    if len(name) == 0:
        syslog.syslog(syslog.LOG_ERR, "No source file name specified")
        return
    try:
        source = open(name, "rb")
    except OSError as e:
        tx_buffer += struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_START_FETCH_FILE, STREAM_ID_CONTROL)
        syslog.syslog(syslog.LOG_ERR, "Unable to open file {} for reading: {}".format(name, e.strerror))
        return
    stream_id = attach_send_stream(source)
    tx_buffer += struct.pack("<HHH", STREAM_ID_CONTROL, COMMAND_START_FETCH_FILE, stream_id)
    syslog.syslog(syslog.LOG_DEBUG, "FETCH {}".format(name))


def start_process(args, stdin_stream_id):
    global tx_buffer
    if len(args) == 0 or len(args[0]) == 0:
        syslog.syslog(syslog.LOG_ERR, "No executable specified")
        return
    try:
        stdin_source = subprocess.DEVNULL
        if stdin_stream_id != STREAM_ID_CONTROL:
            stdin_source = subprocess.PIPE
        process = subprocess.Popen(args, shell=False, stdin=stdin_source,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError as e:
        tx_buffer += struct.pack("<HHHHHH", STREAM_ID_CONTROL, COMMAND_START_PROCESS,
                                 STREAM_ID_CONTROL, STREAM_ID_CONTROL, STREAM_ID_CONTROL, STREAM_ID_CONTROL)
        syslog.syslog(syslog.LOG_ERR, "Error starting process {}: {}".format(' '.join(args), e.strerror))
        return
    except subprocess.SubprocessError:
        tx_buffer += struct.pack("<HHHHHH", STREAM_ID_CONTROL, COMMAND_START_PROCESS,
                                 STREAM_ID_CONTROL, STREAM_ID_CONTROL, STREAM_ID_CONTROL, STREAM_ID_CONTROL)
        syslog.syslog(syslog.LOG_ERR, "Error starting process {}".format(' '.join(args)))
        return
    if stdin_source == subprocess.PIPE:
        ReceiveStream(stdin_stream_id, process.stdin)

    class Monitor(threading.Thread):
        def __init__(self, process):
            threading.Thread.__init__(self, daemon=True, name="process_monitor")
            self._process = process
            (r, w) = os.pipe2(os.O_CLOEXEC)
            self._status_target = w
            self.stream_source = open(r, "rb")

            global process_lock
            global active_processes
            with process_lock:
                active_processes.add(self)

        def close(self):
            self._process.send_signal(signal.SIGHUP)

        def run(self):
            self._process.wait()
            try:
                os.write(self._status_target, struct.pack("<h", self._process.returncode))
                os.close(self._status_target)
            except OSError:
                pass
            with process_lock:
                active_processes.discard(self)

    stream_id_stdout = attach_send_stream(process.stdout)
    stream_id_stderr = attach_send_stream(process.stderr)
    m = Monitor(process)
    stream_id_monitor = attach_send_stream(m.stream_source)
    m.start()
    tx_buffer += struct.pack("<HHHHHH", STREAM_ID_CONTROL, COMMAND_START_PROCESS,
                             stdin_stream_id, stream_id_stdout, stream_id_stderr, stream_id_monitor)
    syslog.syslog(syslog.LOG_DEBUG, "EXEC {}".format(' '.join(args)))


def process_control(command, offset):
    if command == COMMAND_END_STREAM:
        if len(rx_buffer) - offset < 2:
            return False
        stream_id, = struct.unpack_from("<H", rx_buffer, offset)
        offset += 2
        del rx_buffer[:offset]
        end_receive_stream(stream_id)
        return True
    elif command == COMMAND_START_PUT_FILE:
        if len(rx_buffer) - offset < 4:
            return False
        stream_id, name_length = struct.unpack_from("<HH", rx_buffer, offset)
        offset += 4
        if len(rx_buffer) - offset < name_length:
            return False
        name = rx_buffer[offset:offset + name_length].decode("utf-8", "ignore")
        offset += name_length
        del rx_buffer[:offset]
        start_put_file(name, stream_id)
        return True
    elif command == COMMAND_START_FETCH_FILE:
        if len(rx_buffer) - offset < 2:
            return False
        name_length, = struct.unpack_from("<H", rx_buffer, offset)
        offset += 2
        if len(rx_buffer) - offset < name_length:
            return False
        name = rx_buffer[offset:offset+name_length].decode("utf-8", "ignore")
        offset += name_length
        del rx_buffer[:offset]
        start_fetch_file(name)
        return True
    elif command == COMMAND_START_PROCESS:
        if len(rx_buffer) - offset < 4:
            return False
        stdin_stream_id, n_args = struct.unpack_from("<HH", rx_buffer, offset)
        offset += 4
        args = list()
        for i in range(n_args):
            if len(rx_buffer) - offset < 4:
                return False
            arg_length, = struct.unpack_from("<L", rx_buffer, offset)
            offset += 4
            if len(rx_buffer) - offset < arg_length:
                return False
            arg = rx_buffer[offset:offset+arg_length].decode("utf-8", "ignore")
            offset += arg_length
            args.append(arg)
        del rx_buffer[:offset]
        start_process(args, stdin_stream_id)
        return True
    else:
        close_port("invalid command")
        return False


def process_stream():
    global rx_buffer
    global active_receive_streams
    if len(rx_buffer) < 4:
        return False
    stream_id, length = struct.unpack_from("<HH", rx_buffer)
    offset = 4
    if stream_id == STREAM_ID_CONTROL:
        return process_control(length, offset)
    if len(rx_buffer) - offset < length:
        return False
    target = active_receive_streams.get(stream_id)
    if target is None:
        del rx_buffer[:offset+length]
        return True
    target.received_data(rx_buffer[offset:offset+length])
    offset += length
    del rx_buffer[:offset]
    return True


def process_handshake():
    global read_processor
    if len(rx_buffer) < 3:
        return False
    if rx_buffer[0] != 0x00:
        close_port("handshake failure")
        return False
    if rx_buffer[1] != 0xAA:
        close_port("handshake failure")
        return False
    if rx_buffer[2] != 0xFF:
        close_port("handshake failure")
        return False
    del rx_buffer[:3]
    read_processor = process_stream
    return read_processor()


def port_event(event):
    global port
    global tx_buffer
    global rx_buffer
    if event & (select.POLLERR | select.POLLHUP | select.POLLNVAL):
        close_port("hangup")
        return
    if (event & select.POLLOUT) and len(tx_buffer) > 0:
        try:
            nwr = os.write(port, tx_buffer)
            del tx_buffer[:nwr]
        except OSError as e:
            if e.errno != errno.EAGAIN:
                # EAGAIN (disconnected) caught by the POLLHUP, so it might just be a buffer full which we ignore
                close_port("write failure")
                return
    if event & select.POLLIN:
        try:
            data = os.read(port, 131072)
            if len(data) == 0:
                # Host disconnected
                close_port("disconnected")
                return
            rx_buffer += data
        except OSError as e:
            if e.errno != errno.EAGAIN:
                # EAGAIN is no data available
                close_port("read failure")
                return
        if read_processor is not None:
            while read_processor():
                pass


def open_port():
    global port
    global tx_buffer
    global rx_buffer
    global read_processor
    close_port("reopen")
    try:
        port = os.open("/dev/virtio-ports/" + VIRTIO_PORT, os.O_RDWR)
    except (OSError, IOError):
        return False

    try:
        fcntl.fcntl(port, fcntl.F_SETOWN, os.getpid())
    except (OSError, IOError):
        close_port("SIGIO enable failure")
        return False

    # Only way to wait for the host seems to be a blocking write, so do that
    try:
        os.write(port, struct.pack("<BBB", 0x00, 0xFF, 0xAA))
    except (OSError, IOError):
        close_port("handshake write failure")
        return False

    try:
        fcntl.fcntl(port, fcntl.F_SETFL, fcntl.fcntl(port, fcntl.F_GETFL) | os.O_NONBLOCK | os.O_ASYNC)
    except (OSError, IOError):
        close_port("set non-blocking failure")
        return False

    tx_buffer.clear()
    rx_buffer.clear()
    read_processor = process_handshake
    poll.register(port, select.POLLIN | select.POLLERR | select.POLLNVAL | select.POLLHUP)
    poll_dispatch[port] = port_event

    syslog.syslog("Host connection open")
    return True


def port_state_changed(sig, frame):
    # Just ignore it and make the syscall get interrupted
    pass


signal.signal(signal.SIGIO, port_state_changed)
while True:
    while port is None and not open_port():
        time.sleep(0.1)
    port_events = select.POLLIN | select.POLLERR | select.POLLNVAL | select.POLLHUP
    if len(tx_buffer) > 0:
        port_events |= select.POLLOUT
    poll.modify(port, port_events)
    for fd, event in poll.poll():
        target = poll_dispatch.get(fd)
        if target is not None:
            target(event)

